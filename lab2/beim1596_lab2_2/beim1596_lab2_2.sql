/*N�v: Bal�zs �va-T�nde
Azonos�t�: beim1596
Csoport: 511/1
Feladat: 2. sorsz�m, 2. labor*/

CREATE TABLE Szineszek(
  SzineszID INT, CONSTRAINT PK_SzineszID PRIMARY KEY(SzineszID),
  SzineszNev VARCHAR(30),
  SzulDatum DATE
);

CREATE TABLE Mufaj(
  MufajID INT, CONSTRAINT PK_MufajID PRIMARY KEY(MufajID),
  MufajNev VARCHAR(30),
);

CREATE TABLE Studiok(
  StudioID INT, CONSTRAINT PK_StudioID PRIMARY KEY(StudioID),
  StudioNev VARCHAR(30),
  StudioCim VARCHAR(30),
);

CREATE TABLE Filmek(
  FilmID INT, CONSTRAINT PK_FilmID PRIMARY KEY(FilmID),
  FilmCim VARCHAR(30),
  Koltseg FLOAT(30),
  MegjEv DATE,
  StudioID INT, CONSTRAINT FK_F_SID FOREIGN KEY (StudioID) REFERENCES Studiok(StudioID),
  MufajID INT, CONSTRAINT FK_F_MID FOREIGN KEY (MufajID) REFERENCES Mufaj(MufajID),
);

CREATE TABLE Szerepel(
  SzineszID INT,
  FilmID INT,
  CONSTRAINT FK_Sz_SzID FOREIGN KEY (SzineszID) REFERENCES Szineszek(SzineszID),
  CONSTRAINT FK_Sz_FID FOREIGN KEY (FilmID) REFERENCES Filmek(FilmID),
  CONSTRAINT PK_Szerepel PRIMARY KEY(SzineszID,FilmID)
);

CREATE TABLE Vetites(
  VetitesID INT,CONSTRAINT PK_VID PRIMARY KEY(VetitesID),
  FilmID INT,CONSTRAINT FK_V_FID FOREIGN KEY (FilmID) REFERENCES Filmek(FilmID),
  Datum DATE
);

ALTER TABLE Szineszek
  ADD Eletkor INT;

ALTER TABLE Studiok
	ADD DEFAULT 'Anonymus' FOR StudioCim;

ALTER TABLE Filmek
  DROP COLUMN Koltseg;

ALTER TABLE Vetites
   DROP CONSTRAINT PK_VID;
   
ALTER TABLE Vetites
   DROP COLUMN VetitesID;
   
ALTER TABLE Vetites
   ADD VetitesID INT IDENTITY(3,2);
   
ALTER TABLE Vetites
  ADD CONSTRAINT PK_VID PRIMARY KEY(VetitesID);

ALTER TABLE Filmek
  ALTER COLUMN FilmCim VARCHAR(30) not null;
  
INSERT INTO Szineszek 
  VALUES ('1', 'Leonardo DiCaprio', '06/26/1982','35'),('2', 'Benedict Cumberbatch', '07/19/1976','41'),('3', 'Angelina Jolie', '06/12/1977','40'),('4', 'Rowan Akinson', '01/06/1955','62'),('5', 'Mandy Moore', '04/10/1984','33');
INSERT INTO Mufaj
  VALUES ('1', 'Drama'),('2', 'Akcio'),('3', 'Horror'),('4', 'Krimi'),('5', 'Romantikus'),('6', 'Vigjatek'),('7', 'Fantasy'),('8', 'Sci-fi');
INSERT INTO Studiok 
  VALUES ('1','Abrakadabra','Nemtudommi str. egyeb'),('2','Universal Studios','Los Angeles'),('3','Capitol Studios','Los Angeles'),('4','Sunset Las Palmas Studios','Hollywood'),('5','Universal STudios','Hollywood');
INSERT INTO Filmek
  VALUES ('1','Inception','06/26/1999','1','1'),('2','Sherlock','06/26/2008','1','4'),('3','This is us','06/26/2015','1','4'),('4','Mr. Bean','06/26/1998','1','6'),('5','Doctor Strange','06/26/3016','1','8');
INSERT INTO Szerepel
  VALUES ('1','1'),('2','2'),('5','3'),('4','4'),('2','5');
INSERT INTO Vetites
  VALUES('1','06/26/2018'),('2','03/12/2017'),('3','05/26/2017'),('4','01/19/2018'),('5','12/26/2017');
INSERT INTO Filmek
  VALUES ('6','Blabla','06/26/1999','1','9');
DELETE FROM Szerepel
  WHERE SzineszID=2;
DELETE FROM Szineszek
  WHERE SzineszID=2;
SELECT * FROM Szineszek;