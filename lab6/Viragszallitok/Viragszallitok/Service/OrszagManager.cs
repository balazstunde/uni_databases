﻿using System;
using System.Collections.Generic;
using Viragszallitok.Models;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Balazs Eva-Tunde, 521, beim1596*/
namespace Viragszallitok.Service
{
    class OrszagManager:DataAccesLayer
    {
        public List<Orszagok> GetOrszagList(ref string error)
        {
            string query = "SELECT OrszagID, Nev FROM Orszagok";
            SqlDataReader dataReader = ExecuteReader(query, ref error);

            List<Orszagok> orszagLista = new List<Orszagok>();

            if (error == "OK")
            {

                while (dataReader.Read())
                {
                    Orszagok item = new Orszagok();
                    try
                    {
                        item.orszagID = Convert.ToInt32(dataReader["OrszagID"]);
                        item.OrszagNev = dataReader["Nev"].ToString();
                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                    orszagLista.Add(item);
                }
            }
            CloseDataReader(dataReader);

            return orszagLista;
        }

        public List<String> GetOrszagok(ref string error)
        {
            string query = "SELECT * FROM Orszagok";
            SqlDataReader dataReader = ExecuteReader(query, ref error);

            List<String> orszagLista = new List<String>();

            if (error == "OK")
            {

                while (dataReader.Read())
                {
                    try
                    {
                        String item = dataReader[1].ToString();
                        orszagLista.Add(item);
                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                }
            }
            CloseDataReader(dataReader);

            return orszagLista;
        }
    }
}
