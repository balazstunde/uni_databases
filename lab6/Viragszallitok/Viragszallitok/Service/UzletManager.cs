﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Viragszallitok.Models;
using System.Windows.Forms;
/*Balazs Eva-Tunde, 521, beim1596*/
namespace Viragszallitok.Service
{
    class UzletManager:DataAccesLayer
    {

        public UzletManager(ref string error)
        {
            base.CreateConnection(ref error);
        }
        public List<Uzletek> GetUzletListDataReader(char kbetu, string orszag, ref string error)
        {
            string query = "SELECT Uzletek.UzletID, Uzletek.Cim, Uzletek.Nev, Uzletek.Telefonszam, Orszagok.OrszagID, Orszagok.Nev as ONev " +
                "FROM Uzletek, Orszagok" +
                " WHERE Orszagok.OrszagID = Uzletek.OrszagID AND Uzletek.Nev LIKE(\'" + kbetu + "%\')" + " AND Orszagok.OrszagID = " + orszag ;

            SqlDataReader dataReader = ExecuteReader(query, ref error);
            List<Uzletek> uzletLista = new List<Uzletek>();

           //MessageBox.Show(error);

            if (error == "OK")
            {
                
                while (dataReader.Read())
                {
                    Uzletek item = new Uzletek();
                    try
                    {
                        item.uzletID = Convert.ToInt32(dataReader["UzletID"]);
                        item.uzletCim = dataReader["Cim"].ToString();
                        item.uzletNev = dataReader["Nev"].ToString();
                        item.uzletTelefonszam = dataReader["Telefonszam"].ToString();
                        item.uzletOrszaga = new Orszagok(Convert.ToInt32(dataReader["OrszagID"]), dataReader["ONev"].ToString());

                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                    uzletLista.Add(item);
                }
            }
            CloseDataReader(dataReader);

            return uzletLista;
        }

        public List<Uzletek> GetUzletList(ref string error)
        {
            string query = "SELECT Uzletek.UzletID, Uzletek.Cim, Uzletek.Nev, Uzletek.Telefonszam, Orszagok.OrszagID, Orszagok.Nev as ONev " +
                " FROM Uzletek, Orszagok " +
                "WHERE Orszagok.OrszagID = Uzletek.OrszagID";

            SqlDataReader dataReader = ExecuteReader(query, ref error);

            List<Uzletek> uzletLista = new List<Uzletek>();
            Uzletek item = null;

            if (error == "OK")
            {
                while (dataReader.Read())
                {
                    item = new Uzletek();
                    try
                    {
                        item.uzletID = Convert.ToInt32(dataReader["UzletID"]);
                        item.uzletCim = dataReader["Cim"].ToString();
                        item.uzletNev = dataReader["Nev"].ToString();
                        item.uzletTelefonszam = dataReader["Telefonszam"].ToString();
                        item.uzletOrszaga = new Orszagok(Convert.ToInt32(dataReader["OrszagID"]), dataReader["ONev"].ToString());
                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                    uzletLista.Add(item);
                }
            }
            CloseDataReader(dataReader);
            return uzletLista;
        }

        public int GetUzletNumber(ref string error)
        {
            string query = "SELECT COUNT(*) FROM Uzletek";
            object value = ExecuteScalar(query, ref error);
            if (error == "OK")
            {
                return Convert.ToInt32(value);
            }
            else
            {
                return -1;
            }
        }

        public void InsertUzlet(string uzletnev, string cim, string telefonszam, int orszagID, ref string error)
        {
            string ErrMess = string.Empty;
            string query = "INSERT INTO Uzletek VALUES ("
                            + "'" + uzletnev + "','"
                            + cim + "','"
                            + telefonszam + "',"
                            + orszagID.ToString() + ")";
            MessageBox.Show(query);
            SqlCommand sqlCommand = new SqlCommand(query, conn);

            ExecuteNonQuery(query, ref error);
        }

        public void UpdateUzlet(int uzletID, string uzletnev, string cim, string telefonszam, int orszagID, ref string error)
        {
            string ErrMess = string.Empty;
            string query = "UPDATE Uzletek " +
                            "SET Nev='" + uzletnev + "', Cim='" + cim + "', Telefonszam='" + telefonszam + "', OrszagID=" + orszagID +
                            " WHERE UzletID=" + uzletID;
            MessageBox.Show(query);
            SqlCommand sqlCommand = new SqlCommand(query, conn);
            
            ExecuteNonQuery(query, ref error);
        }

        public void DeleteSmth(string tabla, string feltetel, int feltetelID, ref string error)
        {
            string ErrMess = string.Empty;
            string query = "DELETE FROM " + tabla +
                           " WHERE " + feltetel + "=" + feltetelID;
            SqlCommand sqlCommand = new SqlCommand(query, conn);

            ExecuteNonQuery(query, ref error);
        }
    }
}
