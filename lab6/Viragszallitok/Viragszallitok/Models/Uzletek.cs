﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Balazs Eva-Tunde, 521, beim1596*/
namespace Viragszallitok.Models
{
    class Uzletek
    {
        private int UzletID;
        private string Cim;
        private string Nev;
        private string Telefonszam;
        private Orszagok Orszag;

        public int uzletID
        {
            get { return UzletID; }
            set { UzletID = value; }
        }
        public string uzletCim
        {
            get { return Cim; }
            set { Cim = value; }
        }
        public string uzletNev
        {
            get { return Nev; }
            set { Nev = value; }
        }
        public string uzletTelefonszam
        {
            get { return Telefonszam; }
            set { Telefonszam = value; }
        }

        public Orszagok uzletOrszaga
        {
            get { return Orszag; }
            set { Orszag = value; }
        }
    }
}
