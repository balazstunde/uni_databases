USE master;
GO
IF EXISTS(select * from sys.databases where name='Lab8_2')
	DROP DATABASE Lab8_2

CREATE DATABASE Lab8_2;
GO
USE Lab8_2;
GO

CREATE TABLE Bentlakasok(
	BentlakasID INT PRIMARY KEY,
	Nev VARCHAR(30),
	Cim VARCHAR(30) DEFAULT 'UNKNOWN',
	SzobaSzam INT)
CREATE TABLE SzobaTipusok(
	SzobaTipusID INT PRIMARY KEY,
	Nev VARCHAR(30) UNIQUE,
	AgyakSzama INT,
	Ar INT)
CREATE TABLE Szobak(
	BentlakasID INT FOREIGN KEY REFERENCES Bentlakasok(BentlakasID),
	SzobaTipusID INT FOREIGN KEY REFERENCES SzobaTipusok(SzobaTipusID),
	SzobakSzama INT,
	PRIMARY KEY(BentlakasID, SzobaTipusID))
CREATE TABLE Szakok(
	SzakID INT PRIMARY KEY,
	SzakNev VARCHAR(30) UNIQUE)
CREATE TABLE Diakok(
	DiakID INT PRIMARY KEY,
	Nev VARCHAR(30),
	Telefonszam VARCHAR(12),
	SzakID INT FOREIGN KEY REFERENCES Szakok(SzakID))
CREATE TABLE Lakik(
	BentlakasID INT FOREIGN KEY REFERENCES Bentlakasok(BentlakasID),
	DiakID INT FOREIGN KEY REFERENCES Diakok(DiakID),
	PRIMARY KEY(BentlakasID, DiakID))

insert into Bentlakasok (BentlakasID, Nev, Cim, SzobaSzam) values (1, 'Kreiger, Thiel and Schmeler', '395 Blaine Crossing', 169);
insert into Bentlakasok (BentlakasID, Nev, Cim, SzobaSzam) values (2, 'Kshlerin and Sons', '88370 Independence Way', 73);
insert into Bentlakasok (BentlakasID, Nev, Cim, SzobaSzam) values (3, 'Hoeger-Rodriguez', '376 Kennedy Crossing', 36);
insert into Bentlakasok (BentlakasID, Nev, Cim, SzobaSzam) values (4, 'Maggio', '450 Ludington Lane', 168);
insert into Bentlakasok (BentlakasID, Nev, Cim, SzobaSzam) values (5, 'Kuvalis Group', '9 Hoard Court', 107);

insert into SzobaTipusok (SzobaTipusID, Nev, AgyakSzama, Ar) values (1, '8 agyas', 8, 205);
insert into SzobaTipusok (SzobaTipusID, Nev, AgyakSzama, Ar) values (2, '5 agyas', 5, 315);
insert into SzobaTipusok (SzobaTipusID, Nev, AgyakSzama, Ar) values (3, '3 agyas', 3, 356);
insert into SzobaTipusok (SzobaTipusID, Nev, AgyakSzama, Ar) values (4, '2 agyas', 2, 450);

insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (3, 1, 20);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (3, 2, 8);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (3, 3, 18);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (2, 1, 10);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (3, 4, 21);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (4, 4, 13);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (1, 2, 30);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (5, 1, 28);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (4, 3, 16);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (1, 3, 10);
insert into Szobak (BentlakasID, SzobaTipusID, SzobakSzama) values (1, 4, 17);

insert into Szakok (SzakID, SzakNev) values (1, 'Informatika')
insert into Szakok (SzakID, SzakNev) values (2, 'Matematika')
insert into Szakok (SzakID, SzakNev) values (3, 'Teologia')
insert into Szakok (SzakID, SzakNev) values (4, 'Filozofia')
insert into Szakok (SzakID, SzakNev) values (5, 'Tortenelem')
insert into Szakok (SzakID, SzakNev) values (6, 'Foldrajz')
insert into Szakok (SzakID, SzakNev) values (7, 'Biologia')


insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (1, 'Tallia Sarre', '3044581137', 6);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (2, 'Seymour Vicker', '7405055285', 3);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (3, 'Haven Rossetti', '9188785600', 1);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (4, 'Taryn Tuson', '4534731942', 1);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (5, 'Horatia Mallon', '3889878978', 5);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (6, 'Chase Goodinson', '7368570454', 3);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (7, 'Elsey Bickerdike', '5489837737', 3);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (8, 'Faythe Nail', '7926686675', 3);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (9, 'Rorke Esel', '5482954074', 4);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (10, 'Layla Latimer', '1683846044', 1);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (11, 'Nanine Benoy', '2113826330', 1);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (12, 'Georgeta Brach', '7556144035', 2);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (13, 'Ilyssa Collinwood', '7315026248', 5);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (14, 'Chastity Itzcak', '5853090939', 4);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (15, 'Kirsteni Izod', '1192533668', 6);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (16, 'Cliff Plaster', '7842707207', 3);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (17, 'Danit Samms', '5207048091', 4);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (18, 'Barnett Lavell', '3493004163', 1);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (19, 'Nicoli Tanzig', '5043576453', 2);
insert into Diakok (DiakID, Nev, Telefonszam, SzakID) values (20, 'Hedda Buckthorpe', '8048985941', 4);

insert into Lakik (BentlakasID, DiakID) values (3, 1);
insert into Lakik (BentlakasID, DiakID) values (3, 2);
insert into Lakik (BentlakasID, DiakID) values (5, 3);
insert into Lakik (BentlakasID, DiakID) values (5, 4);
insert into Lakik (BentlakasID, DiakID) values (2, 5);
insert into Lakik (BentlakasID, DiakID) values (4, 6);
insert into Lakik (BentlakasID, DiakID) values (5, 7);
insert into Lakik (BentlakasID, DiakID) values (4, 8);
insert into Lakik (BentlakasID, DiakID) values (4, 9);
insert into Lakik (BentlakasID, DiakID) values (2, 10);
insert into Lakik (BentlakasID, DiakID) values (2, 11);
insert into Lakik (BentlakasID, DiakID) values (3, 12);
insert into Lakik (BentlakasID, DiakID) values (1, 13);
insert into Lakik (BentlakasID, DiakID) values (5, 14);
insert into Lakik (BentlakasID, DiakID) values (2, 15);
insert into Lakik (BentlakasID, DiakID) values (2, 16);
insert into Lakik (BentlakasID, DiakID) values (1, 17);
insert into Lakik (BentlakasID, DiakID) values (3, 18);
insert into Lakik (BentlakasID, DiakID) values (2, 19);
insert into Lakik (BentlakasID, DiakID) values (5, 20);