/*Bal�zs �va-T�nde, beim1596, 521, labor8/2*/

/*Adjuk meg azon bentlak�st (bentlak�sokat), melyek a legt�bb szobat�pussal rendelkeznek! (Bentlakasok.Nev)*/
use Lab8_2
GO
CREATE VIEW Szobaszamok AS
SELECT Bentlakasok.BentlakasID, COUNT(SzobaTipusok.SzobaTipusID) As SzT
FROM Bentlakasok, SzobaTipusok, Szobak
WHERE Bentlakasok.BentlakasID=Szobak.BentlakasID AND Szobak.SzobaTipusID=SzobaTipusok.SzobaTipusID
Group BY Bentlakasok.BentlakasID
GO
SELECT Bentlakasok.Nev
FROM Bentlakasok, Szobaszamok
WHERE Bentlakasok.BentlakasID=Szobaszamok.BentlakasID AND Szobaszamok.SzT=(SELECT MAX(SzT) FROM Szobaszamok)

/*Adjuk meg azon szakokat, melyeken legal�bb annyi di�k tanul, mint a 'Foldrajz' szakon! (Szakok.Szakn�v)*/
GO
CREATE VIEW DiakokSzakonkent AS
SELECT Szakok.SzakNev, COUNT(Diakok.DiakID) AS DiakSzam
FROM Szakok LEFT JOIN Diakok ON
Szakok.SzakID=Diakok.SzakID
GROUP BY Szakok.SzakNev
GO
SELECT DISTINCT Szakok.SzakNev
FROM Szakok,DiakokSzakonkent
WHERE Szakok.SzakNev=DiakokSzakonkent.SzakNev AND DiakokSzakonkent.DiakSzam>=(SELECT DiakSzam FROM DiakokSzakonkent WHERE DiakokSzakonkent.SzakNev='Foldrajz')
/*Adjuk meg a m�sodik legdr�g�bb szob�t! (Szobatipusok.Nev)*/
SELECT SzobaTipusok.Nev
FROM SzobaTipusok
WHERE SzobaTipusok.Ar=(SELECT MAX(SzobaTipusok.Ar) FROM SzobaTipusok) AND SzobaTipusok.Ar NOT IN (SELECT DISTINCT TOP 1 SzobaTipusok.Ar FROM SzobaTipusok ORDER BY Ar)

/*Adjuk meg azon bentlak�sokat, melyekben legal�bb 5 di�k lakik! (Bentlakasok.Nev)*/
GO
CREATE VIEW BentlakasokDiakjai AS
SELECT Bentlakasok.Nev, COUNT(Lakik.DiakID) AS diakszam
FROM Bentlakasok, Lakik
WHERE Bentlakasok.BentlakasID=Lakik.BentlakasID
GROUP BY Bentlakasok.Nev
GO
SELECT Bentlakasok.Nev
FROM Bentlakasok, BentlakasokDiakjai
WHERE Bentlakasok.Nev=BentlakasokDiakjai.Nev AND diakszam>=5
/*Adjuk meg azon bentlak�sokat, ahol a legt�bb di�k lakik a legdr�g�bb szobat�pus� szob�(k)ban! (Bentlakasok.Nev)*/
GO
CREATE VIEW LegragabbSzobakbanLakok AS
SELECT Bentlakasok.Nev, COUNT(Lakik.DiakID) AS lakokszama
FROM Bentlakasok,SzobaTipusok, Szobak,Lakik
WHERE SzobaTipusok.SzobaTipusID=Szobak.SzobaTipusID AND Bentlakasok.BentlakasID=Szobak.BentlakasID AND Lakik.BentlakasID=Bentlakasok.BentlakasID AND SzobaTipusok.Ar=(SELECT MAX(Ar) FROM SzobaTipusok)
GROUP BY Bentlakasok.Nev
GO 
SELECT Bentlakasok.Nev
FROM LegragabbSzobakbanLakok,Bentlakasok
WHERE LegragabbSzobakbanLakok.Nev=Bentlakasok.Nev AND lakokszama=(SELECT MAX(lakokszama) FROM LegragabbSzobakbanLakok)
/*Adjuk meg azon bentlak�sokat, melyek CSAK �8 agyas� szob�val rendelkeznek! (Bentlakasok.Nev, Bentlakasok.Cim)*/
SELECT Bentlakasok.Nev, Bentlakasok.Cim
FROM Bentlakasok,Szobak,SzobaTipusok
WHERE Bentlakasok.BentlakasID=Szobak.BentlakasID AND Szobak.SzobaTipusID=SzobaTipusok.SzobaTipusID AND SzobaTipusok.Nev='8 agyas'
EXCEPT
SELECT Bentlakasok.Nev, Bentlakasok.Cim
FROM Bentlakasok,Szobak,SzobaTipusok
WHERE Bentlakasok.BentlakasID=Szobak.BentlakasID AND Szobak.SzobaTipusID=SzobaTipusok.SzobaTipusID AND SzobaTipusok.Nev!='8 agyas'
/*Adjuk meg azon bentlak�sokat, melyek minden tipus� szob�t tartalmaznak! (Bentlakasok.Nev)*/

SELECT Bentlakasok.Nev
FROM Bentlakasok, Szobak
WHERE Bentlakasok.BentlakasID=Szobak.BentlakasID
GROUP BY Bentlakasok.Nev
HAVING COUNT(Szobak.SzobaTipusID)=(SELECT COUNT(SzobaTipusok.SzobaTipusID)
FROM SzobaTipusok)

/*SELECT Bentlakasok.Nev, SzobaTipusok.Nev
FROM Bentlakasok,SzobaTipusok, Szobak
WHERE Bentlakasok.BentlakasID=Szobak.BentlakasID AND Szobak.SzobaTipusID=SzobaTipusok.SzobaTipusID*/
/*Adjuk meg szakonk�nt a di�kok sz�m�t! (Azon szakokat is, amelyeken egy di�k sem tanul!) (Szakok.SzakNev, DiakokSzama)*/
SELECT Szakok.SzakNev, COUNT(Diakok.DiakID) AS DiakokSzama
FROM Szakok LEFT JOIN Diakok
ON Diakok.SzakID=Szakok.SzakID
GROUP BY Szakok.SzakNev
/*Adjuk meg azon bentlak�sokat, melyekben van �8 agyas�, DE �5 agyas� �S �2 agyas� szoba NINCS! (Bentlakasok.Nev)*/
SELECT DISTINCT Bentlakasok.Nev, Bentlakasok.Cim
FROM Bentlakasok,Szobak,SzobaTipusok
WHERE Bentlakasok.BentlakasID=Szobak.BentlakasID AND Szobak.SzobaTipusID=SzobaTipusok.SzobaTipusID AND SzobaTipusok.Nev='8 agyas'
EXCEPT
SELECT DISTINCT Bentlakasok.Nev, Bentlakasok.Cim
FROM Bentlakasok,Szobak,SzobaTipusok
WHERE Bentlakasok.BentlakasID=Szobak.BentlakasID AND Szobak.SzobaTipusID=SzobaTipusok.SzobaTipusID 
	AND (SzobaTipusok.Nev='5 agyas' OR SzobaTipusok.Nev='2 agyas')

/*Adjuk meg, melyik bentlak�s(ok)ban tal�lhat� meg a legolcs�bb szoba! (Bentlakasok.Nev)*/
SELECT DISTINCT Bentlakasok.Nev
FROM Bentlakasok, Szobak, SzobaTipusok
WHERE Bentlakasok.BentlakasID=Szobak.BentlakasID AND Szobak.SzobaTipusID=SzobaTipusok.SzobaTipusID 
	AND SzobaTipusok.Ar=(SELECT MIN(Ar) FROM SzobaTipusok)

DROP VIEW Szobaszamok;
DROP VIEW DiakokSzakonkent;
DROP VIEW BentlakasokDiakjai;
DROP VIEW LegragabbSzobakbanLakok;