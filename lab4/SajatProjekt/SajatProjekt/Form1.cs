﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;
using System.Data.SqlClient;

namespace SajatProjekt
{
    public partial class frm_Proba : Form
    {

        DataAccesLayer connection;
        string connectionstring=null;
        public frm_Proba()
        {
            InitializeComponent();
            connectionstring = " Server=TUNDIKE-LAPTOP\\SQLEXPRESS;" +
                                " Database=ViragSzallito;" +
                                " Trusted_Connection=Yes";
            connection = new DataAccesLayer(connectionstring);
        }

        private void bt_OK_Click(object sender, EventArgs e)
        {
            string errorMessage = null;
            if (errorMessage != null)
            {
                MessageBox.Show("Csatlakozas hiba!");
            }
        }

        private void frm_Proba_Load(object sender, EventArgs e)
        {
            string errorMessage = null;
            connection.Connect(ref errorMessage);
            if (errorMessage != null)
            {
                MessageBox.Show("Csatlakozas hiba!");
            }
            SqlConnection conn = new SqlConnection(connectionstring);
            conn.Open();
            string Sql = "select Nev from Orszagok";
            SqlCommand cmd = new SqlCommand(Sql, conn);
            SqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                CBOrszagok.Items.Add(DR[0]);
            }
            conn.Close();
        }
    }
}
