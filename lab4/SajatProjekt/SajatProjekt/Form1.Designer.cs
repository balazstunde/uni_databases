﻿namespace SajatProjekt
{
    partial class frm_Proba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_OK = new System.Windows.Forms.Button();
            this.DG_list = new System.Windows.Forms.DataGridView();
            this.CBOrszagok = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.bt_ComboBox = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DG_list)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_OK
            // 
            this.bt_OK.Location = new System.Drawing.Point(274, 257);
            this.bt_OK.Name = "bt_OK";
            this.bt_OK.Size = new System.Drawing.Size(75, 23);
            this.bt_OK.TabIndex = 0;
            this.bt_OK.Text = "Szűkít";
            this.bt_OK.UseVisualStyleBackColor = true;
            this.bt_OK.Click += new System.EventHandler(this.bt_OK_Click);
            // 
            // DG_list
            // 
            this.DG_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DG_list.Location = new System.Drawing.Point(38, 32);
            this.DG_list.Name = "DG_list";
            this.DG_list.Size = new System.Drawing.Size(499, 198);
            this.DG_list.TabIndex = 1;
            // 
            // CBOrszagok
            // 
            this.CBOrszagok.FormattingEnabled = true;
            this.CBOrszagok.Location = new System.Drawing.Point(38, 334);
            this.CBOrszagok.Name = "CBOrszagok";
            this.CBOrszagok.Size = new System.Drawing.Size(121, 21);
            this.CBOrszagok.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(38, 257);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // bt_ComboBox
            // 
            this.bt_ComboBox.Location = new System.Drawing.Point(274, 334);
            this.bt_ComboBox.Name = "bt_ComboBox";
            this.bt_ComboBox.Size = new System.Drawing.Size(75, 23);
            this.bt_ComboBox.TabIndex = 4;
            this.bt_ComboBox.Text = "Szűkít";
            this.bt_ComboBox.UseVisualStyleBackColor = true;
            // 
            // frm_Proba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 419);
            this.Controls.Add(this.bt_ComboBox);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.CBOrszagok);
            this.Controls.Add(this.DG_list);
            this.Controls.Add(this.bt_OK);
            this.Name = "frm_Proba";
            this.Text = "Proba";
            this.Load += new System.EventHandler(this.frm_Proba_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DG_list)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_OK;
        private System.Windows.Forms.DataGridView DG_list;
        private System.Windows.Forms.ComboBox CBOrszagok;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button bt_ComboBox;
    }
}

