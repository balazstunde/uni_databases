/*Bal�zs �va-T�nde, beim1596, 521/1, 10. labor*/
/*1. �rjunk t�rolt elj�r�st, melynek bemeneti param�tere: @pNap int t�pus�! 
A t�rolt elj�r�s seg�ts�g�vel irassuk ki azon tan�r(ok) nev�t, aki(k)nek a legkevesebb csoporttal 
van �r�juk a param�terk�nt megadott napon! Megj. Lehets�ges, hogy van olyan tan�r, akinek nincs �r�ja az adott napon! 
Ez esetben a kimeneti param�ter (@pOut-int t�pus�) �rt�k�t �ll�tsuk -1-re, ellenkez� esetben a kimeneti param�terben 
adjuk meg a felt�telnek eleget tev� tan�r(ok) �tlagfizet�s�t!*/
/*hiba vagy figyelmezteto RAISERROR 0-10 ERROR, 11-18 WAITING
@n. valamit irassunk ki, akkor kurzorral dolgozzunk!!
fuggveny<=>function
*/

use Lab10_2;
GO
CREATE PROCEDURE kevesetdolgozotanarok
	@pNap INT, @pOut INT OUTPUT
	AS
	BEGIN
			SELECT Tanarok.TanarID,Tanarok.Nev INTO #Temp
			FROM Tanarok
			EXCEPT
				SELECT Tanit.TanarID, Tanarok.Nev
				FROM Tanit,Tanarok
				WHERE Tanit.Nap=@pNap AND Tanarok.TanarID=Tanit.TanarID
			IF EXISTS(SELECT * FROM #Temp)
			BEGIN
				SELECT Nev FROM #Temp
				SET @pOut=-1
				DROP TABLE #Temp
			END
			ELSE
			BEGIN
				SELECT Tanarok.TanarID,Tanarok.Nev,(COUNT(Csoportok.CsoportID)) AS cssz
				INTO #Temp1
				FROM Csoportok,Tanarok, Tanit
				WHERE Tanit.Nap=@pNap AND Tanit.TanarID=Tanarok.TanarID AND Tanit.CsoportID=Csoportok.CsoportID
				GROUP BY Tanarok.TanarID, Tanarok.Nev

				SELECT Nev
				FROM #Temp1
				WHERE cssz=(SELECT MIN(cssz) FROM #Temp1)

				SET @pOut=(SELECT AVG(Tanarok.Fizetes) FROM Tanarok,#Temp1 WHERE #Temp1.TanarID=Tanarok.TanarID)
				DROP TABLE #Temp1
			END
			
			
	END
GO

DECLARE @kimenet INT
EXEC kevesetdolgozotanarok 1,@kimenet output
SELECT @kimenet
DROP PROCEDURE kevesetdolgozotanarok
/*2. �rjunk t�rolt elj�r�st, mellyel egy �j tan�rra, valamint egy tan�t�s�ra vonatkoz� inform�ci�kat vezethet�nk 
fel az adatb�zisba! A t�rolt elj�r�s param�terei: @pTanarNev, @pTanarCim, @pTelSzam, @pTantargyNev, @pCsoportNev, @pTeremNev, 
@pNap, @pOra! Ha az adott tan�r m�r l�tezik az adatb�zisban, �rjunk ki megfelel� �zenetet! A tan�t�s besz�r�sa el�tt ellen�rizz�k, 
hogy megtal�lhat�-e minden m�s adat az adatb�zisban a tan�t�sra vonatkoz�an (pl. l�tezik-e az adott tant�rgy? stb.)! 
Ha nem, akkor sz�rjuk be az �sszes megfelel� t�bl�ba az �j adatokat (a param�terk�nt meg nem adott �rt�keket NULL-ra �ll�thatjuk, 
ha NINCS az adott mez�h�z Default megszor�t�s rendelve)!
[Fakultat�v: A m�veletsorra tekints�nk egys�gk�nt s vagy az �sszes besz�r�st hajtsa v�gre a t�rolt elj�r�s, vagy egyet sem. (Seg�ts�g: tranzakci�k haszn�lat�val megoldhat� a feladat!)]*/

GO
CREATE PROCEDURE tanaros
	 (@pTanarNev VARCHAR(30), @pTanarCim VARCHAR(30), @pTelSzam VARCHAR(12), @pTantargyNev VARCHAR(30), @pCsoportNev VARCHAR(30), @pTeremNev VARCHAR(30), @pNap INT, @pOra TIME)
	AS BEGIN
	SET NOCOUNT ON
	 DECLARE @id INT
		IF NOT EXISTS (SELECT * FROM Tantargyak WHERE Tantargyak.Nev=@pTantargyNev)
			BEGIN
				SET @id=(SELECT MAX(Tantargyak.TantargyID) FROM Tantargyak)+1
				INSERT INTO Tantargyak(TantargyID,Nev,KreditSzam) VALUES(@id,@pTantargyNev,NULL)
			END

		IF NOT EXISTS (SELECT * FROM Csoportok WHERE Csoportok.Nev=@pCsoportNev)
			BEGIN
				SET @id=(SELECT MAX(Csoportok.CsoportID) FROM Csoportok)+1
				INSERT INTO Csoportok VALUES(@id,@pCsoportNev,NULL,NULL)
			END

		IF NOT EXISTS (SELECT * FROM Termek WHERE Termek.Nev=@pTeremNev)
			BEGIN
				SET @id=(SELECT MAX(Termek.TeremID) FROM Termek)+1
				INSERT INTO Termek VALUES(@id,@pTeremNev,NULL)
			END

		IF NOT EXISTS(SELECT * FROM Tanarok WHERE Tanarok.Nev=@pTanarNev AND @pTanarCim=Tanarok.Cim AND @pTelSzam=Tanarok.Tel)
			BEGIN
				SET @id=(SELECT MAX(Tanarok.TanarID) FROM Tanarok)+1
				INSERT INTO Tanarok VALUES(@id,@pTanarNev,@pTanarCim,@pTelSzam,NULL)
			END
		ELSE
			BEGIN
				PRINT 'A tanar mar letezik az adatbazisban.'
			END
		INSERT INTO Tanit VALUES((SELECT Tanarok.TanarID FROM Tanarok WHERE Tanarok.Nev=@pTanarNev),(SELECT Tantargyak.TantargyID FROM Tantargyak WHERE Tantargyak.Nev=@pTantargyNev),(SELECT Csoportok.CsoportID FROM Csoportok WHERE Csoportok.Nev=@pCsoportNev),(SELECT Termek.TeremID FROM Termek WHERE Termek.Nev=@pTeremNev),@pNap,@pOra)
	END
GO

EXEC tanaros 'Segesvari Antal', 'Kolozsvar', '0742342361', 'Mesterseges intelligencia', 'Info3', 'phi', 2, '14:00'
DROP PROCEDURE tanaros
/*3. �rjunk f�ggv�nyt, melynek bemen� param�terei: @pTanarNev, @pSzakNev. 
A f�ggv�ny egy t�bl�ban t�r�tse vissza azon tant�rgyak nev�t, kreditsz�m�t �s le�r�s�t, mely(ek)et a 
param�terk�nt megadott tan�r tan�t a szint�n param�terk�nt megadott szaknak!*/
GO
CREATE FUNCTION tanartantargyai
	(@pTanarNev VARCHAR(30), @pSzakNev VARCHAR(30))
RETURNS TABLE AS RETURN
	SELECT DISTINCT Tantargyak.Nev,Tantargyak.KreditSzam,Tantargyak.Leiras
	FROM Tantargyak, Tanit, Tanarok, Szakok, Csoportok
	WHERE Tanit.TantargyID=Tantargyak.TantargyID AND Tanarok.Nev=@pTanarNev AND Tanarok.TanarID=Tanit.TanarID AND Szakok.Nev=@pSzakNev AND Tanit.CsoportID=Csoportok.CsoportID AND Csoportok.SzakID=Szakok.SzakID
GO

SELECT * FROM tanartantargyai('Emelina Littlechild','Informatika')
DROP FUNCTION tanartantargyai
/*4. �rjunk f�ggv�nyt, melynek bemen� param�terei: @pNap-int, @pFerohely-int, @pOra1-time, @pOra2-time t�pus�ak! 
A f�ggv�ny t�r�tse vissza (int t�pus� v�ltoz�ban), hogy az adott napon (@pNap) a k�t id�pont k�z�tt a foglalt termek h�ny sz�zal�ka 
rendelkezik t�bb, mint @pFerohely f�r�hellyel!*/
GO
CREATE FUNCTION ferohelyek
	(@pNap INT, @pFerohely INT, @pOra1 TIME, @pOra2 TIME)
RETURNS FLOAT
BEGIN
	 DECLARE @osszes INT, @tobbmintferohely INT;
	 SET @osszes = (SELECT COUNT(Termek.TeremID) 
					FROM Termek,Tanit 
					WHERE Tanit.TeremID=Termek.TeremID AND Tanit.Nap=@pNap AND Tanit.Ora>=@pOra1 AND Tanit.Ora<@pOra2)
	 SET @tobbmintferohely = (SELECT COUNT(Termek.TeremID) 
					FROM Termek,Tanit 
					WHERE Tanit.TeremID=Termek.TeremID AND Tanit.Nap=@pNap AND Tanit.Ora>=@pOra1 AND Tanit.Ora<@pOra2 AND Termek.Ferohely>@pFerohely)
	
	 RETURN CAST(@tobbmintferohely AS FLOAT)/ CAST(@osszes AS FLOAT) * 100;
END
GO
DECLARE @sz FLOAT
EXEC @sz = ferohelyek 6,740,'3:00', '10:00'
SELECT @sz
DROP FUNCTION ferohelyek
/*5. �rjunk t�rolt elj�r�st, melynek bemen� param�terei: @pn, @pi-int t�pus�ak! A t�rolt elj�r�s seg�ts�g�vel adjuk 
meg a @pn., @pi. �s @pn+@pi. legnagyobb kreditsz�m� tant�rgy nev�t �s le�r�s�t! Pl. @pn=3, @pi=1, akkor az 1.,3. �s 4. 
legnagyobb kreditsz�m� tant�rgyakat keress�k. Nem megfelel� bemeneti param�terek eset�n 
(negat�v sz�m, tant�rgyak sz�m�n�l nagyobb sz�m) irassunk ki megfelel� figyelmeztet�- vagy hiba�zenetet!*/
GO
CREATE PROCEDURE kreditszamutantargy
 (@pn INT, @pi INT)
AS BEGIN
SET NOCOUNT ON
	DECLARE @tantargyakszama INT
	SET @tantargyakszama=(SELECT COUNT(*) FROM Tantargyak)
	IF @pi<=0 OR @pn<=0 OR @pi>@tantargyakszama OR @pn>@tantargyakszama OR @pi+@pn>@tantargyakszama
	BEGIN
		RAISERROR('Helytelen bemeneti parameterek.',16,1)
		RETURN
	END
	DECLARE c CURSOR FOR
		SELECT Tantargyak.Nev, Tantargyak.Leiras
		FROM Tantargyak
		ORDER BY KreditSzam DESC;
	OPEN c;
	DECLARE @TNev VARCHAR(30),@TLeiras VARCHAR(30),@i INT
	SET @i=1
	FETCH NEXT FROM c INTO @Tnev, @TLeiras
	WHILE @@FETCH_STATUS=0
	BEGIN
		IF (@i=@pn OR @i=@pi OR @i=@pi+@pn)
			BEGIN
				PRINT @Tnev+ ' ' + @TLeiras
			END
		SET @i=@i+1;
		FETCH NEXT FROM c INTO @Tnev, @TLeiras
	END
	CLOSE c
	DEALLOCATE c
END
GO

EXEC kreditszamutantargy 2,3
EXEC kreditszamutantargy 2,12
DROP PROCEDURE kreditszamutantargy
/*6. �rjunk DELETE triggert, mely akkor aktiv�l�dik, ha t�rl�nk egy vagy t�bb sort a Szakok t�bl�b�l! 
(Figyelem! A hivatkoz�si �ps�g megszor�t�sok fenntart�sa miatt probl�ma lehet, ha egy olyan szakot szeretn�k t�r�lni, 
melyhez tartozik csoport. Ezt a probl�m�t orvosoljuk a trigger seg�ts�g�vel! Az adott szak mindenk�pp t�rl�dj�n a trigger 
v�grehajt�s�nak k�vetkezt�ben!)*/
GO
CREATE TRIGGER tri
ON Szakok
INSTEAD OF DELETE
AS
UPDATE Csoportok
SET Csoportok.SzakID=NULL
WHERE Csoportok.SzakID in (SELECT SzakID FROM deleted)
DELETE FROM Szakok
WHERE SzakID IN (SELECT SzakID FROM deleted)
GO

DELETE FROM Szakok
WHERE SzakID=2
DROP TRIGGER tri
