﻿USE master;
GO
IF EXISTS(select * from sys.databases where name='Lab10_2')
	DROP DATABASE Lab10_2

CREATE DATABASE Lab10_2;
GO
USE Lab10_2;
GO

CREATE TABLE Termek(
	TeremID INT PRIMARY KEY,
	Nev VARCHAR(30),
	Ferohely INT)
CREATE TABLE Tantargyak(
	TantargyID INT PRIMARY KEY,
	Nev VARCHAR(30) UNIQUE,
	KreditSzam INT,
	Leiras VARCHAR(30) DEFAULT 'NOTHING')
CREATE TABLE Szakok(
	SzakID INT PRIMARY KEY,
	Nev VARCHAR(30) UNIQUE)
CREATE TABLE Csoportok(
	CsoportID INT PRIMARY KEY,
	Nev VARCHAR(30) UNIQUE,
	SzakID INT FOREIGN KEY REFERENCES Szakok(SzakID),
	Letszam INT)
CREATE TABLE Tanarok(
	TanarID INT PRIMARY KEY,
	Nev VARCHAR(30),
	Cim VARCHAR(30),
	Tel VARCHAR(12),
	Fizetes INT)
CREATE TABLE Tanit(
    TanitID INT IDENTITY PRIMARY KEY,
	TanarID INT FOREIGN KEY REFERENCES Tanarok(TanarID),
	TantargyID INT FOREIGN KEY REFERENCES Tantargyak(TantargyID),
	CsoportID INT FOREIGN KEY REFERENCES Csoportok(CsoportID),
	TeremID INT FOREIGN KEY REFERENCES Termek(TeremID),
	Nap INT, --A h�t valamelyik napja. PL. 1-h�tfo, 2-kedd, ..., 7-vas�rnap
	Ora TIME) --pl. 13:23

insert into Termek (TeremID, Nev, Ferohely) values (1, 'Oyondu', '2019');
insert into Termek (TeremID, Nev, Ferohely) values (2, 'Voonix', '87302');
insert into Termek (TeremID, Nev, Ferohely) values (3, 'Babbleset', '18');
insert into Termek (TeremID, Nev, Ferohely) values (4, 'Oyoba', '59');
insert into Termek (TeremID, Nev, Ferohely) values (5, 'Topiclounge', '69282');
insert into Termek (TeremID, Nev, Ferohely) values (6, 'Divavu', '440');
insert into Termek (TeremID, Nev, Ferohely) values (7, 'Devshare', '770');
insert into Termek (TeremID, Nev, Ferohely) values (8, 'Digitube', '757');
insert into Termek (TeremID, Nev, Ferohely) values (9, 'Flashspan', '52');
insert into Termek (TeremID, Nev, Ferohely) values (10, 'Blogspan', '6912');
insert into Termek (TeremID, Nev, Ferohely) values (11, 'Central', '212');


--Informatika
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (1, 'Informatika alapjai', 7);
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (2, 'Szoftver fejelsztes', 7);
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (3, 'C++', 10);
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (4, 'Java', 6);
--Matematika
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (5, 'Geometria 1', 6);
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (6, 'Algebra', 10);
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (7, 'Analizis', 7);
--Teologia
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (8, 'Teologiai alapok 1', 2);
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (9, 'Teologiai alapok 2', 8);
--Fizika
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (10, 'Hotan', 2);
insert into Tantargyak (TantargyID, Nev, KreditSzam) values (11, 'Mechanika', 8);


insert into Szakok (SzakID, Nev) values (1, 'Informatika')
insert into Szakok (SzakID, Nev) values (2, 'Matematika')
insert into Szakok (SzakID, Nev) values (3, 'Teologia')
insert into Szakok (SzakID, Nev) values (4, 'Fizika informatika')

insert into Csoportok (CsoportID, Nev, SzakID, Letszam) values (1, 'Teo1', 3, 93);
insert into Csoportok (CsoportID, Nev, SzakID, Letszam) values (2, 'Mat1', 2, 48);
insert into Csoportok (CsoportID, Nev, SzakID, Letszam) values (3, 'Info1', 1, 85);
insert into Csoportok (CsoportID, Nev, SzakID, Letszam) values (4, 'Info2', 1, 99);
insert into Csoportok (CsoportID, Nev, SzakID, Letszam) values (5, 'Info3', 1, 99);
insert into Csoportok (CsoportID, Nev, SzakID, Letszam) values (6, 'Fizinfo', 1, 15);

insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (1, 'Lazarus Clendinning', '0 Arrowood Plaza', '1739686202', 500);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (2, 'Emelina Littlechild', '10581 3rd Park', '1654211387',1000);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (3, 'Rozamond Jowle', '75272 Bay Street', '7099120366',400);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (4, 'Evita Mitcheson', '7455 Schlimgen Circle', '6336939128',350);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (5, 'Jannelle McArd', '07 Boyd Street', '2554230517',650);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (6, 'Bruce Gennerich', '09 Portage Terrace', '8222631307',725);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (7, 'Belva Chardin', '011 Doe Crossing Road', '4209681426',250);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (8, 'Richart Park', '9 Scofield Street', '6733493403',1200);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (9, 'Mac Cosgrove', '2 Huxley Point', '9419078633',650);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (10, 'Isacco Mollitt', '24 Esch Circle', '6004062431',475);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (11, 'Jennifer Smith', '34 Central Perk', '3333332431',850);
insert into Tanarok (TanarID, Nev, Cim, Tel, Fizetes) values (12, 'Richart Park', '14 Central Perk', '8888332431',725);


insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (8, 5, 1, 7, 1, '11:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (3, 2, 1, 8, 5, '6:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (5, 1, 2, 8, 2, '10:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (2, 2, 4, 4, 4, '10:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (1, 8, 1, 1, 2, '6:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (1, 2, 3, 9, 2, '2:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (7, 3, 3, 7, 5, '12:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (5, 7, 2, 10, 2, '3:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (9, 5, 2, 6, 3, '8:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (9, 4, 1, 7, 6, '3:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (3, 2, 4, 6, 6, '3:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (4, 1, 4, 9, 4, '8:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (4, 9, 2, 7, 5, '1:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (5, 6, 2, 8, 2, '10:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (8, 9, 3, 10, 6, '7:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (6, 1, 2, 5, 6, '11:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (7, 3, 3, 4, 6, '2:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (5, 2, 2, 3, 1, '9:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (8, 3, 2, 10, 2, '2:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (9, 6, 2, 6, 3, '5:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (6, 9, 4, 8, 6, '9:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (2, 1, 4, 2, 4, '1:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (7, 2, 3, 9, 3, '11:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (2, 8, 2, 8, 5, '11:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (1, 1, 2, 2, 4, '9:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (1, 10, 6, 2, 4, '18:00');
insert into Tanit (TanarID, TantargyID, CsoportID, TeremID, Nap, Ora) values (1, 10, 5, 11, 4, '18:00');