using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace HouseRent
{
    /// <summary>
    /// Summary description for DAL.
    /// </summary>
    public abstract class DALGen
    {
        //protected lathatosagi szabaly-csak a tartalmazó osztályból és a származtatott osztályokban lehet használni
        protected static bool isConnected;

        //ebben a projektben egy SQL szerveren futo adatbazishoz akarunk csatlakozni
        //SQL adatbazishoz SqlConnection objektumokkal kapcsolodhatunk
        protected static SqlConnection sqlConnection;

        //kapcsolati karakterlanc (Connection string): MSSQL szerver neve, adatbazis neve, kapcsolodas modja

        protected string strSqlConn = "Data Source=ANDI-PC;Initial Catalog=Berles;Integrated Security=SSPI"; //Windows Authentication esetén
        //   protected string strSqlConn = "Data Source=(local);Initial Catalog=Berlesek;uid=hatha_m_2016;pwd=1234"; //SQL Server Authentication esetén
        //   protected string strSqlConn = "Data Source=ANDI-PC\\SQLEXPRESS;Initial Catalog=Berlesek;uid=hatha_m_2016;pwd=1234"; //SQL Server Authentication esetén


        //parancsobjektum-ezek segitsegevel tudunk SQL utasitasokat vegrehajtani
        //parancsobjektumok a kapcsolati objektumok segitsegevel tartanak kapcsolatot 
        //az adatbazissal
        //protected System.Data.SqlClient.SqlCommand sqlCommand;

        //a kovetkezo ket objektum segitsegevel az adatbazisbol adatokat olvashatunk be
        //illetve tarolhatjuk ezeket
        //protected System.Data.SqlClient.SqlDataReader sqlDataReader;
        //protected System.Data.DataSet dataSet;

        //kapcsolatot teremt az adatbazissal, ha ez meg nem tortent meg eddig         
        protected void CreateConnection(ref string errMess)
        {
            // Create the Connection if is was not already created.
            if (isConnected != true)
            {
                try
                {
                    //uj OleDbConnection objektum letrehozasa
                    //ezzel a konstruktorral letrehoztuk a kapcsolati objektumot es beallitottuk a ConnectionString
                    //tulajdonsagat is
                    //ez csak akkor allithato be, ha a kapcsolati objektum zarva van
                    sqlConnection = new SqlConnection(strSqlConn);

                    //kapcsolodunk az adatbazishoz
                    sqlConnection.Open();
                    //igazat terithetunk vissza, hisz a kapcsolat gond nelkul letrejott
                    errMess = "OK";
                }
                catch (SqlException ex)
                {
                    //tovabbkuldjuk az errorMessage-t
                    errMess = ex.Message;
                }
                finally
                // A finally block nagyon lenyeges, 
                // mivel ez biztositja, hogy a kapcsolat akkor is bezarodik, 
                // ha barmilyen hiba fellepett (ami kivetelt valtott ki).
                {
                    //igyekezzunk minel kevesebb ideig nyitva hagyni a kapcsolatot,
                    //mert igy sporolunk az adatbazis eroforrasaival
                    if (sqlConnection != null)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }



        /// <summary>
        /// Open the Connection when the state is not already open.
        /// </summary>
        private void OpenConnection(ref string errMess)
        {
            // Open the Connection when the state is not already open.
            if (isConnected == false)
            {
                try
                {
                    sqlConnection.Open();
                    isConnected = true;
                    errMess = "OK";
                }
                catch (SqlException ex)
                {
                    errMess = ex.Message;
                }
            }
        }

        //bezarjuk a kapcsolatot, ha meg nyitva van
        //internal lathatosagi szabaly-csak a tartalmazo osztalybol lehet hozzaferni
        private void CloseConnection()
        {
            // Close the Connection when the connection is opened.
            if (isConnected == true)
            {
                sqlConnection.Close();
                isConnected = false;
            }
        }


        //vegrehajtja az elso parameterben megadott stringben levo select utasitast
        //egy eredmenyhalmaz jon letre ezaltal, melyet egy DataReader objektumban tarolunk el
        //es teritunk vissza
        protected SqlDataReader ExecuteReader(string sQuery, ref string errMess)
        {
            SqlDataReader sqlDataReader = null;
            try
            {
                OpenConnection(ref errMess);//megnyitja az adatbazis-kapcsolatot, ha meg nincs megnyitva) 

                //uj sqlcommand objektumot igy is letrehozhatunk:
                //sQuery-tartalmazza az SQL utasitast, melybol adatot kerunk le
                //mySqlConn-SqlConnection objektum 
                SqlCommand sqlCommand = new SqlCommand(sQuery, sqlConnection);
                //myComm OleDbCommand CommandText tulajdonsagat atkuldi az m_Conn
                //connection obj-nak es letrehoz egy DataReadert

                //ExecuterReader tagfgv-olyan select utasitasok vegrehajtasara szolgal, melyek
                //eredmenyhalmazt adnak vissza-az eredmenyhalmaz egy DataReader objektumban ter vissza
                sqlDataReader = sqlCommand.ExecuteReader();
                errMess = "OK";
            }
            catch (Exception e)
            {
                errMess = e.Message;
                CloseDataReader(sqlDataReader);
            }
            return sqlDataReader;
        }

        /// <summary>
        /// Closes the data reader given as a parameter, and also closes the connection
        /// </summary>
        /// <param name="rdr">The SqlDataReader to be closed</param>
        protected void CloseDataReader(SqlDataReader rdr)
        {
            if (rdr != null)
                rdr.Close();
            CloseConnection();
        }

        /// <summary>
        /// Executes a given query, and returns the result in a dataset.
        /// A megfelelo select utasitassal feltolti a DataSet-et a megfelelo adatokkal egy DataAdapter objektum segitsegevel
        /// </summary>
        /// <param name="sQuery"> The query to be executed </param>
        /// <param name="sTableName"> The name of the DataSet. </param>
        /// <param name="ErrMess"> Output error message </param>
        /// <returns></returns>
        protected DataSet ExecuteDS(string query, ref string errMess)
        {
            //letrehozunk egy Dataset objektumot-a parameter segitsegevel 
            //beallitottuk a DataSetName tulajdonsagat a DataSet objektumunknak
            DataSet dataSet = new DataSet();
            try
            {
                OpenConnection(ref errMess);

                //Create a SqlDataAdapter for the Houses table.
                SqlDataAdapter dataAdapter = new SqlDataAdapter(query, sqlConnection);

                //Fill()-DataSet tagfgv-e-osszehangolja a DataSet objektum sorait az adatbazis soraival
                //a visszaadott int ertek azt mutatja, hogy az adatbazis hany soranak osszehangolasa tortent meg
                //VAGYIS-a Fill-el tudjuk feltolteni a Dataset objektumot adatokkal
                dataAdapter.Fill(dataSet);

                errMess = "OK";
            }
            catch (SqlException e)
            {
                errMess = e.Message;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    CloseConnection();
                }
            }
            return dataSet;
        }
        /// <summary>
        /// Executes a stored procedure that has an output parameter and returns a value in it.
        /// </summary>
        /// <param name="spName">The name of the stored procedure</param>
        /// <param name="parameterNames">The list of the parameter names</param>
        /// <param name="parameterValues">The list of the parameter values</param>
        /// <returns></returns>

        protected object ExecuteStoredProcedureNonQuery(string spName, string[] parameterNames, string[] parameterValues, string outputParameterName, ref string errMess)
        {
            object parOut = null;
            try
            {
                OpenConnection(ref errMess);
                SqlCommand cmd = new SqlCommand(spName, sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                for (int i = 0; i < parameterNames.Length; i++)
                {
                    cmd.Parameters.AddWithValue(parameterNames[i], parameterValues[i]);
                }

                SqlParameter pOut = new SqlParameter();
                pOut.ParameterName = outputParameterName;
                pOut.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pOut);

                cmd.ExecuteNonQuery();

                parOut = pOut.Value;
            }
            catch (SqlException e)
            {
                errMess = e.Message;
            }
            finally
            {
                CloseConnection();
            }
            return parOut;
        }

        /// <summary>
        /// Executes a given stored procedure that has an output parameter and returns the result in a dataset.
        /// </summary>
        /// <param name="spName">The name of the stored procedure</param>
        /// <param name="parameterNames">The list of the parameter names</param>
        /// <param name="parameterValues">The list of the parameter values</param> 
        /// <param name="outputParameterName">The name of the output parameter</param>
        /// <param name="parOut">The value of the output parameter</param>
        /// <param name="errMess">error message</param>
        /// <returns></returns>

        protected DataSet ExecuteStoredProcedureDS(string spName, string[] parameterNames, string[] parameterValues, string outputParameterName, ref object parOut, ref string errMess)
        {
            DataSet dataSet = new DataSet();         

            try
            {
                OpenConnection(ref errMess);

                SqlCommand sqlCommand = new SqlCommand(spName, sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                for (int i = 0; i < parameterNames.Length; i++)
                {
                    sqlCommand.Parameters.AddWithValue(parameterNames[i], parameterValues[i]);
                }

                SqlParameter pOut = new SqlParameter();
                pOut.ParameterName = outputParameterName;
                pOut.Direction = ParameterDirection.Output;
                pOut.Size = 30; //a kimeneti parameternek mindenkepp be kell allitani valamilyen meretet
                sqlCommand.Parameters.Add(pOut);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(spName, sqlConnection);

                dataAdapter.SelectCommand = sqlCommand;               

                dataAdapter.Fill(dataSet);

                parOut = pOut.Value;

                errMess = "OK";
            }
            catch (SqlException e)
            {
                errMess = e.Message;
            }
            finally
            {
                CloseConnection();
            }
            return dataSet;
        }
    }
}



