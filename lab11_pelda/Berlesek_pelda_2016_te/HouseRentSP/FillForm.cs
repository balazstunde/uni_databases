﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HouseRent
{
    public partial class FillForm : Form
    {
        private CountriesDAL countriesDAL;
        private HousesDAL housesDAL;
        private string errMess;
        private int errNumber;
       
        public FillForm()
        {
            InitializeComponent();
            string error = string.Empty;
            housesDAL = new HousesDAL(ref error);
            if (error != "OK")
            {
                errNumber = 1;
                errMess = "Error"+errNumber+Environment.NewLine+"Houses objektumot nem tudtam letrehozni. " + error;                
            }
            else
            {
                errMess = "OK";
                countriesDAL = new CountriesDAL();
            }
        }


        /// <summary>
        /// This event occurs before a form is displayed for the first time.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FillForm_Load(object sender, EventArgs e)
        {
            if (errMess == "OK") //letrejott a Houses objektum
            {
                FillCbCountries();

                FillDgvHouses(tbHouseNameFilter.Text, -1);

            }            
        }

        /// <summary>
        /// Fills the country comboboxes with the country list.
        /// </summary>
        /// <param name="combo">specifies which combobox to fill</param>
        private void FillCbCountries()
        {
            string error = string.Empty;
            List<Country> countryList = countriesDAL.GetCountryList(ref error);

            if (error != "OK")
            {
                errNumber++;
                if (errMess == "OK") 
                    errMess = string.Empty;
                errMess = errMess + Environment.NewLine+ 
                    "Error"+errNumber+Environment.NewLine+"Hiba a ComboBox feltoltesenel." + error;
            }
            else
            {
                //fill the cbCountryFilter combobox
                cbCountryFilter.DataSource = countryList;
                //the text to be dispayed as the combobox text
                cbCountryFilter.DisplayMember = "CountryName";
                //the value of the combobox (can be accessed by the selectedValue property)
                cbCountryFilter.ValueMember = "CountryId";
            }
            
        }


        //dvgHouses DataGridView feltoltese adatokkal
        //adatok = a megadott karakterrel/karakterekkel kezdodo nevu es orszagba tartozo nyaralok
        //az OrszagID ervenyes kell legyen (countryID>0)
        private void FillDgvHouses(string HouseName, int countryID)
        {
            string error = string.Empty;
            dgvHouses.Rows.Clear();
            List<House> houseList = housesDAL.GetHouseListDataSet(HouseName, countryID, ref error);

            if ((houseList.Count != 0) && (error == "OK")) //ha van a felteteleknek eleget tevo nyaralo es nem lepett fel hiba,
            //a lista elemeit hozzaadjuk a DataGridView-hoz(lesznek olyan oszlopok/cellak, melyek 
            //nem jelennek meg a DataGridView-ban
            {
                foreach (House item in houseList)
                {
                    try
                    {
                        dgvHouses.Rows.Add(item.HouseId,
                                           item.HouseName,
                                           item.HouseCountry.CountryId,
                                           item.HouseCountry.CountryName,
                                           item.HouseOwner.OwnerId,
                                           item.HouseOwner.OwnerName,
                                           item.HouseOwner.OwnerEmail,
                                           item.Capacity,
                                           item.Price);
                    }
                    catch (Exception ex)
                    {
                        errNumber++;
                        if (errMess == "OK") errMess = string.Empty;
                        errMess = errMess + Environment.NewLine + 
                            "Error"+errNumber+Environment.NewLine+"Invalid data " + ex.Message;
                    }
                }
            }
            else if (error != "OK")
            {
                errNumber++;
                if (errMess == "OK") errMess = string.Empty;
                errMess = errMess + Environment.NewLine + 
                    "Error"+errNumber+Environment.NewLine+"Hiba a DataGridView feltoltesenel." + error;
            }
        }


        //dvgHouses DataGridView feltoltese adatokkal - tarolt eljaras segitsegevel        
        private void FillDgvTextBoxSP(string facilityName, int capacity)
        {
            string error = string.Empty;
            int maxPrice = 0;
            dgvHouses.Rows.Clear();
            List<House> houseList = housesDAL.GetHouseListDataSetSP(facilityName, capacity, ref maxPrice, ref error);
            if ((maxPrice >= 0) && (error == "OK")) //ha van a felteteleknek eleget tevo nyaralo es nem lepett fel hiba,
            //a lista elemeit hozzaadjuk a DataGridView-hoz(lesznek olyan oszlopok/cellak, melyek 
            //nem jelennek meg a DataGridView-ban
            {
                foreach (House item in houseList)
                {
                    try
                    {
                        dgvHouses.Rows.Add(item.HouseId,
                                           item.HouseName,
                                           item.HouseCountry.CountryId,
                                           item.HouseCountry.CountryName,
                                           item.HouseOwner.OwnerId,
                                           item.HouseOwner.OwnerName,
                                           item.HouseOwner.OwnerEmail,
                                           item.Capacity,
                                           item.Price);
                    }
                    catch (Exception ex)
                    {
                        errNumber++;
                        if (errMess == "OK") errMess = string.Empty;
                        errMess = errMess + Environment.NewLine +
                            "Error" + errNumber + Environment.NewLine + "Invalid data " + ex.Message;
                    }
                }
                tbMaxPrice.Text = maxPrice.ToString();
            }
            else if (error != "OK")
            {
                errNumber++;
                if (errMess == "OK") errMess = string.Empty;
                errMess = errMess + Environment.NewLine +
                    "Error" + errNumber + Environment.NewLine + "Hiba a DataGridView feltoltesenel." + error;
            }
            else MessageBox.Show("Nincs megfelelo nyaralo.");
        }
    

        /// <summary>
        /// Filters the houselist based on the HouseName and the selected countryId 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFilter_Click(object sender, EventArgs e)
        {
            //cbCountryFilter.SelectedValue -> countryID
            //cbCountryFilter.SelectedText -> countryName
            FillDgvHouses(tbHouseNameFilter.Text, Convert.ToInt32(cbCountryFilter.SelectedValue));
    
            if (errMess != "OK")
            {
                ErrorForm errorForm = new ErrorForm(errMess);
                errorForm.Show();
                errorForm.Focus();
            }
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Occurs whenever the form is first shown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FillForm_Shown(object sender, EventArgs e)
        {
            if (errMess != "OK")
            {
                ErrorForm errorForm = new ErrorForm(errMess);
                errorForm.Show();
                errorForm.Focus();
            }
        }

        /// <summary>
        /// Filters the houselist based on the HouseName, the selected countryId and on the price's value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFilterSP_Click(object sender, EventArgs e)
        {
            int capacity = -1;
            string error = string.Empty;
            
            try
            {
                capacity = Convert.ToInt32(tbCapacity.Text);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                MessageBox.Show("Nem jo a megadott formatum!" + Environment.NewLine + error);
            }

            FillDgvTextBoxSP(tbFacility.Text, capacity);
           
            if (errMess != "OK")
            {
                ErrorForm errorForm = new ErrorForm(errMess);
                errorForm.Show();
                errorForm.Focus();
            }
        }


    }
}
