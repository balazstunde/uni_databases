﻿namespace HouseRent
{
    partial class FillForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbHouseName = new System.Windows.Forms.Label();
            this.tbHouseNameFilter = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnFilter = new System.Windows.Forms.Button();
            this.dgvHouses = new System.Windows.Forms.DataGridView();
            this.houseId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.houseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ownerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ownerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OwnerEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.houseCapacity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbCountryFilter = new System.Windows.Forms.ComboBox();
            this.lbCountryName = new System.Windows.Forms.Label();
            this.btFilterSP = new System.Windows.Forms.Button();
            this.lbCapacity = new System.Windows.Forms.Label();
            this.tbCapacity = new System.Windows.Forms.TextBox();
            this.gb_Search = new System.Windows.Forms.GroupBox();
            this.lbFacility = new System.Windows.Forms.Label();
            this.tbFacility = new System.Windows.Forms.TextBox();
            this.gb_Filter = new System.Windows.Forms.GroupBox();
            this.lbMaxPrice = new System.Windows.Forms.Label();
            this.tbMaxPrice = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHouses)).BeginInit();
            this.gb_Search.SuspendLayout();
            this.gb_Filter.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbHouseName
            // 
            this.lbHouseName.AutoSize = true;
            this.lbHouseName.Location = new System.Drawing.Point(21, 47);
            this.lbHouseName.Name = "lbHouseName";
            this.lbHouseName.Size = new System.Drawing.Size(100, 20);
            this.lbHouseName.TabIndex = 31;
            this.lbHouseName.Text = "Nyaraló neve";
            // 
            // tbHouseNameFilter
            // 
            this.tbHouseNameFilter.Location = new System.Drawing.Point(142, 46);
            this.tbHouseNameFilter.Name = "tbHouseNameFilter";
            this.tbHouseNameFilter.Size = new System.Drawing.Size(171, 26);
            this.tbHouseNameFilter.TabIndex = 30;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(964, 484);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(112, 35);
            this.btnExit.TabIndex = 28;
            this.btnExit.Text = "Kilép";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(347, 87);
            this.btnFilter.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(112, 35);
            this.btnFilter.TabIndex = 27;
            this.btnFilter.Text = "Szűkít";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // dgvHouses
            // 
            this.dgvHouses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHouses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.houseId,
            this.houseName,
            this.countryId,
            this.countryName,
            this.ownerId,
            this.ownerName,
            this.OwnerEmail,
            this.houseCapacity,
            this.price});
            this.dgvHouses.Location = new System.Drawing.Point(42, 181);
            this.dgvHouses.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvHouses.Name = "dgvHouses";
            this.dgvHouses.ReadOnly = true;
            this.dgvHouses.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHouses.Size = new System.Drawing.Size(1034, 277);
            this.dgvHouses.TabIndex = 26;
            // 
            // houseId
            // 
            this.houseId.HeaderText = "Id";
            this.houseId.Name = "houseId";
            this.houseId.ReadOnly = true;
            this.houseId.Visible = false;
            // 
            // houseName
            // 
            this.houseName.HeaderText = "Nev";
            this.houseName.Name = "houseName";
            this.houseName.ReadOnly = true;
            // 
            // countryId
            // 
            this.countryId.HeaderText = "OrszagID";
            this.countryId.Name = "countryId";
            this.countryId.ReadOnly = true;
            this.countryId.Visible = false;
            // 
            // countryName
            // 
            this.countryName.HeaderText = "Orszag";
            this.countryName.Name = "countryName";
            this.countryName.ReadOnly = true;
            // 
            // ownerId
            // 
            this.ownerId.HeaderText = "TulajID";
            this.ownerId.Name = "ownerId";
            this.ownerId.ReadOnly = true;
            this.ownerId.Visible = false;
            // 
            // ownerName
            // 
            this.ownerName.HeaderText = "Tulajdonos";
            this.ownerName.Name = "ownerName";
            this.ownerName.ReadOnly = true;
            // 
            // OwnerEmail
            // 
            this.OwnerEmail.HeaderText = "TulajEmail";
            this.OwnerEmail.Name = "OwnerEmail";
            this.OwnerEmail.ReadOnly = true;
            this.OwnerEmail.Visible = false;
            // 
            // houseCapacity
            // 
            this.houseCapacity.HeaderText = "Ferohely";
            this.houseCapacity.Name = "houseCapacity";
            this.houseCapacity.ReadOnly = true;
            // 
            // price
            // 
            this.price.HeaderText = "Ar";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // cbCountryFilter
            // 
            this.cbCountryFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCountryFilter.FormattingEnabled = true;
            this.cbCountryFilter.Location = new System.Drawing.Point(142, 92);
            this.cbCountryFilter.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbCountryFilter.Name = "cbCountryFilter";
            this.cbCountryFilter.Size = new System.Drawing.Size(171, 28);
            this.cbCountryFilter.TabIndex = 25;
            // 
            // lbCountryName
            // 
            this.lbCountryName.AutoSize = true;
            this.lbCountryName.Location = new System.Drawing.Point(21, 95);
            this.lbCountryName.Name = "lbCountryName";
            this.lbCountryName.Size = new System.Drawing.Size(98, 20);
            this.lbCountryName.TabIndex = 32;
            this.lbCountryName.Text = "Ország neve";
            // 
            // btFilterSP
            // 
            this.btFilterSP.Location = new System.Drawing.Point(239, 85);
            this.btFilterSP.Name = "btFilterSP";
            this.btFilterSP.Size = new System.Drawing.Size(92, 34);
            this.btFilterSP.TabIndex = 34;
            this.btFilterSP.Text = "SzűkítTE";
            this.btFilterSP.UseVisualStyleBackColor = true;
            this.btFilterSP.Click += new System.EventHandler(this.btnFilterSP_Click);
            // 
            // lbCapacity
            // 
            this.lbCapacity.AutoSize = true;
            this.lbCapacity.Location = new System.Drawing.Point(19, 96);
            this.lbCapacity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbCapacity.Name = "lbCapacity";
            this.lbCapacity.Size = new System.Drawing.Size(83, 20);
            this.lbCapacity.TabIndex = 38;
            this.lbCapacity.Text = "Férőhely <";
            // 
            // tbCapacity
            // 
            this.tbCapacity.Location = new System.Drawing.Point(115, 93);
            this.tbCapacity.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbCapacity.Name = "tbCapacity";
            this.tbCapacity.Size = new System.Drawing.Size(73, 26);
            this.tbCapacity.TabIndex = 36;
            // 
            // gb_Search
            // 
            this.gb_Search.Controls.Add(this.tbMaxPrice);
            this.gb_Search.Controls.Add(this.lbMaxPrice);
            this.gb_Search.Controls.Add(this.lbFacility);
            this.gb_Search.Controls.Add(this.tbFacility);
            this.gb_Search.Controls.Add(this.tbCapacity);
            this.gb_Search.Controls.Add(this.lbCapacity);
            this.gb_Search.Controls.Add(this.btFilterSP);
            this.gb_Search.Location = new System.Drawing.Point(546, 20);
            this.gb_Search.Name = "gb_Search";
            this.gb_Search.Size = new System.Drawing.Size(530, 137);
            this.gb_Search.TabIndex = 39;
            this.gb_Search.TabStop = false;
            this.gb_Search.Text = "Keresés tárolt eljárással";
            // 
            // lbFacility
            // 
            this.lbFacility.AutoSize = true;
            this.lbFacility.Location = new System.Drawing.Point(19, 44);
            this.lbFacility.Name = "lbFacility";
            this.lbFacility.Size = new System.Drawing.Size(90, 20);
            this.lbFacility.TabIndex = 40;
            this.lbFacility.Text = "Felszerelés";
            // 
            // tbFacility
            // 
            this.tbFacility.Location = new System.Drawing.Point(115, 41);
            this.tbFacility.Name = "tbFacility";
            this.tbFacility.Size = new System.Drawing.Size(115, 26);
            this.tbFacility.TabIndex = 39;
            // 
            // gb_Filter
            // 
            this.gb_Filter.Controls.Add(this.lbCountryName);
            this.gb_Filter.Controls.Add(this.lbHouseName);
            this.gb_Filter.Controls.Add(this.tbHouseNameFilter);
            this.gb_Filter.Controls.Add(this.btnFilter);
            this.gb_Filter.Controls.Add(this.cbCountryFilter);
            this.gb_Filter.Location = new System.Drawing.Point(40, 21);
            this.gb_Filter.Name = "gb_Filter";
            this.gb_Filter.Size = new System.Drawing.Size(485, 135);
            this.gb_Filter.TabIndex = 40;
            this.gb_Filter.TabStop = false;
            this.gb_Filter.Text = "Keresés select utasítással";
            // 
            // lbMaxPrice
            // 
            this.lbMaxPrice.AutoSize = true;
            this.lbMaxPrice.Location = new System.Drawing.Point(365, 65);
            this.lbMaxPrice.Name = "lbMaxPrice";
            this.lbMaxPrice.Size = new System.Drawing.Size(129, 20);
            this.lbMaxPrice.TabIndex = 41;
            this.lbMaxPrice.Text = "Legmagasabb ár";
            // 
            // tbMaxPrice
            // 
            this.tbMaxPrice.Enabled = false;
            this.tbMaxPrice.Location = new System.Drawing.Point(427, 97);
            this.tbMaxPrice.Name = "tbMaxPrice";
            this.tbMaxPrice.Size = new System.Drawing.Size(67, 26);
            this.tbMaxPrice.TabIndex = 42;
            // 
            // FillForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 563);
            this.Controls.Add(this.gb_Filter);
            this.Controls.Add(this.gb_Search);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.dgvHouses);
            this.Name = "FillForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FillForm";
            this.Load += new System.EventHandler(this.FillForm_Load);
            this.Shown += new System.EventHandler(this.FillForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHouses)).EndInit();
            this.gb_Search.ResumeLayout(false);
            this.gb_Search.PerformLayout();
            this.gb_Filter.ResumeLayout(false);
            this.gb_Filter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbHouseName;
        private System.Windows.Forms.TextBox tbHouseNameFilter;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DataGridView dgvHouses;
        private System.Windows.Forms.ComboBox cbCountryFilter;
        private System.Windows.Forms.Label lbCountryName;
        private System.Windows.Forms.Button btFilterSP;
        private System.Windows.Forms.Label lbCapacity;
        private System.Windows.Forms.TextBox tbCapacity;
        private System.Windows.Forms.GroupBox gb_Search;
        private System.Windows.Forms.Label lbFacility;
        private System.Windows.Forms.TextBox tbFacility;
        private System.Windows.Forms.GroupBox gb_Filter;
        private System.Windows.Forms.DataGridViewTextBoxColumn houseId;
        private System.Windows.Forms.DataGridViewTextBoxColumn houseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn OwnerEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn houseCapacity;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.TextBox tbMaxPrice;
        private System.Windows.Forms.Label lbMaxPrice;

    }
}