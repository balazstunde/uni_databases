--t.e., mely a parameterkent megadott felszerelessel es ugyancsak parameterkent megadott ferohelynel kevesebbel rendelkezo 
--nyaralok adatait teriti vissza
--ha van a felteteleknek eleget tevo nyaralo, a kimeneti parameter erteke ezek kozul a legnagyobb arral rendelkezo nyaralo berlesi ara, 
--ekkor a visszateritesi ertek 0. Ha nincs ilyen nyaralo, a visszateritesi ertek -1, a kimeneti parameter erteke pedig 
--az adatbazisban levo nyaralok kozul a legalacsonyabb arral rendelkezo nyaralo berlesi ara! 
IF (OBJECT_ID(N'sp_SearchHouses') IS NOT NULL)
  DROP PROCEDURE  sp_SearchHouses
GO
CREATE PROCEDURE sp_SearchHouses(@pFacilityName VARCHAR(30), @pCapacity INT, @pOut INT OUT)
AS
BEGIN
SET NOCOUNT ON
SELECT ny.NyaraloID INTO #tmp1
FROM Nyaralok ny, Felszerelesek f, Felszerelesei fi
WHERE fi.NyaraloID=ny.NyaraloID AND f.FelszerelesID=fi.FelszerelesID 
	  AND FelszerelesNev = @pFacilityName 
	  AND Ferohely<@pCapacity

DECLARE @rowcount INT=@@ROWCOUNT, @error INT=@@ERROR;

IF (@error<>0) 
	SET @pOut = -2 --valamilyen hiba lepett fel a select utasitas vegrehajtasa kozben
  ELSE IF (@rowcount=0)
	SET @pOut = -1 -- ha nincs megfelelo nyaralo
    ELSE
	BEGIN
	SELECT ny.NyaraloID, NyaraloNev,OrszagNev, Nev, Ferohely, Ar INTO #tmp2	
	FROM Nyaralok ny, Orszagok o, Tulajdonosok t, #tmp1 t1
	WHERE ny.orszagid=o.orszagid AND ny.TulajID=t.TulajID AND ny.NyaraloID=t1.NyaraloID
	SET @error=@@ERROR
	IF (@error=0)
		BEGIN 
		SELECT * FROM #tmp2
		SET @pOut = (SELECT MAX(Ar) FROM #tmp2)
		END
    ELSE 
	   SET @pOut = -2 --valamilyen hiba lepett fel a select utasitas vegrehajtasa kozben
    END
END
GO

DECLARE @pOut INT
EXEC sp_SearchHouses 'terasz',10, @pOut OUT
SELECT @pOut AS pOut

