/*Balazs Eva-Tunde, beim1596*/
/*1. a. Sz�rjuk be a Nepszeruseg mez�t a Filmek t�bl�ba, ahol a Nepszeruseg-INT t�pus� mez�, 0-5 k�z�tti �rt�keket vehet fel, alap�rtelmez�s szerinti �rt�ke 0.

b. �rjunk t�rolt elj�r�st, mely minden film eset�n be�ll�tja a Nepszeruseg mez� �rt�k�t a film vet�t�sei alapj�n:

Vet�t�sek sz�ma < = 2  �> Nepszeruseg = 1;
Vet�t�sek sz�ma > 2 �s <= 3  �> Nepszeruseg = 2;
Vet�t�sek sz�ma  > 3 �s <= 5   �> Nepszeruseg = 3;
Vet�t�sek sz�ma  > 5 �s <= 6   �> Nepszeruseg = 4;
Vet�t�sek sz�ma  > 6 �> Nepszeruseg = 5.
Megj. Haszn�ljunk kurzort!*/
ALTER TABLE Filmek
ADD Nepszeruseg INT NOT NULL CONSTRAINT btw_0_5 CHECK (Nepszeruseg>=1 AND Nepszeruseg<=5) DEFAULT 0

GO
CREATE PROCEDURE NepszBeallit
AS
BEGIN
SET NOCOUNT ON
	DECLARE c CURSOR FOR
		SELECT Filmek.FilmID, COUNT(Vetites.VetitesID) AS Vetitve
		FROM Filmek,Vetites 
		WHERE Filmek.FilmID=Vetites.FilmID
		GROUP BY Filmek.FilmID 
	OPEN  c 
	DECLARE @FID INT, @VSz INT
	FETCH NEXT FROM c INTO @FID,@VSz
	WHILE @@FETCH_STATUS=0
		BEGIN
			IF @VSz <= 2 
				BEGIN
					UPDATE Filmek
					SET Nepszeruseg=1
					WHERE Filmek.FilmID=@FID
				END
			ELSE 
				BEGIN
					IF @VSz <=3 
						BEGIN
							UPDATE Filmek
							SET Nepszeruseg=2
							WHERE Filmek.FilmID=@FID
						END
					ELSE
						BEGIN
							IF @VSz <=5
								BEGIN
									UPDATE Filmek
									SET Nepszeruseg=3
									WHERE Filmek.FilmID=@FID
								END
							ELSE
								BEGIN
									IF @VSz <=6
										BEGIN
											UPDATE Filmek
											SET Nepszeruseg=4
											WHERE Filmek.FilmID=@FID
										END
									ELSE
										BEGIN
											UPDATE Filmek
											SET Nepszeruseg=5
											WHERE Filmek.FilmID=@FID
										END
								END
						END
				END
			FETCH NEXT FROM c INTO @FID, @VSz
		END
	CLOSE c
	DEALLOCATE c
END
GO
EXEC NepszBeallit
DROP PROCEDURE NepszBeallit
/*2.  �rjunk t�rolt elj�r�st, amelynek bemen� param�tere: @SzineszSzam (int t�pus�)! A t�rolt elj�r�s minden filmnek a k�lts�g�t n�veli 5%-kal, amelyben legal�bb annyi (>) szin�sz szerepel, mint @SzineszSzam!*/
GO
CREATE PROCEDURE koltsegnoveles
	(@pSzinszSzam INT)
AS
BEGIN
	DECLARE c CURSOR FOR
		SELECT Filmek.FilmID, COUNT(Szerepel.SzineszID) AS SzinSzam
		FROM Filmek, Szerepel
		WHERE Szerepel.FilmID=FIlmek.FilmID
		GROUP BY Filmek.FilmID
	OPEN c
	DECLARE @FID INT, @SzSz INT, @Koltseg INT
	FETCH NEXT FROM c INTO @FID, @SzSz
	WHILE @@FETCH_STATUS=0
	BEGIN
		IF @SzSz>@pSzinszSzam
			BEGIN
				SET @Koltseg = (SELECT Filmek.Koltseg FROM Filmek WHERE Filmek.FilmID=@FID)
				UPDATE Filmek
				SET Koltseg = @Koltseg*1.05
				WHERE Filmek.FilmID = @FID
			END
		FETCH NEXT FROM c INTO @FID, @SzSz
	END
	CLOSE c
	DEALLOCATE c
END

EXEC koltsegnoveles 3
DROP PROCEDURE koltsegnoveles
/*3.  3.  �rjunk t�rolt elj�r�st, amely meghat�rozza azokat a filmc�meket, amelyekben legal�bb azok a sz�n�szek szerepelnek, 
mint a param�terk�nt megadott filmc�m (@Filmcim varchar(30) t�pus�)! Ha nincs ilyen film, sz�rjunk be megfelel� �rt�keket! */
GO
CREATE PROCEDURE hasonlofilmek
	(@Filmcim VARCHAR(30))
AS
BEGIN
	SELECT Filmek.FilmCim, COUNT(Szerepel.SzineszID) AS Szineszek_szama 
	FROM Filmek, Szerepel
	WHERE Filmek.FilmID=Szerepel.FilmID AND Szerepel.SzineszID IN (SELECT Szerepel.SzineszID FROM Szerepel, Filmek WHERE Filmek.FilmID=Szerepel.FilmID AND Filmek.FilmCim=@Filmcim) AND Filmek.FilmCim != @Filmcim
	GROUP BY Filmek.FilmID, Filmek.FilmCim
	HAVING COUNT(Szerepel.SzineszID)>=(SELECT COUNT(SzineszID) FROM Szerepel, Filmek WHERE Filmek.FilmID=Szerepel.FilmID AND Filmek.FilmCim=@Filmcim)
END
insert into Szerepel (SzineszID, FilmID) values (18, 11);
EXEC hasonlofilmek 'It''s Complicated'
DROP PROCEDURE hasonlofilmek
/*4.  �rjunk Insert illetve Delete triggert, mely a Vetites t�bl�ba val� besz�r�s, illetve t�rl�s eset�n aktualiz�lja 
a Filmek t�bla Nepszeruseg mez�j�nek �rt�k�t! Oldjuk meg a feladatot k�tf�lek�ppen: 
felt�telezve, hogy egyszerre (1) csak egy sort, (2) t�bb sort sz�rhatunk be/t�r�lhet�nk a t�bl�ba/t�bl�b�l!*/
GO
CREATE TRIGGER Tr On Vetites
AFTER INSERT, DELETE AS
BEGIN
	DECLARE @VSz INT, @FID INT
IF NOT EXISTS (SELECT * FROM deleted)
BEGIN
	SET @FID = (SELECT inserted.FilmID FROM inserted)
	SET @VSz = (SELECT COUNT(Vetites.VetitesID) FROM Vetites WHERE  FilmID=@FID)
END
ELSE
BEGIN
	SET @FID = (SELECT deleted.FilmID FROM deleted)
	SET @VSz = (SELECT COUNT(Vetites.VetitesID) FROM Vetites WHERE  FilmID=@FID)
END
	IF @VSz <= 2
				BEGIN
					UPDATE Filmek
					SET Nepszeruseg=1
					WHERE Filmek.FilmID=@FID
				END
		ELSE 
			BEGIN
				IF @VSz <=3 
					BEGIN
						UPDATE Filmek
						SET Nepszeruseg=2
						WHERE Filmek.FilmID=@FID
					END
				ELSE
					BEGIN
						IF @VSz <=5
							BEGIN
								UPDATE Filmek
								SET Nepszeruseg=3
								WHERE Filmek.FilmID=@FID
							END
						ELSE
							BEGIN
								IF @VSz <=6
									BEGIN
										UPDATE Filmek
										SET Nepszeruseg=4
										WHERE Filmek.FilmID=@FID
									END
								ELSE
									BEGIN
										UPDATE Filmek
										SET Nepszeruseg=5
										WHERE Filmek.FilmID=@FID
									END
							END
					END
			END
END


GO
CREATE TRIGGER Tr_m On Vetites
AFTER INSERT, DELETE AS
BEGIN
	DECLARE @VSz INT, @FID INT, @VID INT
IF NOT EXISTS (SELECT * FROM deleted)
BEGIN
	DECLARE c CURSOR FOR SELECT inserted.FilmID, inserted.VetitesID FROM inserted
	OPEN c
	FETCH NEXT FROM c INTO @FID,@VID
	WHILE @@FETCH_STATUS=0
	BEGIN	
		SET @VSz = (SELECT COUNT(Vetites.VetitesID) FROM Vetites WHERE  FilmID=@FID)
		IF @VSz <= 2
				BEGIN
					UPDATE Filmek
					SET Nepszeruseg=1
					WHERE Filmek.FilmID=@FID
				END
		ELSE 
			BEGIN
				IF @VSz <=3 
					BEGIN
						UPDATE Filmek
						SET Nepszeruseg=2
						WHERE Filmek.FilmID=@FID
					END
				ELSE
					BEGIN
						IF @VSz <=5
							BEGIN
								UPDATE Filmek
								SET Nepszeruseg=3
								WHERE Filmek.FilmID=@FID
							END
						ELSE
							BEGIN
								IF @VSz <=6
									BEGIN
										UPDATE Filmek
										SET Nepszeruseg=4
										WHERE Filmek.FilmID=@FID
									END
								ELSE
									BEGIN
										UPDATE Filmek
										SET Nepszeruseg=5
										WHERE Filmek.FilmID=@FID
									END
							END
					END
			END
	FETCH NEXT FROM c INTO @FID, @VID
	END
END
ELSE
BEGIN
	DECLARE c CURSOR FOR SELECT deleted.FilmID, deleted.VetitesID FROM deleted 
	OPEN c
	FETCH NEXT FROM c INTO @FID,@VID
	WHILE @@FETCH_STATUS=0
	BEGIN	
		SET @VSz = (SELECT COUNT(Vetites.VetitesID) FROM Vetites WHERE  FilmID=@FID)
		IF @VSz <= 2
				BEGIN
					UPDATE Filmek
					SET Nepszeruseg=1
					WHERE Filmek.FilmID=@FID
				END
		ELSE 
			BEGIN
				IF @VSz <=3 
					BEGIN
						UPDATE Filmek
						SET Nepszeruseg=2
						WHERE Filmek.FilmID=@FID
					END
				ELSE
					BEGIN
						IF @VSz <=5
							BEGIN
								UPDATE Filmek
								SET Nepszeruseg=3
								WHERE Filmek.FilmID=@FID
							END
						ELSE
							BEGIN
								IF @VSz <=6
									BEGIN
										UPDATE Filmek
										SET Nepszeruseg=4
										WHERE Filmek.FilmID=@FID
									END
								ELSE
									BEGIN
										UPDATE Filmek
										SET Nepszeruseg=5
										WHERE Filmek.FilmID=@FID
									END
							END
					END
			END
	FETCH NEXT FROM c INTO @FID, @VID
	END
END
	CLOSE c
	DEALLOCATE c
END
/*5.  �rjunk DELETE triggert, amely nem enged t�r�lni olyan filmet, melynek 2-n�l t�bb szerepl�je van!*/
GO
CREATE TRIGGER Del_Film ON Filmek
INSTEAD OF DELETE AS
DECLARE @MSz INT
SET @MSz = (SELECT COUNT(Szerepel.SzineszID) FROM Szerepel,deleted WHERE Szerepel.FilmID=deleted.FilmID)
IF @MSz<=2 
BEGIN
	DELETE FROM Filmek
	WHERE FilmID IN (SELECT deleted.FilmID FROM deleted)
END

/*6.  �rjunk INSERT, UPDATE �s DELETE triggert a Filmek t�bl�ra vonatkoz�an, amely csak akkor enged�lyezi a m�velet v�grehajt�s�t, 
ha a film k�lts�ge  5000 �s 1500000 k�z�tt marad!
	Megj. Felt�telezz�k, hogy egyszerre csak egy sor�t m�dos�tjuk a t�bl�nak!*/
GO
DROP TRIGGER Del_Film
GO
CREATE TRIGGER filmkoltseg ON Filmek
INSTEAD OF DELETE, UPDATE, INSERT AS
BEGIN
	DECLARE @MSz INT
	IF  EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted)
	BEGIN
		SET @MSz = (SELECT Filmek.Koltseg FROM Filmek, deleted WHERE Filmek.FilmID=deleted.FilmID)
		IF @Msz<1500000 AND @Msz>5000
		BEGIN
			/*DELETE FROM Filmek WHERE FilmID IN (SELECT deleted.FilmID FROM deleted)*/
			INSERT INTO Filmek (FilmID,FilmCim, Koltseg,MegjEv,Nepszeruseg,MufajID,StudioID) VALUES ((SELECT FilmID FROM inserted),(SELECT FilmCim FROM inserted),(SELECT Koltseg FROM inserted),(SELECT MegjEv FROM inserted),(SELECT Nepszeruseg FROM inserted),(SELECT MufajID FROM inserted),(SELECT StudioID FROM inserted))
		END
	END
	ELSE
	BEGIN
			IF EXISTS (SELECT * FROM deleted)
			BEGIN
				SET @MSz = (SELECT Filmek.Koltseg FROM Filmek, deleted WHERE Filmek.FilmID=deleted.FilmID)
				IF @Msz<1500000 AND @Msz>5000
				BEGIN
					DELETE FROM Filmek WHERE FilmID IN (SELECT deleted.FilmID FROM deleted)
				END
			END
			/*ELSE
			BEGIN
				SET @MSz = (SELECT Filmek.Koltseg FROM Filmek, deleted WHERE Filmek.FilmID=deleted.FilmID)
				IF @Msz<1500000 AND @Msz>5000
				BEGIN
					DELETE FROM Filmek WHERE FilmID IN (SELECT deleted.FilmID FROM deleted)
				END
			END*/
	END
END

/*7.  �rjunk DELETE triggert, amely a Vetitesek t�bl�b�l val� t�rl�s eset�n t�rli a megfelel� kapcsolatokat is, majd besz�r egy �j sz�n�szt a k�vetkez�k�ppen:

-Nev: t�r�lt vetit�s filmc�m�nek els� 4 bet�je + a d�tum�nak napja (konkaten�lva),

-Szuldatum: a t�r�lt vetites d�tuma -20 �v,

-Szerepel t�bla eset�n: FIlmID-azon film ID-ja, amelyet a legt�bbsz�r vet�tett�k; Szin�szID-a t�r�lt film szerepl�i k�z�l annak a szin�sznek az ID-ja, aki szerepelt a legkisebb k�lts�gvet�s� filmben. Ha nincs ilyen sz�n�sz, v�lasszunk ki egyet a sz�n�szek k�z�l.

Megj. Felt�telezz�k, hogy egyszerre csak egy sor�t m�dos�tjuk a t�bl�nak!*/
GO
CREATE TRIGGER Del_Vet_Insert ON Vetites 
AFTER DELETE
AS
BEGIN
 DECLARE @Nev VARCHAR (30), @Nap INT, @Datum VARCHAR(30), @Ev INT, @FID INT, @SzID INT
 SET @Nev = (SELECT Filmek.FilmCim FROM Filmek,deleted WHERE Filmek.FilmID=deleted.FilmID)
 SET @Nev = SUBSTRING(@Nev,1,4)
 SET @Nap = (SELECT DAY(deleted.Datum) FROM deleted)
 SET @Nev = CONCAT(@Nev, @Nap)

 SET @Datum = (SELECT Datum FROM deleted)
 SET @Ev = (SELECT YEAR(Datum) FROM deleted)
 SET @Ev = @Ev-20
 SET @Datum = SUBSTRING(@Datum,5,LEN(@Datum)-4)
 SET @Datum = CONCAT(@Ev,@Datum)

 SELECT Filmek.FilmID, COUNT(Vetites.VetitesID) AS Vetitve INTO #temp1 FROM Filmek, Vetites WHERE Filmek.FilmID=Vetites.FilmID GROUP BY Filmek.FilmID
 SET @FID = (SELECT FilmID FROM #temp1 WHERE #temp1.Vetitve=(SELECT MAX(Vetitve) FROM #temp1))

 SET @SzID = (SELECT TOP 1 Szineszek.SzineszID FROM Szineszek)

 INSERT INTO Szineszek (SzineszID, SzineszNev, SzulDatum) values (@SzID,@Nev,@Datum)
 INSERT INTO Szerepel (SzineszID, FilmID) VALUES (@SzID, @FID)
END
/*8. a. Hozzunk l�tre egy �j t�bl�t:

  FilmekKoltseg_Log(ID, Idopont, Muvelet, FilmID, FilmCim, RegiKoltseg, UjKoltseg),

ahol Idopont-datetime, Muvelet-varchar(50), RegiKoltseg-int, UjKoltseg-int t�pus� mez�k; 
FilmID - a Filmek t�bl�b�l val� t�rl�s eset�n �rt�ke NULL-ra m�dosuljon.

b. �rjunk UPDATE triggert, mely a Filmek t�bla Koltseg mez�j�nek m�dos�t�sakor besz�r egy �j sort a FilmekKoltseg_Log t�bl�ba. 
Az id�pont legyen a m�dos�t�s id�pontja, m�velet sz�vege: �koltseg n�vel�se� vagy �koltseg cs�kkent�se�, Regikoltseg-r�gi �rt�k, 
Ujkoltseg-�j �rt�k.

Oldjuk meg a feladatot k�tf�lek�ppen: felt�telezve, hogy egyszerre (1) csak egy sort, (2) t�bb sort m�dos�thatunk a t�bl�ban!*/

CREATE TABLE FilmekKoltseg_Log(
	ID INT IDENTITY PRIMARY KEY,
	Idopont DATETIME,
	Muvelet VARCHAR(50),
	RegiKoltseg INT,
	UjKoltseg INT,
	FilmID INT FOREIGN KEY REFERENCES Filmek(FilmID)
)

GO
CREATE TRIGGER torolFilm
ON Filmek
AFTER DELETE AS
	DECLARE @FID INT = (SELECT deleted.FilmID FROM deleted);
	UPDATE FilmekKoltseg_Log SET FilmID = NULL WHERE FilmID = @FID;
GO
DROP TRIGGER filmkoltseg
GO
CREATE TRIGGER up_Log
ON Filmek
INSTEAD OF UPDATE AS
BEGIN
	IF (UPDATE (Koltseg))
	BEGIN
		DECLARE @rKoltseg INT, @uKoltseg INT, @FID INT, @muv VARCHAR(20)
		SET @FID = (SELECT FilmID FROM inserted)
		SET @rKoltseg = (SELECT Koltseg FROM Filmek WHERE Filmek.FilmID = @FID)
		SET @uKoltseg = (SELECT Koltseg FROM inserted)
		IF(@rKoltseg < @uKoltseg)
		BEGIN
			SET @muv = 'koltseg novelese'
		END
		ELSE
		BEGIN
			SET @muv = 'koltseg csokkenese'
		END
		INSERT INTO FilmekKoltseg_Log (Idopont, Muvelet, RegiKoltseg, UjKoltseg, FilmID) VALUES(GETDATE(),@muv,@rKoltseg,@uKoltseg,@FID)
		INSERT INTO Filmek (FilmID,FilmCim, Koltseg,MegjEv,Nepszeruseg,MufajID,StudioID) VALUES ((SELECT FilmID FROM inserted),(SELECT FilmCim FROM inserted),(SELECT Koltseg FROM inserted),(SELECT MegjEv FROM inserted),(SELECT Nepszeruseg FROM inserted),(SELECT MufajID FROM inserted),(SELECT StudioID FROM inserted))
	END
END
DROP TRIGGER up_Log
GO
CREATE TRIGGER up_Log1
ON Filmek
INSTEAD OF UPDATE AS
BEGIN
	DECLARE c CURSOR FOR (SELECT inserted.FilmID, inserted.FilmCim, inserted.Koltseg, inserted.MegjEv, inserted.Nepszeruseg, inserted.MufajID, inserted.StudioID FROM inserted)
	DECLARE @rKoltseg INT, @uKoltseg INT, @FID INT, @muv VARCHAR(20), @FilmCim VARCHAR(30), @Ev INT, @Nepszeruseg INT, @SID INT, @MID INT
	OPEN c
	FETCH NEXT FROM c INTO @FID, @FilmCim, @uKoltseg, @Ev, @Nepszeruseg, @MID, @SID
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @rKoltseg = (SELECT Koltseg FROM Filmek WHERE Filmek.FilmID = @FID)
		IF(@rKoltseg < @uKoltseg)
		BEGIN
			SET @muv = 'koltseg novelese'
		END
		ELSE
		BEGIN
			SET @muv = 'koltseg csokkenese'
		END
		INSERT INTO FilmekKoltseg_Log (Idopont, Muvelet, RegiKoltseg, UjKoltseg, FilmID) VALUES(GETDATE(),@muv,@rKoltseg,@uKoltseg,@FID)
		INSERT INTO Filmek (FilmID,FilmCim, Koltseg,MegjEv,Nepszeruseg,MufajID,StudioID) VALUES (@FID,@FilmCim,@uKoltseg,@Ev, @Nepszeruseg, @MID, @SID)
		FETCH NEXT FROM c INTO @FID, @FilmCim, @uKoltseg, @Ev, @Nepszeruseg, @MID, @SID
	END
	CLOSE c
	DEALLOCATE C
END
GO


