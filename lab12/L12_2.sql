USE master;
GO
IF EXISTS(select * from sys.databases where name='Lab12_2')
	DROP DATABASE Lab12_2

CREATE DATABASE Lab12_2;
GO
USE Lab12_2;
GO

CREATE TABLE Szineszek(
	SzineszID INT PRIMARY KEY,
	SzineszNev VARCHAR(30),
	SzulDatum DATE)
CREATE TABLE Mufajok(
	MufajID INT PRIMARY KEY,
	MufajNev VARCHAR(30) UNIQUE)
CREATE TABLE Studiok(
	StudioID INT PRIMARY KEY,
	StudioNev VARCHAR(30),
	StudioCim VARCHAR(30) DEFAULT 'UNKNOWN')
CREATE TABLE Filmek(
	FilmID INT PRIMARY KEY,
	FilmCim VARCHAR(30),
	Koltseg INT,
	MegjEv INT,
	StudioID INT FOREIGN KEY REFERENCES Studiok(StudioID),
	MufajID INT FOREIGN KEY REFERENCES Mufajok(MufajID))
CREATE TABLE Szerepel(
	SzineszID INT FOREIGN KEY REFERENCES Szineszek(SzineszID),
	FilmID INT FOREIGN KEY REFERENCES Filmek(FilmID),
	PRIMARY KEY(SzineszID, FilmID))
CREATE TABLE Vetites(
	VetitesID INT PRIMARY KEY,
	FilmID INT FOREIGN KEY REFERENCES Filmek(FilmID),
	Datum DATE)

insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (1, 'Derrik Charkham', '1923-07-17');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (2, 'Ansell Hembery', '1979-09-18');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (3, 'Kipp Maeer', '1970-06-18');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (4, 'Shaina Worge', '2001-05-02');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (5, 'Vinita Lissimore', '1958-09-12');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (6, 'Rayna Mothersdale', '1985-01-09');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (7, 'Sosanna Triplett', '1952-07-15');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (8, 'Iosep Abrahamsson', '1993-12-08');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (9, 'Keith Balmer', '1970-08-23');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (10, 'Mort Yggo', '1933-12-05');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (11, 'Jolynn Andreasen', '1995-05-12');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (12, 'Dina Jeffcock', '1988-01-28');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (13, 'Rozella Willcot', '1971-01-19');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (14, 'Elvira Sever', '1934-11-15');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (15, 'Lillis Shadrack', '1950-02-15');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (16, 'Bell Lightbody', '1984-03-04');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (17, 'Elden MacCorley', '1978-12-05');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (18, 'Annalee Masterton', '1935-11-03');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (19, 'Lonna Klaassens', '1956-10-23');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (20, 'Boote Coker', '1993-02-27');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (21, 'Margaretha Buddell', '1997-07-21');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (22, 'Wadsworth Fludgate', '1929-09-26');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (23, 'Davidde Reddel', '1964-03-16');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (24, 'Aleksandr Broadbere', '1982-10-26');
insert into Szineszek (SzineszID, SzineszNev, SzulDatum) values (25, 'Husain Hodcroft', '1945-05-12');

insert into Mufajok (MufajID, MufajNev) values (1, 'Drama')
insert into Mufajok (MufajID, MufajNev) values (2, 'Sci-fi')
insert into Mufajok (MufajID, MufajNev) values (3, 'Comedy')
insert into Mufajok (MufajID, MufajNev) values (4, 'Horror')

insert into Studiok (StudioID, StudioNev, StudioCim) values (1, 'Heathcote-Frami', '42 Duke Drive');
insert into Studiok (StudioID, StudioNev, StudioCim) values (2, 'Orn-Hickle', '64448 Springs Trail');
insert into Studiok (StudioID, StudioNev, StudioCim) values (3, 'Gleichner-Okuneva', '099 Declaration Lane');
insert into Studiok (StudioID, StudioNev, StudioCim) values (4, 'Mayert-Luettgen', '15 Prairie Rose Street');
insert into Studiok (StudioID, StudioNev, StudioCim) values (5, 'Daniel LLC', '7 Lakeland Park');

insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (1, 'Invisible Ray, The', 75275, 1954, 2, 3);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (2, 'Carry On Teacher', 98356, 1964, 3, 3);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (3, 'It''s Complicated', 71002, 1971, 1, 1);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (4, 'Don''t Look in the Basement!', 19936, 1987, 2, 3);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (5, 'I Love You Too', 76389, 1983, 4, 3);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (6, 'Jekyll & Hyde', 59985, 1947, 5, 2);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (7, 'The Flesh and the Devil', 73455, 1927, 3, 2);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (8, 'Story of a Love Affair', 85930, 1990, 1, 1);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (9, 'Incident at Oglala', 93452, 2014, 3, 4);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (10, 'Uninvited Guest', 16171, 1967, 2, 1);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (11, 'Just Like a Woman', 99770, 1921, 3, 4);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (12, 'Long, Hot Summer, The', 16489, 1951, 1, 4);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (13, 'What to Do in Case of Fire', 26340, 1969, 5, 1);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (14, 'My Friend Irma Goes West', 19674, 1990, 1, 1);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (15, 'Incubus, The', 40858, 1993, 5, 1);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (16, 'Girls About Town', 11777, 1930, 2, 4);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (17, 'Wedding Date, The', 46203, 1932, 5, 1);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (18, 'Confiance r?gne, La', 25345, 1987, 4, 2);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (19, 'Traffic (Trafic)', 76174, 1924, 2, 2);
insert into Filmek (FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID) values (20, 'Moordwijven', 99770, 1999, 3, 1);

insert into Szerepel (SzineszID, FilmID) values (2, 11);
insert into Szerepel (SzineszID, FilmID) values (18, 11);
insert into Szerepel (SzineszID, FilmID) values (18, 3);
insert into Szerepel (SzineszID, FilmID) values (19, 13);
insert into Szerepel (SzineszID, FilmID) values (10, 8);
insert into Szerepel (SzineszID, FilmID) values (5, 18);
insert into Szerepel (SzineszID, FilmID) values (15, 12);
insert into Szerepel (SzineszID, FilmID) values (12, 3);
insert into Szerepel (SzineszID, FilmID) values (10, 11);
insert into Szerepel (SzineszID, FilmID) values (21, 9);
insert into Szerepel (SzineszID, FilmID) values (13, 1);
insert into Szerepel (SzineszID, FilmID) values (9, 19);
insert into Szerepel (SzineszID, FilmID) values (8, 9);
insert into Szerepel (SzineszID, FilmID) values (23, 15);
insert into Szerepel (SzineszID, FilmID) values (14, 19);
insert into Szerepel (SzineszID, FilmID) values (7, 1);
insert into Szerepel (SzineszID, FilmID) values (23, 12);
insert into Szerepel (SzineszID, FilmID) values (18, 9);
insert into Szerepel (SzineszID, FilmID) values (21, 6);
insert into Szerepel (SzineszID, FilmID) values (10, 16);
insert into Szerepel (SzineszID, FilmID) values (4, 13);
insert into Szerepel (SzineszID, FilmID) values (13, 2);
insert into Szerepel (SzineszID, FilmID) values (1, 7);
insert into Szerepel (SzineszID, FilmID) values (3, 13);
insert into Szerepel (SzineszID, FilmID) values (22, 15);
insert into Szerepel (SzineszID, FilmID) values (14, 11);
insert into Szerepel (SzineszID, FilmID) values (24, 12);
insert into Szerepel (SzineszID, FilmID) values (12, 11);
insert into Szerepel (SzineszID, FilmID) values (13, 7);
insert into Szerepel (SzineszID, FilmID) values (9, 10);
insert into Szerepel (SzineszID, FilmID) values (13, 4);

insert into Vetites (VetitesID, FilmID, Datum) values (1, 2, '2017-03-19 16:42:45');
insert into Vetites (VetitesID, FilmID, Datum) values (2, 9, '2017-09-20 02:50:25');
insert into Vetites (VetitesID, FilmID, Datum) values (3, 18, '2017-10-15 15:51:40');
insert into Vetites (VetitesID, FilmID, Datum) values (4, 6, '2017-02-15 02:09:39');
insert into Vetites (VetitesID, FilmID, Datum) values (5, 2, '2017-06-29 01:30:45');
insert into Vetites (VetitesID, FilmID, Datum) values (6, 1, '2017-02-20 07:47:41');
insert into Vetites (VetitesID, FilmID, Datum) values (7, 10, '2017-05-29 08:25:33');
insert into Vetites (VetitesID, FilmID, Datum) values (8, 7, '2017-07-08 11:36:31');
insert into Vetites (VetitesID, FilmID, Datum) values (9, 9, '2017-05-18 02:45:40');
insert into Vetites (VetitesID, FilmID, Datum) values (10, 4, '2016-11-07 19:27:32');
insert into Vetites (VetitesID, FilmID, Datum) values (11, 6, '2016-12-05 22:16:25');
insert into Vetites (VetitesID, FilmID, Datum) values (12, 4, '2017-08-23 09:45:02');
insert into Vetites (VetitesID, FilmID, Datum) values (13, 1, '2016-11-22 06:24:06');
insert into Vetites (VetitesID, FilmID, Datum) values (14, 6, '2017-10-14 21:41:10');
insert into Vetites (VetitesID, FilmID, Datum) values (15, 4, '2016-12-16 13:08:02');
insert into Vetites (VetitesID, FilmID, Datum) values (16, 5, '2017-09-08 17:44:24');
insert into Vetites (VetitesID, FilmID, Datum) values (17, 8, '2016-12-29 14:32:54');
insert into Vetites (VetitesID, FilmID, Datum) values (18, 14, '2017-10-17 08:48:55');
insert into Vetites (VetitesID, FilmID, Datum) values (19, 2, '2017-06-23 01:12:04');
insert into Vetites (VetitesID, FilmID, Datum) values (20, 10, '2017-03-27 21:21:46');
insert into Vetites (VetitesID, FilmID, Datum) values (21, 17, '2017-08-29 15:53:22');
insert into Vetites (VetitesID, FilmID, Datum) values (22, 2, '2016-11-07 18:26:50');
insert into Vetites (VetitesID, FilmID, Datum) values (23, 1, '2017-09-12 19:26:24');
insert into Vetites (VetitesID, FilmID, Datum) values (24, 19, '2017-04-21 14:41:22');
insert into Vetites (VetitesID, FilmID, Datum) values (25, 16, '2017-03-13 04:08:07');