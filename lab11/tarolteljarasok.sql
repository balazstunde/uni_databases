--a parameterkent megadott viragokat tartalmazo csokrokat keresi, ha van ilyen akkor a kimeneti parameter a legdragabb csokor arat tartalmazza, kulonben -1-et

/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/
DROP PROCEDURE CsokorbanAdottVirag
GO
CREATE PROCEDURE CsokorbanAdottVirag
(@pViragNev VARCHAR(30), @pOut FLOAT OUT)
AS
BEGIN
SET NOCOUNT ON
SELECT DISTINCT Csokrok.*, Uzenetek.Uzenet INTO #temp1 
FROM Csokrok,ViragFajtak,CsokrokViragai,Uzenetek
WHERE Csokrok.CsokorID=CsokrokViragai.CsokorID AND Uzenetek.UzenetID=Csokrok.UzenetID AND ViragFajtak.ViragFajtaID=CsokrokViragai.ViragFajtaID AND ViragFajtak.Nev=@pViragNev
DECLARE @rowcount INT=@@ROWCOUNT;
IF @rowcount=0
SET @pOut=-1;
ELSE 
	BEGIN
		SELECT * FROM #temp1
		SELECT Csokrok.CsokorID, SUM(ViragFajtak.Ar) AS CsokorAra INTO #temp2
		FROM Csokrok,ViragFajtak,CsokrokViragai
		WHERE Csokrok.CsokorID=CsokrokViragai.CsokorID AND ViragFajtak.ViragFajtaID=CsokrokViragai.ViragFajtaID
		GROUP BY Csokrok.CsokorID
		SET @pOut=(SELECT MAX(CsokorAra) FROM #temp2)
	END
END
GO

DECLARE @pOut FLOAT
EXEC CsokorbanAdottVirag 'R�zsa', @pOut OUT
SELECT @pOut AS pOut

DROP PROCEDURE CsokorbanAdottViragTipusSzerint
GO
CREATE PROCEDURE CsokorbanAdottViragTipusSzerint
(@pViragNev VARCHAR(30), @pTipusNev VARCHAR(30), @pOut FLOAT OUT)
AS
BEGIN
SET NOCOUNT ON
SELECT DISTINCT Csokrok.*, Uzenetek.Uzenet INTO #temp1 
FROM Csokrok,ViragFajtak,CsokrokViragai,Uzenetek,UzenetTipusok
WHERE Csokrok.CsokorID=CsokrokViragai.CsokorID AND Uzenetek.UzenetID=Csokrok.UzenetID AND ViragFajtak.ViragFajtaID=CsokrokViragai.ViragFajtaID AND Csokrok.UzenetID=Uzenetek.UzenetID AND UzenetTipusok.UzenetTipusID=Uzenetek.UzenetTipusID AND ViragFajtak.Nev=@pViragNev AND UzenetTipusok.Szoveg=@pTipusNev
DECLARE @rowcount INT=@@ROWCOUNT;
IF @rowcount=0
SET @pOut=-1;
ELSE 
	BEGIN
		SELECT * FROM #temp1
		SELECT Csokrok.CsokorID, SUM(ViragFajtak.Ar) AS CsokorAra INTO #temp2
		FROM Csokrok,ViragFajtak,CsokrokViragai
		WHERE Csokrok.CsokorID=CsokrokViragai.CsokorID AND ViragFajtak.ViragFajtaID=CsokrokViragai.ViragFajtaID
		GROUP BY Csokrok.CsokorID
		SET @pOut=(SELECT MIN(CsokorAra) FROM #temp2)
	END
END
GO


CREATE PROCEDURE LegtobbFeleViragotTartalmazoCsokor
(@pOut INT OUT)
AS
BEGIN
SET NOCOUNT ON
	SELECT Csokrok.CsokorID, Szavatossag, Uzenetek.Uzenet, COUNT(DISTINCT ViragFajtak.ViragFajtaID) AS ViragokSzama INTO #temp3
	FROM Csokrok,ViragFajtak,CsokrokViragai,Uzenetek
	WHERE Csokrok.CsokorID=CsokrokViragai.CsokorID AND CsokrokViragai.ViragFajtaID=ViragFajtak.ViragFajtaID AND Csokrok.UzenetID=Uzenetek.UzenetID
	GROUP BY Csokrok.CsokorID, Csokrok.Szavatossag, Uzenetek.Uzenet

	SELECT *
	FROM #temp3
	WHERE ViragokSzama=(SELECT MAX(ViragokSzama) FROM #temp3)
	SET @pOut = (SELECT MAX(ViragokSzama) FROM #temp3)
END

DECLARE @pOut FLOAT
EXEC LegtobbFeleViragotTartalmazoCsokor @pOut OUT
SELECT @pOut AS pOut
