﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Viragszallitok.Models;
/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/

namespace Viragszallitok.Service
{
    class CsokrokManager:DataAccesLayer
    {
        public CsokrokManager(ref string error)
        {
            //megpróbáljuk, hogy létrejön-e a kapcsolat            
            base.CreateConnection(ref error);
        }

        public List<Csokrok> GetCsokrokListDataSetSP(string name, ref float maxPrice, ref string error)
        {
            //bemeneti parameterek
            string[] parameterNames = new string[1], parameterValues = new string[1];
            parameterNames[0] = "@pViragNev"; parameterValues[0] = name;

            //kimeneti parameter neve
            string outputParameterName = "@pOut";

            DataSet ds_tabla = new DataSet();
            object pOut = null;
            ds_tabla = ExecuteStoredProcedureDS("CsokorbanAdottVirag", parameterNames, parameterValues, outputParameterName, ref pOut, ref error);

            List<Csokrok> csokrok = new List<Csokrok>();

            if (error == "OK")
            {
                maxPrice = (float)Convert.ToDouble(pOut);
                
                if (maxPrice >= 0)
                {
                    foreach (DataRow r in ds_tabla.Tables[0].Rows)
                    {
                        Csokrok item = new Csokrok();
                        try
                        {
                            item.csokorID = Convert.ToInt32(r["CsokorID"]);
                            item.szavatossag = Convert.ToInt32(r["Szavatossag"]);
                            item.uzenet = new Uzenetek(0, r["Uzenet"].ToString(),0);
                        }
                        catch (Exception ex)
                        {
                            error = "Invalid data " + ex.Message;
                        }

                        csokrok.Add(item);
                    }
                }
            }
            return csokrok;
        }

        public List<Csokrok> GetCsokrokListDataSetSP(string name, string type, ref float lowPrice, ref string error)
        {
            //bemeneti parameterek
            string[] parameterNames = new string[2], parameterValues = new string[2];
            parameterNames[0] = "@pViragNev"; parameterValues[0] = name;
            parameterNames[1] = "@pTipusNev"; parameterValues[1] = type;

            //kimeneti parameter neve
            string outputParameterName = "@pOut";

            DataSet ds_tabla = new DataSet();
            object pOut = null;
            ds_tabla = ExecuteStoredProcedureDS("CsokorbanAdottViragTipusSzerint", parameterNames, parameterValues, outputParameterName, ref pOut, ref error);

            List<Csokrok> csokrok = new List<Csokrok>();

            if (error == "OK")
            {
                lowPrice = (float)Convert.ToDouble(pOut);

                if (lowPrice >= 0)
                {
                    foreach (DataRow r in ds_tabla.Tables[0].Rows)
                    {
                        Csokrok item = new Csokrok();
                        try
                        {
                            item.csokorID = Convert.ToInt32(r["CsokorID"]);
                            item.szavatossag = Convert.ToInt32(r["Szavatossag"]);
                            item.uzenet = new Uzenetek(0, r["Uzenet"].ToString(), 0);
                        }
                        catch (Exception ex)
                        {
                            error = "Invalid data " + ex.Message;
                        }

                        csokrok.Add(item);
                    }
                }
            }
            return csokrok;
        }

        public List<Csokrok> GetCsokrokListDataSetSP(ref int max, ref string error)
        {

            //kimeneti parameter neve
            string outputParameterName = "@pOut";

            DataSet ds_tabla = new DataSet();
            object pOut = null;
            ds_tabla = ExecuteStoredProcedureDS("LegtobbFeleViragotTartalmazoCsokor",  outputParameterName, ref pOut, ref error);

            List<Csokrok> csokrok = new List<Csokrok>();

            if (error == "OK")
            {
                max = Convert.ToInt32(pOut);

                if (max >= 0)
                {
                    foreach (DataRow r in ds_tabla.Tables[0].Rows)
                    {
                        Csokrok item = new Csokrok();
                        try
                        {
                            item.csokorID = Convert.ToInt32(r["CsokorID"]);
                            item.szavatossag = Convert.ToInt32(r["Szavatossag"]);
                            item.uzenet = new Uzenetek(0, r["Uzenet"].ToString(), 0);
                        }
                        catch (Exception ex)
                        {
                            error = "Invalid data " + ex.Message;
                        }

                        csokrok.Add(item);
                    }
                }
            }
            return csokrok;
        }
    }
}
