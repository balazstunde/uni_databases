﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viragszallitok.Models;
using System.Data;
using System.Data.SqlClient;
/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/

namespace Viragszallitok.Service
{
    class ViragFajtakManager:DataAccesLayer
    {
        public ViragFajtakManager(ref string error)
        {
            base.CreateConnection(ref error);
        }
        public List<Viragfajtak> GetViragokDataReader( ref string error)
        {
            string query = "SELECT  *" +
                "FROM ViragFajtak" ;

            SqlDataReader dataReader = ExecuteReader(query, ref error);
            List<Viragfajtak> viragok = new List<Viragfajtak>();

            //MessageBox.Show(error);

            if (error == "OK")
            {

                while (dataReader.Read())
                {
                    Viragfajtak item = new Viragfajtak();
                    try
                    {
                        item.viragfajtaID = Convert.ToInt32(dataReader["ViragFajtaID"]);
                        item.nev = dataReader["Nev"].ToString();
                        item.leiras = dataReader["Leiras"].ToString();
                        item.jelkep = dataReader["Jelkep"].ToString();
                        item.ar =(float) Convert.ToDouble(dataReader["Ar"]);
                        
                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                    viragok.Add(item);
                }
            }
            CloseDataReader(dataReader);

            return viragok;
        }
    }
}
