﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viragszallitok.Models;
using System.Data;
using System.Data.SqlClient;
/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/

namespace Viragszallitok.Service
{
    class UzenetTipusManager:DataAccesLayer
    {
        public UzenetTipusManager(ref string error)
        {
            base.CreateConnection(ref error);
        }
        public List<UzenetTipusok> GetUzenetTipusokDataReader(ref string error)
        {
            string query = "SELECT  *" +
                "FROM UzenetTipusok";

            SqlDataReader dataReader = ExecuteReader(query, ref error);
            List<UzenetTipusok> uzenetek = new List<UzenetTipusok>();

            //MessageBox.Show(error);

            if (error == "OK")
            {

                while (dataReader.Read())
                {
                    UzenetTipusok item = new UzenetTipusok();
                    try
                    {
                        item.uzenettipusID = Convert.ToInt32(dataReader["UzenetTipusID"]);
                        item.szoveg = dataReader["Szoveg"].ToString();

                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                    uzenetek.Add(item);
                }
            }
            CloseDataReader(dataReader);

            return uzenetek;
        }
    }
}
