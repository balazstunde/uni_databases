﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
/*Balazs Eva-Tunde, 521, beim1596*/
namespace Viragszallitok.Service
{
    public abstract class DataAccesLayer
    {
        protected static SqlConnection conn;
        protected static bool isConnected;
        private static string connstr = "Data Source=TUNDIKE-LAPTOP\\SQLEXPRESS;Initial Catalog=ViragSzallito;Integrated Security=SSPI";

        public DataAccesLayer()
        {
            isConnected = false;
            conn = null;
        }

        public void CreateConnection(ref string errMess)
        {
            if (isConnected != true)
            {
                try
                {
                    conn = new SqlConnection(connstr);
                    conn.Open();
                    errMess = "OK";
                }
                catch (Exception exc)
                {
                    errMess = exc.Message;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        private void OpenConnection(ref string errMess)
        {
            // Open the Connection when the state is not already open.
            if (isConnected == false)
            {
                try
                {
                    conn.Open();
                    isConnected = true;
                    errMess = "OK";
                }
                catch (SqlException ex)
                {
                    errMess = ex.Message;
                    MessageBox.Show(errMess);
                }
            }
        }

        private void CloseConnection()
        {
            // Close the Connection when the connection is opened.
            if (isConnected == true)
            {
                conn.Close();
                isConnected = false;
            }
        }

        public String GetOneValue(string query, ref string errorMessage)
        {
            string value = null;
            errorMessage = null;
            try
            {
                OpenConnection(ref errorMessage);
                SqlCommand cmd = new SqlCommand(query, conn);
                value = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            finally
            {
                CloseConnection();
            }
            return value;
        }

        protected SqlDataReader ExecuteReader(string sQuery, ref string errMess)
        {
            SqlDataReader sqlDataReader = null;
            try
            {
                OpenConnection(ref errMess);//megnyitja az adatbazis-kapcsolatot, ha meg nincs megnyitva) 
                SqlCommand sqlCommand = new SqlCommand(sQuery, conn);
                MessageBox.Show("sql" + sQuery);
                sqlDataReader = sqlCommand.ExecuteReader();
                errMess = "OK";
            }
            catch (Exception e)
            {
                errMess = e.Message;
                CloseDataReader(sqlDataReader);
            }
            return sqlDataReader;
        }

        public void CloseDataReader(SqlDataReader rdr)
        {
            if (rdr != null)
                rdr.Close();
            CloseConnection();
        }

        protected void ExecuteNonQuery(string command, ref string errMess)
        {
            try
            {
                OpenConnection(ref errMess);
                SqlCommand sqlCommand = new SqlCommand(command, conn);
                sqlCommand.ExecuteNonQuery();
                errMess = "OK";
            }
            catch (SqlException ex)
            {

                if (ex.Errors[0].Class == 15)//delete triggernel tortent a hiba, ahol is RAISERROR eseten: SEVERITY LEVEL=15
                {
                    errMess = ex.Errors[0].Message; //ha csak a RAISERROR-nal megadott uzenet erdekel                    
                }
                else
                    errMess = ex.Message;
            }
            finally
            {
                CloseConnection();
            }
        }

        //az elobbit megoldhatjuk parameterezett query-vel is - ezzel az SQL Injection-oket is ki tudjuk kerulni
        protected void ExecuteNonQueryParametrized(string command, string[] parameterNames, string[] parameterValues, ref string errMess)
        {
            try
            {
                OpenConnection(ref errMess);
                SqlCommand sqlCommand = new SqlCommand(command, conn);
                for (int i = 0; i < parameterNames.Length; i++)
                {
                    sqlCommand.Parameters.AddWithValue(parameterNames[i], parameterValues[i]);
                }
                sqlCommand.ExecuteNonQuery();
                errMess = "OK";
            }
            catch (SqlException ex)
            {
                errMess = ex.Message;
            }
            finally
            {
                CloseConnection();
            }
        }


        /// <summary>
        ///  Executes a given select command that is expected to return scalar values (e.g. COUNT, AVG, MIN, MAX, SUM)
        ///  and returns the scalar value. 
        /// (ErrMess is "OK" if no exception occured)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        protected object ExecuteScalar(string query, ref string errMess)
        {
            object value;
            try
            {
                OpenConnection(ref errMess);
                SqlCommand sqlCommand = new SqlCommand(query, conn);
                value = sqlCommand.ExecuteScalar();
                errMess = "OK";
            }
            catch (SqlException e)
            {
                value = null;
                errMess = e.Message;
            }
            finally
            {
                CloseConnection();
            }
            return value;
        }

        protected object ExecuteStoredProcedureNonQuery(string spName, string[] parameterNames, string[] parameterValues, string outputParameterName, ref string errMess)
        {
            object parOut = null;
            try
            {
                OpenConnection(ref errMess);
                SqlCommand cmd = new SqlCommand(spName, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                for (int i = 0; i < parameterNames.Length; i++)
                {
                    cmd.Parameters.AddWithValue(parameterNames[i], parameterValues[i]);
                }

                SqlParameter pOut = new SqlParameter();
                pOut.ParameterName = outputParameterName;
                pOut.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pOut);

                cmd.ExecuteNonQuery();

                parOut = pOut.Value;
            }
            catch (SqlException e)
            {
                errMess = e.Message;
            }
            finally
            {
                CloseConnection();
            }
            return parOut;
        }

        /// <summary>
        /// Executes a given stored procedure that has an output parameter and returns the result in a dataset.
        /// </summary>
        /// <param name="spName">The name of the stored procedure</param>
        /// <param name="parameterNames">The list of the parameter names</param>
        /// <param name="parameterValues">The list of the parameter values</param> 
        /// <param name="outputParameterName">The name of the output parameter</param>
        /// <param name="parOut">The value of the output parameter</param>
        /// <param name="errMess">error message</param>
        /// <returns></returns>

        protected DataSet ExecuteStoredProcedureDS(string spName, string[] parameterNames, string[] parameterValues, string outputParameterName, ref object parOut, ref string errMess)
        {
            DataSet dataSet = new DataSet();

            try
            {
                OpenConnection(ref errMess);

                SqlCommand sqlCommand = new SqlCommand(spName, conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                for (int i = 0; i < parameterNames.Length; i++)
                {
                    sqlCommand.Parameters.AddWithValue(parameterNames[i], parameterValues[i]);
                }

                SqlParameter pOut = new SqlParameter();
                pOut.ParameterName = outputParameterName;
                pOut.Direction = ParameterDirection.Output;
                pOut.Size = 30; //a kimeneti parameternek mindenkepp be kell allitani valamilyen meretet
                sqlCommand.Parameters.Add(pOut);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(spName, conn);

                dataAdapter.SelectCommand = sqlCommand;

                dataAdapter.Fill(dataSet);

                parOut = pOut.Value;

                errMess = "OK";
            }
            catch (SqlException e)
            {
                errMess = e.Message;
            }
            finally
            {
                CloseConnection();
            }
            return dataSet;
        }

        protected DataSet ExecuteStoredProcedureDS(string spName, string outputParameterName, ref object parOut, ref string errMess)
        {
            DataSet dataSet = new DataSet();

            try
            {
                OpenConnection(ref errMess);

                SqlCommand sqlCommand = new SqlCommand(spName, conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                

                SqlParameter pOut = new SqlParameter();
                pOut.ParameterName = outputParameterName;
                pOut.Direction = ParameterDirection.Output;
                pOut.Size = 30; //a kimeneti parameternek mindenkepp be kell allitani valamilyen meretet
                sqlCommand.Parameters.Add(pOut);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(spName, conn);

                dataAdapter.SelectCommand = sqlCommand;

                dataAdapter.Fill(dataSet);

                parOut = pOut.Value;

                errMess = "OK";
            }
            catch (SqlException e)
            {
                errMess = e.Message;
            }
            finally
            {
                CloseConnection();
            }
            return dataSet;
        }
    }
}
