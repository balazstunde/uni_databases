﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Viragszallitok.Models;
/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/

namespace Viragszallitok.Service
{
    class BonBonokManager:DataAccesLayer
    {
        public BonBonokManager(ref string error)
        {
            base.CreateConnection(ref error);
        }
        public List<Bonbonok> GetBonbonokDataReader(ref string error)
        {
            string query = "SELECT  *" +
                "FROM Bonbonok";

            SqlDataReader dataReader = ExecuteReader(query, ref error);
            List<Bonbonok> bonbonok = new List<Bonbonok>();

            //MessageBox.Show(error);

            if (error == "OK")
            {

                while (dataReader.Read())
                {
                    Bonbonok item = new Bonbonok();
                    try
                    {
                        item.bonbonID = Convert.ToInt32(dataReader["BonbonID"]);
                        item.nev = dataReader["Nev"].ToString();
                        item.osszetevok = dataReader["Osszetevok"].ToString();
                        item.szavatossag = dataReader["Szavatossag"].ToString();
                        item.minosites = (float)Convert.ToDouble(dataReader["Minosites"]);

                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                    bonbonok.Add(item);
                }
            }
            CloseDataReader(dataReader);

            return bonbonok;
        }
    }
}
