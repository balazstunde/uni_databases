﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/
namespace Viragszallitok.Models
{
    class Uzenetek
    {
        private int UzenetID;
        private string Uzenet;
        private int UzenetTipusID;

        public Uzenetek()
        {
            UzenetID = 0;
            Uzenet = null;
            UzenetTipusID = 0;
        }

        public Uzenetek(int id, string uz, int id2)
        {
            UzenetID = id;
            Uzenet = uz;
            UzenetTipusID = id2;
        }

        public int uzenettipusID
        {
            get { return UzenetTipusID; }
            set { UzenetTipusID = value; }
        }

        public int uzenetID
        {
            get { return UzenetID; }
            set { UzenetID = value; }
        }

        public string uzenet
        {
            get { return Uzenet; }
            set { Uzenet = value; }
        }
    }
}
