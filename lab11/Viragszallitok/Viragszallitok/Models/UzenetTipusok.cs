﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/
namespace Viragszallitok.Models
{
    class UzenetTipusok
    {
        private int UzenetTipusID;
        private string Szoveg;

        public UzenetTipusok(int id, string szov)
        {
            UzenetTipusID = id;
            Szoveg = szov;
        }

        public UzenetTipusok()
        {
            UzenetTipusID = 0;
            Szoveg = null;
        }

        public int uzenettipusID
        {
            get { return UzenetTipusID; }
            set { UzenetTipusID = value; }
        }

        public string szoveg
        {
            get { return Szoveg; }
            set { Szoveg = value; }
        }
    }
}
