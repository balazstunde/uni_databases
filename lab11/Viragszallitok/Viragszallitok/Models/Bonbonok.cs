﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/
namespace Viragszallitok.Models
{
    class Bonbonok
    {
        private int BonbonID;
        private string Nev;
        private string Osszetevok;
        private string Szavatossag;
        private float Minosites;

        public Bonbonok(int id, string n, string ossz, string szav, float min)
        {
            BonbonID = id;
            Nev = n;
            Osszetevok = ossz;
            Szavatossag = szav;
            Minosites = min;
        }

        public Bonbonok()
        {
            BonbonID = 0;
            Nev = null;
            Osszetevok = null;
            Szavatossag = null;
            Minosites = 0;
        }

        public int bonbonID
        {
            get { return BonbonID; }
            set { BonbonID = value; }
        }

        public string nev
        {
            get { return Nev; }
            set { Nev = value; }
        }

        public string osszetevok
        {
            get { return Osszetevok; }
            set { Osszetevok = value; }
        }

        public string szavatossag
        {
            get { return Szavatossag; }
            set { Szavatossag = value; }
        }

        public float minosites
        {
            get { return Minosites; }
            set { Minosites = value; }
        }
    }
}
