﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/
namespace Viragszallitok.Models
{
    class Viragfajtak
    {
        private int ViragfajtaID;
        private string Nev;
        private string Leiras;
        private string Jelkep;
        private float Ar;
        
        public Viragfajtak(int id, string n, string leir, string jelk, float a)
        {
            ViragfajtaID = id;
            Nev = n;
            Leiras = leir;
            Jelkep = jelk;
            Ar = a;
        }

        public Viragfajtak()
        {
            ViragfajtaID = 0;
            Nev = null;
            Leiras = null;
            Jelkep = null;
            Ar = 0;
        }

        public int viragfajtaID
        {
            get { return ViragfajtaID; }
            set { ViragfajtaID = value; }
        }

        public string nev
        {
            get { return Nev; }
            set { Nev = value; }
        }

        public string jelkep
        {
            get { return Jelkep; }
            set { Jelkep = value; }
        }

        public string leiras
        {
            get { return Leiras; }
            set { Leiras = value; }
        }

        public float ar
        {
            get { return Ar; }
            set { Ar = value; }
        }
    }
}
