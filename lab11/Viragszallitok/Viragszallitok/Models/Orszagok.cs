﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Balazs Eva-Tunde, 521, beim1596*/
namespace Viragszallitok.Models
{
    class Orszagok
    {
        private int OrszagID;
        private string ONev;

        public Orszagok(int oID, string n)
        {
            OrszagID = oID;
            ONev = n;
        }

        public Orszagok()
        {
            ONev = null;
            OrszagID = 0;
        }

        public int orszagID
        {
            get { return OrszagID; }
            set { OrszagID = value; }
        }

        public string OrszagNev
        {
            get { return ONev; }
            set { ONev = value; }
        }
    }
}
