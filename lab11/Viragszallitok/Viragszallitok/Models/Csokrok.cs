﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/
namespace Viragszallitok.Models
{
    class Csokrok
    {
        private int CsokorID;
        private int Szavatossag;
        private Uzenetek Uzenet;

        public Csokrok(int csID, int szav, Uzenetek uz)
        {
            CsokorID = csID;
            Szavatossag = szav;
            Uzenet = uz;
        }

        public Csokrok()
        {
            CsokorID = 0;
            Szavatossag = 0;
            Uzenet = new Uzenetek();
        }

        public int csokorID
        {
            get { return CsokorID; }
            set { CsokorID = value; }
        }

        public int szavatossag
        {
            get { return Szavatossag; }
            set { Szavatossag = value; }
        }

        public Uzenetek uzenet
        {
            get { return Uzenet; }
            set { Uzenet = value; }
        }
    }
}
