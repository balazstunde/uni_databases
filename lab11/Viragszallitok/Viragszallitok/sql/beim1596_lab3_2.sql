/*N�v: Bal�zs �va-T�nde
Azonos�t�: beim1596
Csoport: 511/1
Feladat: 2. sorsz�m, 3. labor*/

SELECT * FROM Filmek;
--K�rdezz�k le a M�fajok t�bla tartalm�t!
SELECT * 
FROM Mufajok;

--Adjuk meg az els� h�rom filmet az adatb�zisb�l!
SELECT TOP 3 * 
FROM Filmek;

--ABC sorrendben adjuk meg a filmek c�meit az �rt�kel�s�kkel egy�tt!
SELECT FilmCim, Ertekeles 
FROM Filmek
ORDER BY FilmCim;

--Adjuk meg az 1950 �s 2015 k�z�tt megjelent filmek c�m�t!
SELECT FilmCim
FROM Filmek
WHERE YEAR(MegjEv) BETWEEN 1950 AND 2015;

--Adjuk meg az A bet�vel kezd�d� sz�n�szek nev�t �s �letkor�t a k�vetkez� form�ban: Nev (Eletkor)! Pl. Nev=�Angelina Jolie�, Eletkor=42, akkor az eredm�ny: Angelina Jolie (42).
SELECT CONCAT(SzineszNev,' (',(Eletkor),')')
FROM Szineszek
WHERE SzineszNev LIKE 'A%';

--Adjuk meg az adatb�zisban szerepl� st�di�k sz�m�t!
SELECT COUNT(StudioID) AS 'Studiok darabszama'
FROM Studiok

--Adjuk meg az 50 �vn�l id�sebb sz�n�szek nev�t, akik nev�nek harmadik bet�je �T�!
SELECT SzineszNev
FROM Szineszek
WHERE SzineszNev LIKE '__t%' AND Eletkor>50;

--Adjuk meg a legr�videbb film id�tartam�t!
SELECT MIN(Idotartam) AS 'Idotartam'
FROM Filmek;

--Adjuk meg a (�Titanic�, �Star Wars�, �It�) c�m� filmek szerepl�it! Minden szerepl� csak egyszer jelenjen meg az eredm�nyben!
SELECT DISTINCT SzineszNev
FROM Szineszek, Szerepel, Filmek
WHERE Szineszek.SzineszID=Szerepel.SzineszID AND  Filmek.FilmID=Szerepel.FilmID AND Filmek.FilmCim IN('Sherlock', 'This is us','Inception');

--Adjuk meg azon filmek c�m�t �s �rt�kel�s�t, amelyeket vet�tettek �ez �v augusztus�ban�! Minden film csak egyszer jelenjen meg az eredm�nyben!
SELECT DISTINCT FilmCim, Ertekeles
FROM Filmek
INNER JOIN Vetites
ON Filmek.FilmID=Vetites.FilmID
WHERE MONTH(Datum)=8;

SELECT * FROM Szerepel;  