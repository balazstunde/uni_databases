﻿namespace Viragszallitok
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.dgv_form2 = new System.Windows.Forms.DataGridView();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.btn_vissza = new System.Windows.Forms.Button();
            this.btn_select = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.cb_viragok = new System.Windows.Forms.ComboBox();
            this.lb_uzenet = new System.Windows.Forms.Label();
            this.cb_uzenettipus = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_form2)).BeginInit();
            this.SuspendLayout();
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(66, 46);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(178, 17);
            this.radioButton1.TabIndex = 6;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Milyen virágokból válogathatok?";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // dgv_form2
            // 
            this.dgv_form2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_form2.Location = new System.Drawing.Point(66, 175);
            this.dgv_form2.Name = "dgv_form2";
            this.dgv_form2.Size = new System.Drawing.Size(449, 187);
            this.dgv_form2.TabIndex = 7;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(67, 70);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(191, 17);
            this.radioButton2.TabIndex = 8;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Milyen bonbonokból válogathatok?";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(67, 94);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(299, 17);
            this.radioButton3.TabIndex = 9;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Milyen csokrok tartalmazzák az általam kedvelt virágokat?";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(67, 118);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(326, 17);
            this.radioButton4.TabIndex = 10;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Csokrok amelyekben a megfelelő üzenettípus és virág található.";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(67, 141);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(220, 17);
            this.radioButton5.TabIndex = 11;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Legtöbb féle virágot tartalmazók csokrok.";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // btn_vissza
            // 
            this.btn_vissza.Location = new System.Drawing.Point(67, 392);
            this.btn_vissza.Name = "btn_vissza";
            this.btn_vissza.Size = new System.Drawing.Size(75, 23);
            this.btn_vissza.TabIndex = 12;
            this.btn_vissza.Text = "Vissza";
            this.btn_vissza.UseVisualStyleBackColor = true;
            this.btn_vissza.Click += new System.EventHandler(this.btn_vissza_Click);
            // 
            // btn_select
            // 
            this.btn_select.Location = new System.Drawing.Point(440, 391);
            this.btn_select.Name = "btn_select";
            this.btn_select.Size = new System.Drawing.Size(75, 23);
            this.btn_select.TabIndex = 13;
            this.btn_select.Text = "Mehet";
            this.btn_select.UseVisualStyleBackColor = true;
            this.btn_select.Click += new System.EventHandler(this.btn_select_Click);
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(440, 41);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(75, 27);
            this.exit.TabIndex = 14;
            this.exit.Text = "Kilépés";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // cb_viragok
            // 
            this.cb_viragok.FormattingEnabled = true;
            this.cb_viragok.Location = new System.Drawing.Point(419, 94);
            this.cb_viragok.Name = "cb_viragok";
            this.cb_viragok.Size = new System.Drawing.Size(121, 21);
            this.cb_viragok.TabIndex = 15;
            this.cb_viragok.Visible = false;
            // 
            // lb_uzenet
            // 
            this.lb_uzenet.AutoSize = true;
            this.lb_uzenet.Location = new System.Drawing.Point(190, 401);
            this.lb_uzenet.Name = "lb_uzenet";
            this.lb_uzenet.Size = new System.Drawing.Size(35, 13);
            this.lb_uzenet.TabIndex = 16;
            this.lb_uzenet.Text = "label1";
            this.lb_uzenet.Visible = false;
            // 
            // cb_uzenettipus
            // 
            this.cb_uzenettipus.FormattingEnabled = true;
            this.cb_uzenettipus.Location = new System.Drawing.Point(419, 118);
            this.cb_uzenettipus.Name = "cb_uzenettipus";
            this.cb_uzenettipus.Size = new System.Drawing.Size(121, 21);
            this.cb_uzenettipus.TabIndex = 17;
            this.cb_uzenettipus.Visible = false;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 461);
            this.Controls.Add(this.cb_uzenettipus);
            this.Controls.Add(this.lb_uzenet);
            this.Controls.Add(this.cb_viragok);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.btn_select);
            this.Controls.Add(this.btn_vissza);
            this.Controls.Add(this.radioButton5);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.dgv_form2);
            this.Controls.Add(this.radioButton1);
            this.Name = "Form2";
            this.Text = "Form";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_form2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.DataGridView dgv_form2;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Button btn_vissza;
        private System.Windows.Forms.Button btn_select;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.ComboBox cb_viragok;
        private System.Windows.Forms.Label lb_uzenet;
        private System.Windows.Forms.ComboBox cb_uzenettipus;
    }
}