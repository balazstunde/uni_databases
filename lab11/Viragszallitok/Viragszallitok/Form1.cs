﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Viragszallitok.Models;
using Viragszallitok.Service;
/*Balazs Eva-Tunde, 521, beim1596*/
namespace Viragszallitok
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int uj = 0;
        private int modosit = 0;
        private int moduzlet = 0;

        public void cb_upload()
        {
            OrszagManager manager = new OrszagManager();
            string err = null;
            manager.CreateConnection(ref err);
            List<Orszagok> lista = manager.GetOrszagList(ref err);
            this.cb_orszagok.DataSource = lista;
            cb_orszagok.DisplayMember = "OrszagNev";
            cb_orszagok.ValueMember = "orszagID";
        }

        public void cb_uploadorszag()
        {
            OrszagManager manager = new OrszagManager();
            string err = null;
            manager.CreateConnection(ref err);
            List<Orszagok> lista = manager.GetOrszagList(ref err);
            this.cb_orszag.DataSource = lista;
            cb_orszag.DisplayMember = "OrszagNev";
            cb_orszag.ValueMember = "orszagID";
        }

        public void dgv_upload()
        {
            string err = null;
            UzletManager uzletmanager = new UzletManager(ref err);

            List<Uzletek> uzletLista = uzletmanager.GetUzletList(ref err);
            //dgv_Uzletek.DataSource = uzletLista;
            dgv_Uzletek.Rows.Clear();
            dgv_Uzletek.Refresh();
            dgv_Uzletek.ColumnCount = 5;
            dgv_Uzletek.Columns[0].Name = "uzletID";
            dgv_Uzletek.Columns[1].Name = "uzletNev";
            dgv_Uzletek.Columns[2].Name = "uzletCim";
            dgv_Uzletek.Columns[3].Name = "uzletTelefonszam";
            dgv_Uzletek.Columns[4].Name = "OrszagNev";
            foreach (Uzletek item in uzletLista)
            {
                dgv_Uzletek.Rows.Add(item.uzletID, item.uzletNev, item.uzletCim, item.uzletTelefonszam, item.uzletOrszaga.OrszagNev);
            }
            dgv_Uzletek.CurrentRow.Selected = true;
        }

        public void uzletSzam()
        {
            string err = null;
            UzletManager manager = new UzletManager(ref err);
            lb_uzletekszama.Text = "Üzletek száma: " + manager.GetUzletNumber(ref err).ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cb_upload();
            dgv_upload();
            uzletSzam();
        }

        private void bt_szures_Click(object sender, EventArgs e)
        {
            string betu = tb_betu.Text;
            string orszagNev = cb_orszagok.SelectedValue.ToString();
            string err = null;
            UzletManager manager = new UzletManager(ref err);
            
            manager.CreateConnection(ref err);
            List<Uzletek> uzletLista = manager.GetUzletListDataReader(betu[0], orszagNev, ref err);
            dgv_Uzletek.Rows.Clear();
            dgv_Uzletek.Refresh();
            dgv_Uzletek.ColumnCount = 5;
            dgv_Uzletek.Columns[0].Name = "UzletID";
            dgv_Uzletek.Columns[1].Name = "Nev";
            dgv_Uzletek.Columns[2].Name = "Cim";
            dgv_Uzletek.Columns[3].Name = "Telefonszam";
            dgv_Uzletek.Columns[4].Name = "OrszagNev";
            //dgv_Uzletek.DataSource = uzletLista;
            foreach (Uzletek i in uzletLista)
            {
                dgv_Uzletek.Rows.Add(i.uzletID, i.uzletNev, i.uzletCim, i.uzletTelefonszam, i.uzletOrszaga.OrszagNev);
            }
            dgv_Uzletek.CurrentRow.Selected = true;
        }
        

        private void bt_uj_Click(object sender, EventArgs e)
        {
            bt_modosit.Enabled = false;
            bt_kilepes.Enabled = false;
            bt_szures.Enabled = false;
            bt_torles.Enabled = false;
            bt_uj.Enabled = false;
            bt_megse.Enabled = true;
            bt_mentes.Enabled = true;
            tb_cim.Visible = true;
            tb_nev.Visible = true;
            tb_telefonszam.Visible = true;
            cb_orszag.Visible = true;
            lb_cim.Visible = true;
            lb_nev.Visible = true;
            lb_orszag.Visible = true;
            lb_telefonszam.Visible = true;
            cb_uploadorszag();
            uj = 1;
        }

        private void bt_modosit_Click(object sender, EventArgs e)
        {
            bt_modosit.Enabled = false;
            bt_kilepes.Enabled = false;
            bt_szures.Enabled = false;
            bt_torles.Enabled = false;
            bt_uj.Enabled = false;
            bt_megse.Enabled = true;
            bt_mentes.Enabled = true;
            tb_cim.Visible = true;
            tb_nev.Visible = true;
            tb_telefonszam.Visible = true;
            cb_orszag.Visible = true;
            lb_cim.Visible = true;
            lb_nev.Visible = true;
            lb_orszag.Visible = true;
            lb_telefonszam.Visible = true;
            cb_uploadorszag();
            //a megfelelo tartalmak betoltese
            tb_nev.Text = dgv_Uzletek.SelectedCells[0].OwningRow.Cells[1].Value.ToString();
            tb_cim.Text = dgv_Uzletek.SelectedCells[0].OwningRow.Cells[2].Value.ToString();
            tb_telefonszam.Text = dgv_Uzletek.SelectedCells[0].OwningRow.Cells[3].Value.ToString();
            moduzlet = Convert.ToInt32(dgv_Uzletek.SelectedCells[0].OwningRow.Cells[0].Value);
            cb_orszag.SelectedIndex = cb_orszag.FindStringExact(dgv_Uzletek.SelectedCells[0].OwningRow.Cells[4].Value.ToString());
            modosit = 1;
        }

        private void bt_kilepes_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bt_torles_Click(object sender, EventArgs e)
        {
            /*kérdezzük meg a felhasználótól, hogy biztos akarja-e törölni a DataGridView/ListView-ban kiválasztott sort
             *  (lehet használni MessageBox-t). Ha igennel válaszolt, akkor töröljük a sort az adatbázisból. 
             *  Ezután: frissítsük a DataGridView/ListView-t. Természetesen már más soron kell állnunk a 
             *  DataGridView/ListView-ban, mivel az aktuálisat kitöröltük. A gombok olyan állapotban legyenek,
             *   mint a form betöltődésekor. Ha a törlés esetén a felhasználó ‘Nem’-mel válaszol, akkor minden vezérlő állapota 
             *   legyen a form betöltődésének megfelelő.
            Vigyázat: a törlendő sort tartalmazó tábla elsődleges kulcsára más táblákban hivatkozunk,
            tehát előbb azon táblá(k)ból kell töröljük a megfelelő sor(oka)t, amely(ek)ben az elsődleges kulcsra van hivatkozás! */
            DialogResult dialogResult = MessageBox.Show("Biztosan törli a kijelölt sort?","Törlés",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                int uzletid= Convert.ToInt32(dgv_Uzletek.SelectedCells[0].OwningRow.Cells[0].Value);
                string err = null;
                UzletManager manager = new UzletManager(ref err);
                if (err == "OK")
                {
                    //eloszor torolni az Ajandekokbol
                    manager.DeleteSmth("Ajandekok", "UzletID", uzletid, ref err);
                    manager.DeleteSmth("Uzletek","UzletID",uzletid,ref err);
                    if (err == "OK")
                    {
                        formload();
                        uzletSzam();
                    }
                    else
                        MessageBox.Show("Hiba a torleskor!" + Environment.NewLine + err);
                }
                else
                {
                    MessageBox.Show("Hiba az Uzletmanager letrehozasakor."+err);
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                formload();
            }
        }

        private void bt_mentes_Click(object sender, EventArgs e)
        {
            /*Az adatbázisban is hajtsuk végre a megfelelő módosításokat (a DataGridView/ListView megfelelő vezérlői értékei 
            alapján): új érték beszúrása esetén-INSERT, már meglevő sor módosításakor-UPDATE művelet hajtódjon végre. 
                Ezután frissítsük a DataGridView/ListView-t. Minden vezérlő olyan állapotban legyen, mint amilyenben a form 
                betöltődésekor volt.*/
            string error = string.Empty;
            bool empty = false; //igaz, ha nincs kitoltve minden mezo
            int orszagID = Convert.ToInt32(cb_orszag.SelectedValue);
            string uzletnev = tb_nev.Text;
            string cim = tb_cim.Text;
            string telefonszam = tb_telefonszam.Text;
            if (uzletnev == String.Empty || cim == String.Empty || telefonszam == String.Empty)
            {
                empty = true;
            }
            bool joforma = true;
            for(int i = 0; i < telefonszam.Length; i++)
            {
                if(telefonszam[i]>'9' || telefonszam[i] < '0')
                {
                    joforma = false;
                }
            }
            if (empty || !joforma)
            {
                MessageBox.Show("Nincs minden mező kitöltve vagy helytelen formátum!");
            }
            else
            {
                string err = null;
                UzletManager man = new UzletManager(ref err);
                if (uj == 1)
                {
                    man.InsertUzlet(uzletnev,cim,telefonszam,orszagID,ref err);
                    if(err == "OK")
                    {
                        formload();
                        uzletSzam();
                        uj = 0;
                    }
                    else
                        MessageBox.Show("Hiba a beszuraskor!" + Environment.NewLine + err);
                }
                else if (modosit == 1)
                {

                    man.UpdateUzlet(moduzlet,uzletnev,cim,telefonszam,orszagID,ref err);
                    if (err == "OK")
                    {
                        formload();
                        modosit = 0;
                    }
                    else
                        MessageBox.Show("Hiba az modosításkor!" + Environment.NewLine + err);
                }
            }
        }

        private void bt_megse_Click(object sender, EventArgs e)
        {
            /*mindent vezérlőt állítsunk vissza abba az állapotba, mint amilyenben a form betöltődésekor volt. 
             * NEM történik semmilyen módosítás(INSERT/UPDATE) az adatbázisban.*/
            formload();
            uj = 0;
            modosit = 0;
        }

        public void formload()
        {
            bt_modosit.Enabled = true;
            bt_kilepes.Enabled = true;
            bt_szures.Enabled = true;
            bt_torles.Enabled = true;
            bt_uj.Enabled = true;
            bt_megse.Enabled = false;
            bt_mentes.Enabled = false;
            tb_cim.Visible = false;
            tb_nev.Visible = false;
            tb_telefonszam.Visible = false;
            cb_orszag.Visible = false;
            lb_cim.Visible = false;
            lb_nev.Visible = false;
            lb_orszag.Visible = false;
            lb_telefonszam.Visible = false;
            tb_betu.Text = "";
            tb_cim.Text = "";
            tb_nev.Text = "";
            tb_telefonszam.Text = "";
            dgv_upload();
            uzletSzam();
        }

        private void btn_info_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2();
            f2.Show();
        }
    }
}
