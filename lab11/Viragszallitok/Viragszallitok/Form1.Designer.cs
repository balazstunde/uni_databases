﻿namespace Viragszallitok
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_szures = new System.Windows.Forms.Button();
            this.cb_orszagok = new System.Windows.Forms.ComboBox();
            this.tb_betu = new System.Windows.Forms.TextBox();
            this.dgv_Uzletek = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_uzleteklistaja = new System.Windows.Forms.Label();
            this.bt_uj = new System.Windows.Forms.Button();
            this.bt_modosit = new System.Windows.Forms.Button();
            this.bt_torles = new System.Windows.Forms.Button();
            this.bt_mentes = new System.Windows.Forms.Button();
            this.bt_kilepes = new System.Windows.Forms.Button();
            this.bt_megse = new System.Windows.Forms.Button();
            this.tb_nev = new System.Windows.Forms.TextBox();
            this.tb_cim = new System.Windows.Forms.TextBox();
            this.tb_telefonszam = new System.Windows.Forms.TextBox();
            this.cb_orszag = new System.Windows.Forms.ComboBox();
            this.lb_nev = new System.Windows.Forms.Label();
            this.lb_cim = new System.Windows.Forms.Label();
            this.lb_telefonszam = new System.Windows.Forms.Label();
            this.lb_orszag = new System.Windows.Forms.Label();
            this.lb_uzletekszama = new System.Windows.Forms.Label();
            this.btn_info = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Uzletek)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_szures
            // 
            this.bt_szures.Location = new System.Drawing.Point(593, 46);
            this.bt_szures.Name = "bt_szures";
            this.bt_szures.Size = new System.Drawing.Size(127, 23);
            this.bt_szures.TabIndex = 0;
            this.bt_szures.Text = "Szűrés";
            this.bt_szures.UseVisualStyleBackColor = true;
            this.bt_szures.Click += new System.EventHandler(this.bt_szures_Click);
            // 
            // cb_orszagok
            // 
            this.cb_orszagok.FormattingEnabled = true;
            this.cb_orszagok.Location = new System.Drawing.Point(323, 45);
            this.cb_orszagok.Name = "cb_orszagok";
            this.cb_orszagok.Size = new System.Drawing.Size(121, 21);
            this.cb_orszagok.TabIndex = 1;
            // 
            // tb_betu
            // 
            this.tb_betu.Location = new System.Drawing.Point(45, 46);
            this.tb_betu.Name = "tb_betu";
            this.tb_betu.Size = new System.Drawing.Size(100, 20);
            this.tb_betu.TabIndex = 2;
            // 
            // dgv_Uzletek
            // 
            this.dgv_Uzletek.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Uzletek.Location = new System.Drawing.Point(42, 108);
            this.dgv_Uzletek.Name = "dgv_Uzletek";
            this.dgv_Uzletek.Size = new System.Drawing.Size(678, 177);
            this.dgv_Uzletek.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Üzlet kezdőbetűje:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(320, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Országok:";
            // 
            // label_uzleteklistaja
            // 
            this.label_uzleteklistaja.AutoSize = true;
            this.label_uzleteklistaja.Location = new System.Drawing.Point(42, 92);
            this.label_uzleteklistaja.Name = "label_uzleteklistaja";
            this.label_uzleteklistaja.Size = new System.Drawing.Size(72, 13);
            this.label_uzleteklistaja.TabIndex = 6;
            this.label_uzleteklistaja.Text = "Üzletek listája";
            // 
            // bt_uj
            // 
            this.bt_uj.Location = new System.Drawing.Point(45, 325);
            this.bt_uj.Name = "bt_uj";
            this.bt_uj.Size = new System.Drawing.Size(100, 38);
            this.bt_uj.TabIndex = 7;
            this.bt_uj.Text = "Új";
            this.bt_uj.UseVisualStyleBackColor = true;
            this.bt_uj.Click += new System.EventHandler(this.bt_uj_Click);
            // 
            // bt_modosit
            // 
            this.bt_modosit.Location = new System.Drawing.Point(169, 326);
            this.bt_modosit.Name = "bt_modosit";
            this.bt_modosit.Size = new System.Drawing.Size(89, 38);
            this.bt_modosit.TabIndex = 8;
            this.bt_modosit.Text = "Módosítás";
            this.bt_modosit.UseVisualStyleBackColor = true;
            this.bt_modosit.Click += new System.EventHandler(this.bt_modosit_Click);
            // 
            // bt_torles
            // 
            this.bt_torles.Location = new System.Drawing.Point(301, 326);
            this.bt_torles.Name = "bt_torles";
            this.bt_torles.Size = new System.Drawing.Size(75, 39);
            this.bt_torles.TabIndex = 9;
            this.bt_torles.Text = "Törlés";
            this.bt_torles.UseVisualStyleBackColor = true;
            this.bt_torles.Click += new System.EventHandler(this.bt_torles_Click);
            // 
            // bt_mentes
            // 
            this.bt_mentes.Enabled = false;
            this.bt_mentes.Location = new System.Drawing.Point(410, 326);
            this.bt_mentes.Name = "bt_mentes";
            this.bt_mentes.Size = new System.Drawing.Size(82, 39);
            this.bt_mentes.TabIndex = 10;
            this.bt_mentes.Text = "Mentés";
            this.bt_mentes.UseVisualStyleBackColor = true;
            this.bt_mentes.Click += new System.EventHandler(this.bt_mentes_Click);
            // 
            // bt_kilepes
            // 
            this.bt_kilepes.Location = new System.Drawing.Point(629, 325);
            this.bt_kilepes.Name = "bt_kilepes";
            this.bt_kilepes.Size = new System.Drawing.Size(91, 38);
            this.bt_kilepes.TabIndex = 11;
            this.bt_kilepes.Text = "Kilépés";
            this.bt_kilepes.UseVisualStyleBackColor = true;
            this.bt_kilepes.Click += new System.EventHandler(this.bt_kilepes_Click);
            // 
            // bt_megse
            // 
            this.bt_megse.Enabled = false;
            this.bt_megse.Location = new System.Drawing.Point(515, 326);
            this.bt_megse.Name = "bt_megse";
            this.bt_megse.Size = new System.Drawing.Size(90, 39);
            this.bt_megse.TabIndex = 12;
            this.bt_megse.Text = "Mégse";
            this.bt_megse.UseVisualStyleBackColor = true;
            this.bt_megse.Click += new System.EventHandler(this.bt_megse_Click);
            // 
            // tb_nev
            // 
            this.tb_nev.Location = new System.Drawing.Point(42, 416);
            this.tb_nev.Name = "tb_nev";
            this.tb_nev.Size = new System.Drawing.Size(100, 20);
            this.tb_nev.TabIndex = 13;
            this.tb_nev.Visible = false;
            // 
            // tb_cim
            // 
            this.tb_cim.Location = new System.Drawing.Point(181, 416);
            this.tb_cim.Name = "tb_cim";
            this.tb_cim.Size = new System.Drawing.Size(100, 20);
            this.tb_cim.TabIndex = 14;
            this.tb_cim.Visible = false;
            // 
            // tb_telefonszam
            // 
            this.tb_telefonszam.Location = new System.Drawing.Point(344, 416);
            this.tb_telefonszam.Name = "tb_telefonszam";
            this.tb_telefonszam.Size = new System.Drawing.Size(100, 20);
            this.tb_telefonszam.TabIndex = 15;
            this.tb_telefonszam.Visible = false;
            // 
            // cb_orszag
            // 
            this.cb_orszag.FormattingEnabled = true;
            this.cb_orszag.Location = new System.Drawing.Point(493, 415);
            this.cb_orszag.Name = "cb_orszag";
            this.cb_orszag.Size = new System.Drawing.Size(121, 21);
            this.cb_orszag.TabIndex = 16;
            this.cb_orszag.Visible = false;
            // 
            // lb_nev
            // 
            this.lb_nev.AutoSize = true;
            this.lb_nev.Location = new System.Drawing.Point(42, 397);
            this.lb_nev.Name = "lb_nev";
            this.lb_nev.Size = new System.Drawing.Size(61, 13);
            this.lb_nev.TabIndex = 17;
            this.lb_nev.Text = "Üzlet neve:";
            this.lb_nev.Visible = false;
            // 
            // lb_cim
            // 
            this.lb_cim.AutoSize = true;
            this.lb_cim.Location = new System.Drawing.Point(181, 397);
            this.lb_cim.Name = "lb_cim";
            this.lb_cim.Size = new System.Drawing.Size(29, 13);
            this.lb_cim.TabIndex = 18;
            this.lb_cim.Text = "Cím:";
            this.lb_cim.Visible = false;
            // 
            // lb_telefonszam
            // 
            this.lb_telefonszam.AutoSize = true;
            this.lb_telefonszam.Location = new System.Drawing.Point(344, 397);
            this.lb_telefonszam.Name = "lb_telefonszam";
            this.lb_telefonszam.Size = new System.Drawing.Size(70, 13);
            this.lb_telefonszam.TabIndex = 19;
            this.lb_telefonszam.Text = "Telefonszám:";
            this.lb_telefonszam.Visible = false;
            // 
            // lb_orszag
            // 
            this.lb_orszag.AutoSize = true;
            this.lb_orszag.Location = new System.Drawing.Point(493, 396);
            this.lb_orszag.Name = "lb_orszag";
            this.lb_orszag.Size = new System.Drawing.Size(43, 13);
            this.lb_orszag.TabIndex = 20;
            this.lb_orszag.Text = "Ország:";
            this.lb_orszag.Visible = false;
            // 
            // lb_uzletekszama
            // 
            this.lb_uzletekszama.AutoSize = true;
            this.lb_uzletekszama.Location = new System.Drawing.Point(45, 73);
            this.lb_uzletekszama.Name = "lb_uzletekszama";
            this.lb_uzletekszama.Size = new System.Drawing.Size(35, 13);
            this.lb_uzletekszama.TabIndex = 21;
            this.lb_uzletekszama.Text = "label3";
            // 
            // btn_info
            // 
            this.btn_info.Location = new System.Drawing.Point(252, 291);
            this.btn_info.Name = "btn_info";
            this.btn_info.Size = new System.Drawing.Size(240, 23);
            this.btn_info.TabIndex = 22;
            this.btn_info.Text = "Rendeléssel kapcsolatos információk";
            this.btn_info.UseVisualStyleBackColor = true;
            this.btn_info.Click += new System.EventHandler(this.btn_info_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 475);
            this.Controls.Add(this.btn_info);
            this.Controls.Add(this.lb_uzletekszama);
            this.Controls.Add(this.lb_orszag);
            this.Controls.Add(this.lb_telefonszam);
            this.Controls.Add(this.lb_cim);
            this.Controls.Add(this.lb_nev);
            this.Controls.Add(this.cb_orszag);
            this.Controls.Add(this.tb_telefonszam);
            this.Controls.Add(this.tb_cim);
            this.Controls.Add(this.tb_nev);
            this.Controls.Add(this.bt_megse);
            this.Controls.Add(this.bt_kilepes);
            this.Controls.Add(this.bt_mentes);
            this.Controls.Add(this.bt_torles);
            this.Controls.Add(this.bt_modosit);
            this.Controls.Add(this.bt_uj);
            this.Controls.Add(this.label_uzleteklistaja);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_Uzletek);
            this.Controls.Add(this.tb_betu);
            this.Controls.Add(this.cb_orszagok);
            this.Controls.Add(this.bt_szures);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Uzletek)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_szures;
        private System.Windows.Forms.ComboBox cb_orszagok;
        private System.Windows.Forms.TextBox tb_betu;
        private System.Windows.Forms.DataGridView dgv_Uzletek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_uzleteklistaja;
        private System.Windows.Forms.Button bt_uj;
        private System.Windows.Forms.Button bt_modosit;
        private System.Windows.Forms.Button bt_torles;
        private System.Windows.Forms.Button bt_mentes;
        private System.Windows.Forms.Button bt_kilepes;
        private System.Windows.Forms.Button bt_megse;
        private System.Windows.Forms.TextBox tb_nev;
        private System.Windows.Forms.TextBox tb_cim;
        private System.Windows.Forms.TextBox tb_telefonszam;
        private System.Windows.Forms.ComboBox cb_orszag;
        private System.Windows.Forms.Label lb_nev;
        private System.Windows.Forms.Label lb_cim;
        private System.Windows.Forms.Label lb_telefonszam;
        private System.Windows.Forms.Label lb_orszag;
        private System.Windows.Forms.Label lb_uzletekszama;
        private System.Windows.Forms.Button btn_info;
    }
}

