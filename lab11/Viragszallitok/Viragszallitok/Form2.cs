﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Viragszallitok.Models;
using Viragszallitok.Service;
/*Balazs Eva-Tunde, 521/1. beim1596, lab11*/
namespace Viragszallitok
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btn_vissza_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f1 = new Form1();
            f1.Show();
        }

        private void feltoltviragok()
        {
            string err = null;
            ViragFajtakManager manager = new ViragFajtakManager(ref err);
            lb_uzenet.Visible = false;
            manager.CreateConnection(ref err);
            List<Viragfajtak> viragok = manager.GetViragokDataReader(ref err);
            dgv_form2.Rows.Clear();
            dgv_form2.Refresh();
            dgv_form2.ColumnCount = 5;
            dgv_form2.Columns[0].Name = "ViragFajtaID";
            dgv_form2.Columns[1].Name = "Nev";
            dgv_form2.Columns[2].Name = "Leiras";
            dgv_form2.Columns[3].Name = "Ar";
            dgv_form2.Columns[4].Name = "Jelkep";
            //dgv_Uzletek.DataSource = uzletLista;
            foreach (Viragfajtak i in viragok)
            {
                dgv_form2.Rows.Add(i.viragfajtaID,i.nev,i.leiras,i.ar,i.jelkep);
            }
        }

        private void feltoltbonbonok()
        {
            string err = null;
            BonBonokManager manager = new BonBonokManager(ref err);
            lb_uzenet.Visible = false;
            manager.CreateConnection(ref err);
            List<Bonbonok> bonbonok = manager.GetBonbonokDataReader(ref err);
            dgv_form2.Rows.Clear();
            dgv_form2.Refresh();
            dgv_form2.ColumnCount = 5;
            dgv_form2.Columns[0].Name = "BonbonID";
            dgv_form2.Columns[1].Name = "Nev";
            dgv_form2.Columns[2].Name = "Osszetevok";
            dgv_form2.Columns[3].Name = "Minosites";
            dgv_form2.Columns[4].Name = "Szavatossag";
            //dgv_Uzletek.DataSource = uzletLista;
            foreach (Bonbonok i in bonbonok)
            {
                dgv_form2.Rows.Add(i.bonbonID, i.nev, i.osszetevok, i.minosites, i.szavatossag);
            }
        }

        private void feltoltcsokrok()
        {
            string err = null;
            CsokrokManager manager = new CsokrokManager(ref err);
            manager.CreateConnection(ref err);
            string name=cb_viragok.Text;
            MessageBox.Show(cb_viragok.Text);
            float maxPrice=0f;
            List<Csokrok> csokrok = manager.GetCsokrokListDataSetSP(name, ref maxPrice, ref err);
            if (maxPrice >= 0)
            {
                lb_uzenet.Visible = true;
                lb_uzenet.Text = "A legdrágább csokor ára: " + maxPrice;
            }
            dgv_form2.Rows.Clear();
            dgv_form2.Refresh();
            dgv_form2.ColumnCount = 3;
            dgv_form2.Columns[0].Name = "CsokorID";
            dgv_form2.Columns[1].Name = "Szavatossag (napban)";
            dgv_form2.Columns[2].Name = "Uzenet";
            foreach (Csokrok i in csokrok)
            {
                dgv_form2.Rows.Add(i.csokorID, i.szavatossag, i.uzenet.uzenet);
            }
        }

        private void feltoltcsokrokuzenettipus()
        {
            string err = null;
            CsokrokManager manager = new CsokrokManager(ref err);
            manager.CreateConnection(ref err);
            string name = cb_viragok.Text;
            string type = cb_uzenettipus.Text;
            MessageBox.Show(cb_viragok.Text);
            MessageBox.Show(type);
            float lowPrice = 0f;
            List<Csokrok> csokrok = manager.GetCsokrokListDataSetSP(name, type, ref lowPrice, ref err);
            if (lowPrice >= 0)
            {
                lb_uzenet.Visible = true;
                lb_uzenet.Text = "A legolcsóbb csokor ára: " + lowPrice;
            }
            dgv_form2.Rows.Clear();
            dgv_form2.Refresh();
            dgv_form2.ColumnCount = 3;
            dgv_form2.Columns[0].Name = "CsokorID";
            dgv_form2.Columns[1].Name = "Szavatossag (napban)";
            dgv_form2.Columns[2].Name = "Uzenet";
            foreach (Csokrok i in csokrok)
            {
                dgv_form2.Rows.Add(i.csokorID, i.szavatossag, i.uzenet.uzenet);
            }
        }

        private void feltoltlegtobbfajtavirag()
        {
            string err = null;
            CsokrokManager manager = new CsokrokManager(ref err);
            manager.CreateConnection(ref err);
            int max = 0;
            List<Csokrok> csokrok = manager.GetCsokrokListDataSetSP(ref max, ref err);
            if (max >= 0)
            {
                lb_uzenet.Visible = true;
                lb_uzenet.Text = max + " féle virágot tartalmaznak.";
            }
            dgv_form2.Rows.Clear();
            dgv_form2.Refresh();
            dgv_form2.ColumnCount = 3;
            dgv_form2.Columns[0].Name = "CsokorID";
            dgv_form2.Columns[1].Name = "Szavatossag (napban)";
            dgv_form2.Columns[2].Name = "Uzenet";
            foreach (Csokrok i in csokrok)
            {
                dgv_form2.Rows.Add(i.csokorID, i.szavatossag, i.uzenet.uzenet);
            }
        }

        private void btn_select_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                feltoltviragok();
            } else if (radioButton2.Checked == true)
            {
                feltoltbonbonok();
            } else if (radioButton3.Checked == true)
            {
                feltoltcsokrok();
            } else if (radioButton4.Checked == true)
            {
                feltoltcsokrokuzenettipus();
            }else if (radioButton5.Checked == true)
            {
                feltoltlegtobbfajtavirag();
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            string err = null;
            ViragFajtakManager manager1 = new ViragFajtakManager(ref err);
            manager1.CreateConnection(ref err);
            List<Viragfajtak> lista = manager1.GetViragokDataReader(ref err);
            this.cb_viragok.DataSource = lista;
            cb_viragok.DisplayMember = "nev";
            cb_viragok.ValueMember = "viragfajtaID";
            cb_viragok.Visible = true;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            string err = null;
            ViragFajtakManager manager1 = new ViragFajtakManager(ref err);
            manager1.CreateConnection(ref err);
            List<Viragfajtak> lista = manager1.GetViragokDataReader(ref err);
            this.cb_viragok.DataSource = lista;
            cb_viragok.DisplayMember = "nev";
            cb_viragok.ValueMember = "viragfajtaID";
            cb_viragok.Visible = true;

            UzenetTipusManager manager2 = new UzenetTipusManager(ref err);
            manager2.CreateConnection(ref err);
            List<UzenetTipusok> lista1 = manager2.GetUzenetTipusokDataReader(ref err);
            this.cb_uzenettipus.DataSource = lista1;
            cb_uzenettipus.DisplayMember = "szoveg";
            cb_uzenettipus.ValueMember = "uzenettipusID";
            cb_uzenettipus.Visible = true;
        }
    }
}
