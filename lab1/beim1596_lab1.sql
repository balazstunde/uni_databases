/*N�v: Bal�zs �va-T�nde
  Azonos�t�: beim1596
  Csoport: 511/1
  Feladat: labor 1, 2. csoport
  2.csoport

  Szineszek(SzineszID, SzineszNev, SzulDatum)

  Mufaj(MufajID, MufajNev)

  Studiok(StudioID, StudioNev, StudioCim)

  Filmek(FilmID, FilmCim, Koltseg, MegjEv, StudioID, MufajID)

  Szerepel(SzineszID, FilmID)

  Vetites(VetitesID, FilmID, Datum)*/
/*DROP TABLE Vetites;
DROP TABLE Szerepel;
DROP TABLE Filmek;
DROP TABLE Studiok;
DROP TABLE Mufaj;
DROP TABLE Szineszek;*/

CREATE TABLE Szineszek(
  SzineszID INT, CONSTRAINT PK_SzineszID PRIMARY KEY(SzineszID),
  SzineszNev VARCHAR(30),
  SzulDatum DATE
);

CREATE TABLE Mufaj(
  MufajID INT, CONSTRAINT PK_MufajID PRIMARY KEY(MufajID),
  MufajNev VARCHAR(30),
);

CREATE TABLE Studiok(
  StudioID INT, CONSTRAINT PK_StudioID PRIMARY KEY(StudioID),
  StudioNev VARCHAR(30),
  StudioCim VARCHAR(30),
);

CREATE TABLE Filmek(
  FilmID INT, CONSTRAINT PK_FilmID PRIMARY KEY(FilmID),
  FilmCim VARCHAR(30),
  Koltseg FLOAT(30),
  MegjEv DATE,
  StudioID INT, CONSTRAINT FK_F_SID FOREIGN KEY (StudioID) REFERENCES Studiok(StudioID),
  MufajID INT, CONSTRAINT FK_F_MID FOREIGN KEY (MufajID) REFERENCES Mufaj(MufajID),
);

CREATE TABLE Szerepel(
  SzineszID INT,
  FilmID INT,
  CONSTRAINT FK_Sz_SzID FOREIGN KEY (SzineszID) REFERENCES Szineszek(SzineszID),
  CONSTRAINT FK_Sz_FID FOREIGN KEY (FilmID) REFERENCES Filmek(FilmID),
  CONSTRAINT PK_Szerepel PRIMARY KEY(SzineszID,FilmID)
);

CREATE TABLE Vetites(
  VetitesID INT,CONSTRAINT PK_VID PRIMARY KEY(VetitesID),
  FilmID INT,CONSTRAINT FK_V_FID FOREIGN KEY (FilmID) REFERENCES Filmek(FilmID),
  Datum DATE
);