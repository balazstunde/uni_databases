/*Bal�zs �va-T�nde, beim1596, 521/1*/

/*1.Adjuk meg azon rendez�ket melyeket pont ugyanabban a m�faj� filmekben tev�kenykedtek, mint �Audrey Haines� rendez�. (Rendez�k.Rendez�N�v)*/
use Lab9_2

SELECT Rendezok.RendezoNev
FROM Rendezok,Filmek, Mufajok, Rendezte, Mufaja
WHERE Rendezok.RendezoID=Rendezte.RendezoID 
	AND Filmek.FilmID=Rendezte.FilmID 
	AND Mufaja.FilmID=Filmek.FilmID 
	AND Mufaja.MufajID=Mufajok.MufajID
	AND Rendezok.RendezoNev!='Audrey Haines'
	AND Mufajok.MufajNev IN(
		SELECT Mufajok.MufajNev
		FROM Rendezok,Filmek, Mufajok, Rendezte, Mufaja
		WHERE Rendezok.RendezoID=Rendezte.RendezoID 
			AND Filmek.FilmID=Rendezte.FilmID 
			AND Mufaja.FilmID=Filmek.FilmID 
			AND Mufaja.MufajID=Mufajok.MufajID
			AND Rendezok.RendezoNev='Audrey Haines')
GROUP BY Rendezok.RendezoNev
HAVING COUNT(Mufajok.MufajNev)=(
	SELECT COUNT(Mufajok.MufajNev)
	FROM Rendezok,Filmek, Mufajok, Rendezte, Mufaja
		WHERE Rendezok.RendezoID=Rendezte.RendezoID 
			AND Filmek.FilmID=Rendezte.FilmID 
			AND Mufaja.FilmID=Filmek.FilmID 
			AND Mufaja.MufajID=Mufajok.MufajID
			AND Rendezok.RendezoNev='Audrey Haines')

/*2.Adjuk meg az ugyanazzal a n�vvel rendelkez� filmc�meket. (Filmek.FilmCim)*/
GO
CREATE VIEW FilmekOsszes AS
SELECT Filmek.FilmID,Filmek.FilmCim
FROM Filmek
GO
SELECT DISTINCT Filmek.FilmCim
FROM Filmek, FilmekOsszes
WHERE Filmek.FilmID!=FilmekOsszes.FilmID AND Filmek.FilmCim=FilmekOsszes.FilmCim
/*3.Adjuk meg azon m�fajokat, amelyben a legt�bb olyan st�di� k�sz�tett filmet, melyeknek a k�lts�ge minim�lis! (Mufajok.MufajNev)*/

GO
CREATE VIEW StudiokMinKoltseg AS
SELECT Studiok.StudioID, COUNT(Filmek.FilmID) AS Szam
FROM Studiok, Filmek
WHERE Filmek.StudioID=Studiok.StudioID AND Filmek.Koltseg=(SELECT MIN(Filmek.Koltseg) FROM Filmek)
GROUP BY Studiok.StudioID
GO

SELECT DISTINCT Mufajok.MufajNev
FROM Mufajok,Filmek,StudiokMinKoltseg, Mufaja
WHERE StudiokMinKoltseg.StudioID=Filmek.StudioID AND Mufajok.MufajID=Mufaja.MufajID AND Mufaja.FilmID=Filmek.FilmID AND Szam=(SELECT MAX(StudiokMinKoltseg.Szam) FROM StudiokMinKoltseg)

/*4.Adjuk meg azon szerepl�ket, akik CSAK romantikus �S horror filmet rendeztek! (Rendezok.RendezoNev)*/
SELECT DISTINCT Rendezok.RendezoID,Rendezok.RendezoNev
FROM Rendezok, Rendezte, Filmek, Mufajok, Mufaja
WHERE Rendezok.RendezoID=Rendezte.RendezoID AND Rendezte.FilmID=Filmek.FilmID AND Mufajok.MufajID=Mufaja.MufajID AND Mufaja.FilmID=Filmek.FilmID 
	AND Mufajok.MufajNev='Horror'
INTERSECT
SELECT DISTINCT Rendezok.RendezoID, Rendezok.RendezoNev
FROM Rendezok, Rendezte, Filmek, Mufajok, Mufaja
WHERE Rendezok.RendezoID=Rendezte.RendezoID AND Rendezte.FilmID=Filmek.FilmID AND Mufajok.MufajID=Mufaja.MufajID AND Mufaja.FilmID=Filmek.FilmID 
	AND Mufajok.MufajNev='Drama'
EXCEPT
SELECT DISTINCT Rendezok.RendezoID, Rendezok.RendezoNev
FROM Rendezok, Rendezte, Filmek, Mufajok, Mufaja
WHERE Rendezok.RendezoID=Rendezte.RendezoID AND Rendezte.FilmID=Filmek.FilmID AND Mufajok.MufajID=Mufaja.MufajID AND Mufaja.FilmID=Filmek.FilmID 
	AND Mufajok.MufajNev!='Drama' AND Mufajok.MufajNev!='Horror'

/*5.Adjuk meg azon st�di�kat, melyek minden m�fajban k�sz�tettek filmet! (Studiok.StudioNev)*/

GO
CREATE VIEW StudiokMufajszamai AS
SELECT Studiok.StudioNev, COUNT (DISTINCT Mufaja.MufajID) AS StudiokMufajai
FROM Studiok, Filmek, Mufaja
WHERE Studiok.StudioID=Filmek.StudioID AND Mufaja.FilmID=Filmek.FilmID
GROUP BY Studiok.StudioNev
GO
SELECT Studiok.StudioNev
FROM Studiok,StudiokMufajszamai
WHERE Studiok.StudioNev=StudiokMufajszamai.StudioNev AND StudiokMufajszamai.StudiokMufajai=(SELECT Count(*)
FROM Mufajok)

/*6.Adjuk meg minden film eset�n, hogy mennyire sok rendez�je van (FilmCim, Rendez�sek) form�ban, ahol:
		1.ha t�bb, mint 5 rendez�je van, akkor: Rendez�sek �rt�ke: �sok�
		2.ha  2-4 rendez�je van, akkor: Rendez�sek �rt�ke: � k�zepes�
		3.ha kevesebb, mint 2 rendez�je van, akkor: Rendez�sek �rt�ke: �kev�s�*/

GO 
CREATE VIEW FilmekNepszerusege AS
SELECT Filmek.FilmCim, COUNT(Rendezte.RendezoID) AS Rendezesek
FROM Filmek, Rendezte
WHERE Filmek.FilmID=Rendezte.FilmID
GROUP BY FIlmek.FilmID, Filmek.FilmCim
GO

SELECT FilmekNepszerusege.FilmCim, "Rendezesek"=
	CASE 
		WHEN FilmekNepszerusege.Rendezesek>5 THEN 'sok'
		WHEN FilmekNepszerusege.Rendezesek<2 THEN 'keves'
		ELSE 'kozepes'
	END
FROM FilmekNepszerusege
/*7. �rjunk t�rolt elj�r�st, melynek bemen� param�tere egy term�szetes sz�m (@pSzam). Ha @pSzam<0, �rjunk ki megfelel� hiba�zenetet! Ellenkez� esetben, �rassuk ki azon Studiok nev�t �s cim�t, ahol k�sz�ltek kisebb k�lt�sgvet�s� filmek, mint a param�terk�nt megadott sz�m!*/
GO
CREATE PROCEDURE Eljaras
	@pSzam INT
	AS IF @pszam<0
		BEGIN
			PRINT 'Pozitiv szamot!'
		END
	ELSE
		SELECT Studiok.StudioNev, Studiok.StudioCim
		FROM Studiok, Filmek
		WHERE Filmek.StudioID=Studiok.StudioID AND Filmek.Koltseg<@pSzam
GO
EXEC Eljaras @pSzam=40858

/*8. �rjunk t�rolt elj�r�st, mely megadja a param�terk�nt megadott m�faj (@pMufajNev) �s ugyancsak a param�terk�nt megadott  StudioNev (@StudioNev) eset�n az �tlagk�lts�get! Ha nincs egyetlen olyan m�faj� film az adott studi�ban, akkor a @pOut kimeneti param�ter �rt�k�t �ll�tsuk -2-re, ellenkez� esetben 0-ra!*/
GO 
create PROCEDURE Eljaras2
	@pMufajNev VARCHAR(30), @StudioNev VARCHAR(30), @pOut INT output
	AS BEGIN DECLARE @atlag INT
		SELECT @atlag=AVG(Filmek.Koltseg)
		FROM Mufajok, Mufaja, Filmek, Studiok
		WHERE Mufajok.MufajID=Mufaja.MufajID AND Mufaja.FilmID=Filmek.FilmID AND Filmek.StudioID=Studiok.StudioID AND Mufajok.MufajNev=@pMufajNev AND Studiok.StudioNev=@StudioNev
	
		PRINT @atlag
		IF (SELECT COUNT(*) FROM Filmek, Mufaja, Mufajok WHERE Filmek.FilmID=Mufaja.MufajID AND Mufaja.MufajID=Mufajok.MufajID AND Mufajok.MufajNev=@pMufajNev )=0
			BEGIN SET @pOut=-2 END
		ELSE
			BEGIN SET @pOut=0 END
		END
GO

DECLARE @pSzam INT
EXEC Eljaras2 'Horror','Daniel LLC',@pSzam output
SELECT @pSzam 

DROP VIEW StudiokMufajszamai;
DROP VIEW FilmekOsszes;
DROP VIEW FilmekNepszerusege;
DROP VIEW StudiokMinKoltseg;

DROP PROCEDURE Eljaras;
DROP PROCEDURE Eljaras2