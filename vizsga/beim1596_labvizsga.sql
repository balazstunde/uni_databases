use vizsga2
/*Bal�zs �va-T�nde, beim1596, 521/1*/
/*1*/
SELECT DISTINCT Fogorvosok.OrvosVNev, Fogorvosok.OrvosKNev
FROM Fogorvosok, Elojegyzesek
WHERE Fogorvosok.OrvosID = Elojegyzesek.OrvosID AND YEAR(Elojegyzesek.Datum)=2010 AND MONTH(Elojegyzesek.Datum)=11 AND Ora>=10 AND Ora <=13

/*2*/
SELECT DISTINCT Betegek.BetegVNev, Betegek.BetegKNev, Kezelesek.KezelesNev
FROM Betegek, Elojegyzesek, Kezel, Kezelesek
WHERE Betegek.CNP=Elojegyzesek.CNP AND Kezel.ElojegyzesID=Elojegyzesek.ElojegyzesID AND Kezelesek.KezelesID=Kezel.KezelesID AND Kezelesek.KezelesNev='Foghuzas'

/*3*/
SELECT Betegek.BetegVNev, Betegek.BetegKNev, COUNT(Elojegyzesek.ElojegyzesID) AS Elojegyzzesszam
FROM Betegek, Elojegyzesek
WHERE Betegek.CNP=Elojegyzesek.CNP
GROUP BY Betegek.CNP, Betegek.BetegVNev, Betegek.BetegKNev

/*4*/
SELECT SUM(Kezelesek.KezelesAr) AS OsszAr
FROM Kezelesek, Betegek, Elojegyzesek,Kezel
WHERE Betegek.CNP=Elojegyzesek.CNP AND Elojegyzesek.ElojegyzesID=Kezel.ElojegyzesID AND Kezel.KezelesID=Kezelesek.KezelesID  AND Betegek.BetegVNev='Andras'

/*5*/
SELECT Betegek.BetegVNev,Betegek.BetegKNev, SUM(Kezelesek.KezelesAr) AS OsszAr
FROM Kezelesek, Betegek, Elojegyzesek,Kezel
WHERE Betegek.CNP=Elojegyzesek.CNP AND Elojegyzesek.ElojegyzesID=Kezel.ElojegyzesID AND Kezel.KezelesID=Kezelesek.KezelesID
GROUP BY Betegek.CNP, Betegek.BetegVNev,Betegek.BetegKNev
HAVING SUM(Kezelesek.KezelesAr)>100

/*6*/
GO
CREATE VIEW ElojegyzesSzamok AS
SELECT Elojegyzesek.Datum, COUNT(Elojegyzesek.ElojegyzesID) AS ElojegyzesSzam
FROM Elojegyzesek
GROUP BY Elojegyzesek.Datum
GO
SELECT ElojegyzesSzamok.Datum, ElojegyzesSzamok.ElojegyzesSzam
FROM ElojegyzesSzamok
WHERE ElojegyzesSzam = (SELECT MAX(ElojegyzesSzamok.ElojegyzesSzam) FROM ElojegyzesSzamok)
DROP VIEW ElojegyzesSzamok

/*7*/
GO
CREATE VIEW KezOrvSzama AS
SELECT Kezelesek.KezelesID, COUNT(DISTINCT Fogorvosok.OrvosID) AS KezeloOrvosokSzama
FROM Kezelesek,Fogorvosok,Kezel,Elojegyzesek
WHERE Kezelesek.KezelesID=Kezel.KezelesID AND Kezel.ElojegyzesID=Elojegyzesek.ElojegyzesID AND Elojegyzesek.OrvosID=Fogorvosok.OrvosID
GROUP BY Kezelesek.KezelesID
GO
SELECT Kezelesek.KezelesNev,Kezelesek.KezelesAr
FROM KezOrvSzama, Kezelesek
WHERE KezOrvSzama.KezelesID = Kezelesek.KezelesID AND KezOrvSzama.KezeloOrvosokSzama = (SELECT COUNT(*) FROM Fogorvosok)
DROP VIEW KezOrvSzama

/*8*/
SELECT KezeloHelysegek.*
FROM KezeloHelysegek,Kezelesek,KezelesHelyszine
WHERE Kezelesek.KezelesID = KezelesHelyszine.KezelesID AND KezeloHelysegek.SzobaSzam=KezelesHelyszine.SzobaSzam AND Kezelesek.KezelesNev='Rontgen'
EXCEPT
SELECT KezeloHelysegek.*
FROM KezeloHelysegek,Kezelesek,KezelesHelyszine
WHERE Kezelesek.KezelesID = KezelesHelyszine.KezelesID AND KezeloHelysegek.SzobaSzam=KezelesHelyszine.SzobaSzam AND Kezelesek.KezelesNev!='Rontgen'

/*9*/
SELECT Betegek.BetegVNev, Betegek.BetegKNev
FROM Betegek,Elojegyzesek,Kezel,Kezelesek
WHERE Betegek.CNP=Elojegyzesek.CNP AND Elojegyzesek.ElojegyzesID=Kezel.ElojegyzesID AND Kezelesek.KezelesID=Kezel.KezelesID AND Kezelesek.KezelesNev='Altalanos vizsgalat'
INTERSECT
SELECT Betegek.BetegVNev, Betegek.BetegKNev
FROM Betegek,Elojegyzesek,Kezel,Kezelesek
WHERE Betegek.CNP=Elojegyzesek.CNP AND Elojegyzesek.ElojegyzesID=Kezel.ElojegyzesID AND Kezelesek.KezelesID=Kezel.KezelesID AND Kezelesek.KezelesNev='Rontgen'

/*10*/
SELECT KezeloHelysegek.SzobaSzam, COUNT(DISTINCT Betegek.CNP) AS BetegSzam
FROM KezeloHelysegek LEFT JOIN Elojegyzesek ON (Elojegyzesek.Szobaszam=KezeloHelysegek.SzobaSzam)
					LEFT JOIN Betegek ON (Elojegyzesek.CNP=Betegek.CNP)
GROUP BY KezeloHelysegek.SzobaSzam
ORDER BY BetegSzam