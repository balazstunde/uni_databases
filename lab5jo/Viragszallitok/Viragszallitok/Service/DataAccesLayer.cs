﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
/*Balazs Eva-Tunde, 521, beim1596*/
namespace Viragszallitok.Service
{
    public abstract class DataAccesLayer
    {
        private static SqlConnection conn;
        protected static bool isConnected;
        private static string connstr = "Data Source=TUNDIKE-LAPTOP\\SQLEXPRESS;Initial Catalog=ViragSzallito;Integrated Security=SSPI";

        public DataAccesLayer()
        {
            isConnected = false;
            conn = null;
        }

        public void CreateConnection(ref string errMess)
        {
            if (isConnected != true)
            {
                try
                {
                    conn = new SqlConnection(connstr);
                    conn.Open();
                    errMess = "OK";
                }
                catch (Exception exc)
                {
                    errMess = exc.Message;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        private void OpenConnection(ref string errMess)
        {
            // Open the Connection when the state is not already open.
            if (isConnected == false)
            {
                try
                {
                    conn.Open();
                    isConnected = true;
                    errMess = "OK";
                }
                catch (SqlException ex)
                {
                    errMess = ex.Message;
                    MessageBox.Show(errMess);
                }
            }
        }

        private void CloseConnection()
        {
            // Close the Connection when the connection is opened.
            if (isConnected == true)
            {
                conn.Close();
                isConnected = false;
            }
        }

        public String GetOneValue(string query, ref string errorMessage)
        {
            string value = null;
            errorMessage = null;
            try
            {
                OpenConnection(ref errorMessage);
                SqlCommand cmd = new SqlCommand(query, conn);
                value = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            finally
            {
                CloseConnection();
            }
            return value;
        }

        protected SqlDataReader ExecuteReader(string sQuery, ref string errMess)
        {
            SqlDataReader sqlDataReader = null;
            try
            {
                OpenConnection(ref errMess);//megnyitja az adatbazis-kapcsolatot, ha meg nincs megnyitva) 
                SqlCommand sqlCommand = new SqlCommand(sQuery, conn);
                MessageBox.Show("sql" + sQuery);
                sqlDataReader = sqlCommand.ExecuteReader();
                errMess = "OK";
            }
            catch (Exception e)
            {
                errMess = e.Message;
                CloseDataReader(sqlDataReader);
            }
            return sqlDataReader;
        }

        public void CloseDataReader(SqlDataReader rdr)
        {
            if (rdr != null)
                rdr.Close();
            CloseConnection();
        }
        
    }
}
