﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Viragszallitok.Models;
using System.Windows.Forms;
/*Balazs Eva-Tunde, 521, beim1596*/
namespace Viragszallitok.Service
{
    class UzletManager:DataAccesLayer
    {

        public UzletManager(ref string error)
        {
            base.CreateConnection(ref error);
        }
        public List<Uzletek> GetUzletListDataReader(char kbetu, string orszag, ref string error)
        {
            string query = "SELECT Uzletek.UzletID, Uzletek.Cim, Uzletek.Nev, Uzletek.Telefonszam, Orszagok.OrszagID, Orszagok.Nev " +
                "FROM Uzletek, Orszagok" +
                " WHERE Orszagok.OrszagID = Uzletek.OrszagID AND Uzletek.Nev LIKE(\'" + kbetu + "%\')" + " AND Orszagok.OrszagID = " + orszag ;

            SqlDataReader dataReader = ExecuteReader(query, ref error);
            List<Uzletek> uzletLista = new List<Uzletek>();

            MessageBox.Show(error);

            if (error == "OK")
            {
                
                while (dataReader.Read())
                {
                    Uzletek item = new Uzletek();
                    try
                    {
                        item.uzletID = Convert.ToInt32(dataReader["UzletID"]);
                        item.uzletCim = dataReader["Cim"].ToString();
                        item.uzletNev = dataReader["Nev"].ToString();
                        item.uzletTelefonszam = dataReader["Telefonszam"].ToString();
                        item.uzletOrszaga = new Orszagok(Convert.ToInt32(dataReader["OrszagID"]), dataReader["Nev"].ToString());

                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                    uzletLista.Add(item);
                }
            }
            CloseDataReader(dataReader);

            return uzletLista;
        }

        public List<Uzletek> GetUzletList(ref string error)
        {
            string query = "SELECT Uzletek.UzletID, Uzletek.Cim, Uzletek.Nev, Uzletek.Telefonszam, Orszagok.OrszagID, Orszagok.Nev" +
                " FROM Uzletek, Orszagok " +
                "WHERE Orszagok.OrszagID = Uzletek.OrszagID";

            SqlDataReader dataReader = ExecuteReader(query, ref error);

            List<Uzletek> uzletLista = new List<Uzletek>();
            Uzletek item = null;

            if (error == "OK")
            {
                while (dataReader.Read())
                {
                    item = new Uzletek();
                    try
                    {
                        item.uzletID = Convert.ToInt32(dataReader["UzletID"]);
                        item.uzletCim = dataReader["Cim"].ToString();
                        item.uzletNev = dataReader["Nev"].ToString();
                        item.uzletTelefonszam = dataReader["Telefonszam"].ToString();
                        item.uzletOrszaga = new Orszagok(Convert.ToInt32(dataReader["OrszagID"]), dataReader["Nev"].ToString());
                    }
                    catch (Exception ex)
                    {
                        error = "Invalid data " + ex.Message;
                    }
                    uzletLista.Add(item);
                }
            }
            CloseDataReader(dataReader);
            return uzletLista;
        }

        //public List<Uzletek> GetUzletListDataSet(int orszagId, ref string error)
        //{
        //    string query = "SELECT Uzletek.UzletID, Uzletek.Cim,  Uzletek.Nev, Uzletek.Telefonszam, Orszagok.OrszagID, Orszagok.OrszagID, Orszagok.ONev" +
        //        "FROM Uzletek, Orszagok" +
        //        "WHERE Orszagok.OrszagID = Uzletek.OrszagID";

        //    DataSet ds_tabla = new DataSet();
        //    ds_tabla = ExecuteDS(query, ref error);

        //    List<Uzletek> uzletLista = new List<Uzletek>();

        //    if (error == "OK")
        //    {
        //        Uzletek item = new Uzletek();
        //        foreach (DataRow r in ds_tabla.Tables[0].Rows)
        //        {
        //            try
        //            {
        //                item.uzletID = Convert.ToInt32(r["UzletID"]);
        //                item.uzletCim = r["Cim"].ToString();
        //                item.uzletNev = r["Nev"].ToString();
        //                item.uzletTelefonszam = r["Telefonszam"].ToString();
        //                item.uzletOrszaga = new Orszagok(Convert.ToInt32(r["OrszagID"]), r["ONev"].ToString());
        //            }
        //            catch (Exception ex)
        //            {
        //                error = "Invalid data " + ex.Message;
        //            }

        //            uzletLista.Add(item);
        //        }
        //    }

        //    return uzletLista;
        //}
    }
}
