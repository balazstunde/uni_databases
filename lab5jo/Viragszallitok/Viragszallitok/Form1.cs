﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Viragszallitok.Models;
using Viragszallitok.Service;
/*Balazs Eva-Tunde, 521, beim1596*/
namespace Viragszallitok
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OrszagManager manager = new OrszagManager();
            string err = null;
            manager.CreateConnection(ref err);
            List<Orszagok> lista = manager.GetOrszagList(ref err);
            this.cb_orszagok.DataSource = lista;
            cb_orszagok.DisplayMember = "OrszagNev";
            cb_orszagok.ValueMember = "orszagID";

            err = null;
            UzletManager uzletmanager = new UzletManager(ref err);

            List<Uzletek> uzletLista = uzletmanager.GetUzletList(ref err);
            dgv_Uzletek.DataSource = uzletLista;
            //dgv_Uzletek.ColumnCount = 5;
            //dgv_Uzletek.Columns[0].Name = "uzletID";
            //dgv_Uzletek.Columns[1].Name = "uzletNev";
            //dgv_Uzletek.Columns[2].Name = "uzletCim";
            //dgv_Uzletek.Columns[3].Name = "uzletTelefonszam";
            //dgv_Uzletek.Columns[4].Name = "OrszagNev";
            //foreach (Uzletek item in uzletLista)
            //{
            //    dgv_Uzletek.Rows.Add(item.uzletID,item.uzletNev,item.uzletCim,item.uzletTelefonszam,item.uzletOrszaga.OrszagNev);
            //}
        }

        private void bt_szures_Click(object sender, EventArgs e)
        {
            string betu = tb_betu.Text;
            string orszagNev = cb_orszagok.SelectedValue.ToString();
            string err = null;
            UzletManager manager = new UzletManager(ref err);
            
            manager.CreateConnection(ref err);
            List<Uzletek> uzletLista = manager.GetUzletListDataReader(betu[0], orszagNev, ref err);
            
            //dgv_Uzletek.ColumnCount = 5;
            //dgv_Uzletek.Columns[0].Name = "UzletID";
            //dgv_Uzletek.Columns[1].Name = "Nev";
            //dgv_Uzletek.Columns[2].Name = "Cim";
            //dgv_Uzletek.Columns[3].Name = "Telefonszam";
            //dgv_Uzletek.Columns[4].Name = "OrszagNev";
            dgv_Uzletek.DataSource = uzletLista;
            //foreach (Uzletek i in uzletLista)
            //{
            //    dgv_Uzletek.Rows.Add(i.uzletID, i.uzletNev, i.uzletCim, i.uzletTelefonszam, i.uzletOrszaga.OrszagNev);
            //}
        }
    }
}
