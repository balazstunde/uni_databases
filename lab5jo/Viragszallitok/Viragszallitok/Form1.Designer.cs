﻿namespace Viragszallitok
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_szures = new System.Windows.Forms.Button();
            this.cb_orszagok = new System.Windows.Forms.ComboBox();
            this.tb_betu = new System.Windows.Forms.TextBox();
            this.dgv_Uzletek = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Uzletek)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_szures
            // 
            this.bt_szures.Location = new System.Drawing.Point(177, 307);
            this.bt_szures.Name = "bt_szures";
            this.bt_szures.Size = new System.Drawing.Size(75, 23);
            this.bt_szures.TabIndex = 0;
            this.bt_szures.Text = "Szűrés";
            this.bt_szures.UseVisualStyleBackColor = true;
            this.bt_szures.Click += new System.EventHandler(this.bt_szures_Click);
            // 
            // cb_orszagok
            // 
            this.cb_orszagok.FormattingEnabled = true;
            this.cb_orszagok.Location = new System.Drawing.Point(260, 41);
            this.cb_orszagok.Name = "cb_orszagok";
            this.cb_orszagok.Size = new System.Drawing.Size(121, 21);
            this.cb_orszagok.TabIndex = 1;
            // 
            // tb_betu
            // 
            this.tb_betu.Location = new System.Drawing.Point(42, 41);
            this.tb_betu.Name = "tb_betu";
            this.tb_betu.Size = new System.Drawing.Size(100, 20);
            this.tb_betu.TabIndex = 2;
            // 
            // dgv_Uzletek
            // 
            this.dgv_Uzletek.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Uzletek.Location = new System.Drawing.Point(42, 108);
            this.dgv_Uzletek.Name = "dgv_Uzletek";
            this.dgv_Uzletek.Size = new System.Drawing.Size(338, 177);
            this.dgv_Uzletek.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 342);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_Uzletek);
            this.Controls.Add(this.tb_betu);
            this.Controls.Add(this.cb_orszagok);
            this.Controls.Add(this.bt_szures);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Uzletek)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_szures;
        private System.Windows.Forms.ComboBox cb_orszagok;
        private System.Windows.Forms.TextBox tb_betu;
        private System.Windows.Forms.DataGridView dgv_Uzletek;
        private System.Windows.Forms.Label label1;
    }
}

