use Lab7_2;

/*1) Adjuk meg azon kocsm�kat, ahol �rulnak tea t�pus� italokat! (Kocsmak.Nev)*/
SELECT DISTINCT Kocsmak.Nev
FROM Kocsmak,ItalTipusok,Arak,Italok
WHERE Kocsmak.KocsmaID=Arak.KocsmaID AND Arak.ItalID=Italok.ItalID AND Italok.TipusID=ItalTipusok.TipusID AND ItalTipusok.TipusNev='tea';

/*2) Adjuk meg azon bar�tok kedvenc kocsm�inak nev�t �s c�m�t, akiknek `alibaba`-s e-mail c�m�k van! (Kocsmak.Nev, Kocsmak.Cim)*/
SELECT Kocsmak.Nev, Kocsmak.Cim
FROM Kocsmak, Baratok, Kedvencek
WHERE Kocsmak.KocsmaID=Kedvencek.KocsmaID AND Baratok.BaratID=Kedvencek.BaratID AND Baratok.Email LIKE ('%@alibaba%')

/*3) Adjuk meg a 35 RON-n�l dr�g�bb italok nev�t �s t�pus�t! (Italok.Nev, ItalTipusok.TipusNev)*/
SELECT Italok.Nev, ItalTipusok.TipusNev
FROM Italok, ItalTipusok, Arak
WHERE Italok.ItalID=Arak.ItalID AND ItalTipusok.TipusID=Italok.TipusID AND Arak.Ar>=35

/*4) Adjuk meg azon kocsm�kat, ahol nem �rulnak konyakot! (Kocsmak.Nev)*/
SELECT  Kocsmak.Nev
FROM Kocsmak
EXCEPT
SELECT  Kocsmak.Nev
FROM Kocsmak, Arak, ItalTipusok, Italok
WHERE Kocsmak.KocsmaID=Arak.KocsmaID AND ItalTipusok.TipusID=Italok.TipusID AND Italok.ItalID=Arak.ItalID AND ItalTipusok.TipusNev='konyak';
/*5) Adjuk meg kocsm�nk�nt a te�k �tlag elad�si �r�t! (Kocsmak.Nev, AtlagAr)*/
SELECT Kocsmak.Nev, AVG(Arak.Ar) as Tea
FROM Kocsmak, Arak, Italok, ItalTipusok
WHERE Kocsmak.KocsmaID=Arak.KocsmaID AND Italok.ItalID=Arak.ItalID AND ItalTipusok.TipusID=Italok.TipusID AND ItalTipusok.TipusNev='tea'
GROUP BY Kocsmak.Nev;

/*6) Adjuk meg azon kocsm�kat, ahol �rulnak vodk�t �s te�t is! (Kocsmak.Nev)*/
SELECT Kocsmak.Nev
FROM Kocsmak, Arak, Italok, ItalTipusok
WHERE Kocsmak.KocsmaID=Arak.KocsmaID AND Italok.ItalID=Arak.ItalID AND ItalTipusok.TipusID=Italok.TipusID
AND ItalTipusok.TipusNev='vodka'
INTERSECT
SELECT Kocsmak.Nev
FROM Kocsmak, Arak, Italok, ItalTipusok
WHERE Kocsmak.KocsmaID=Arak.KocsmaID AND Italok.ItalID=Arak.ItalID AND ItalTipusok.TipusID=Italok.TipusID
AND ItalTipusok.TipusNev='tea'

/*7) Adjuk meg azon bar�tok nev�t �s telefonsz�m�t, akik legal�bb 2 kocsm�t kedvelnek! (Baratok.Nev, Baratok.Tel)*/
/*SELECT DISTINCT Baratok.Nev, Baratok.Tel
FROM Baratok,Kedvencek
WHERE Baratok.BaratID=Kedvencek.BaratID AND 2<=(SELECT COUNT(BaratID) FROM Kedvencek WHERE Kedvencek.BaratID=Baratok.BaratID)
*/
SELECT DISTINCT Baratok.Nev, Baratok.Tel
FROM Baratok,Kedvencek
WHERE Baratok.BaratID=Kedvencek.BaratID 
GROUP BY Baratok.Nev, Baratok.Tel
HAVING 2<=COUNT(*);
/*8) Adjuk meg a `Frederico Sawley` kocsm�ban kaphat� italt�pusok nev�t! (ItalTipusok.TipusNev)*/
SELECT DISTINCT ItalTipusok.TipusNev
FROM ItalTipusok, Kocsmak, Italok, Arak
WHERE Kocsmak.KocsmaID=Arak.KocsmaID AND Italok.ItalID=Arak.ItalID AND ItalTipusok.TipusID=Italok.TipusID AND Kocsmak.Nev='Frederico Sawley'

/*9) Adjuk meg `Rik Turbard` kedvenc kocsm�iban fellelhet� italok sz�m�t �s �ssz�rt�k�t! (Kocsmak.Nev, ItalSzam, OsszErtek)*/
SELECT Kocsmak.Nev, COUNT(Arak.ItalID) as ItalSzam, SUM(Ar) as OsszErtek
FROM Kocsmak, Arak, Kedvencek, Baratok
WHERE Kocsmak.KocsmaID=Arak.KocsmaID AND Kedvencek.KocsmaID=Kocsmak.KocsmaID AND Kedvencek.BaratID=Baratok.BaratID AND Baratok.Nev='Rik Turbard'
GROUP BY Kocsmak.KocsmaID, Kocsmak.Nev;

/*10)  Adjuk meg azon kocsma(kocsm�k) nev�t �s c�m�t, ahol a legolcs�bb a `tonic` italTipus! (Kocsmak.Nev, Kocsmak.Cim)*/
SELECT Kocsmak.Nev, Kocsmak.Cim
FROM Kocsmak, ItalTipusok, Arak, Italok
WHERE Kocsmak.KocsmaID=Arak.KocsmaID AND Arak.ItalID=Italok.ItalID AND Italok.TipusID=ItalTipusok.TipusID AND ItalTipusok.TipusNev='tonic' 
AND Arak.Ar=(SELECT MIN(Ar) FROM Italok, ItalTipusok, Arak WHERE Arak.ItalID=Italok.ItalID AND Italok.TipusID=ItalTipusok.TipusID AND ItalTipusok.TipusNev='tonic')
